﻿using Dapper;
using Endor.Search.Core;
using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Search.infrastructure.Services
{
    public class ParseGridReaderResults
    {
        public static List<AlertDefinitionSearchModel> parseAlertDefinitionSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<AlertDefinitionSearchModel>().ToList();
            var allAlertDefinitionActions = gridReader.Read<AlertDefinitionAction>().ToList();
            var allSystemAutomationTriggerDefinitions = gridReader.Read<SystemAutomationTriggerDefinition>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allAlertDefinitionActions,
                        alert => alert.ID,
                        alertDefinitionAction => alertDefinitionAction.AlertDefinitionID,
                        (alert, alertDefinitionActions) => { alert.Actions = alertDefinitionActions.Where(l => l.AlertDefinitionID == alert.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allSystemAutomationTriggerDefinitions,
                        alert => alert.TriggerID,
                        systemAutomationTriggerDefinition => systemAutomationTriggerDefinition.ID,
                        (alert, systemAutomationTriggerDefinitions) => { alert.Trigger = systemAutomationTriggerDefinitions.Where(l => l.ID == alert.TriggerID).FirstOrDefault(); }
                    ).ToList();
            return resultList;
        }

        public static List<AssemblyCategorySearchModel> parseAssemblyCategorySearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<AssemblyCategorySearchModel>().ToList();
            return resultList;
        }

        public static List<AssemblyDataSearchModel> parseAssemblyDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<AssemblyDataSearchModel>().ToList();
            var allAssemblyVariables = gridReader.Read<AssemblyVariable>().ToList();
            var allAssemblyCategories = gridReader.Read<AssemblyCategoryLink>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allAssemblyVariables,
                        assemblyData => assemblyData.ID,
                        variable => variable.SubassemblyID,
                        (assemblyData, alertDefinitionActions) => { assemblyData.Variables = alertDefinitionActions.Where(l => l.SubassemblyID == assemblyData.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allAssemblyCategories,
                        assemblyData => assemblyData.ID,
                        assemblyCategory => assemblyCategory.ParentID,
                        (assemblyData, assemblyCategories) => { assemblyData.AssemblyCategories = assemblyCategories.Where(l => l.ParentID == assemblyData.ID).ToList(); }
                    ).ToList();
            return resultList;
        }

        public static List<BoardDefinitionDataSearchModel> parseBoardDefinitionDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<BoardDefinitionDataSearchModel>().ToList();
            var allModuleLinks = gridReader.Read<BoardModuleLink>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allModuleLinks,
                        boardDefinition => boardDefinition.ID,
                        moduleLink => moduleLink.ParentID,
                        (boardDefinition, moduleLinks) => { boardDefinition.ModuleLinks = moduleLinks.Where(l => l.ParentID == boardDefinition.ID).ToList(); }
                    ).ToList();
            return resultList;
        }

        public static List<CompanyDataSearchModel> parseCompanyDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<CompanyDataSearchModel>().ToList();
            var allContacts = gridReader.Read<CompanyContactLink>().ToList();
            var allEmailAddresses = gridReader.Read<IDStringValueObject>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allContacts,
                        company => company.ID,
                        contact => contact.ParentID,
                        (company, contacts) => { company.CompanyContactLinks = contacts.Where(l => l.ParentID == company.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allEmailAddresses,
                        company => company.CompanyContactLinks?.FirstOrDefault().ID,
                        emailAddress => emailAddress.ID,
                        (company, emailAddresses) => { company.EmailAddress = emailAddresses.FirstOrDefault(l => l.ID == company.CompanyContactLinks?.FirstOrDefault().ID).Value; }
                    ).ToList();
            return resultList;
        }

        public static List<ContactDataSearchModel> parseContactDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<ContactDataSearchModel>().ToList();
            var allContactLocators = gridReader.Read<ContactLocator>().ToList();
            var allUserLinks = gridReader.Read<UserLink>().ToList();
            var allContactCompanyLinks = gridReader.Read<ContactCompanyLink>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allContactLocators,
                        contact => contact.ID,
                        locator => locator.ParentID,
                        (contact, locators) => { contact.ContactLocators = locators.Where(l => l.ParentID == contact.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allUserLinks,
                        contact => contact.ID,
                        userLink => userLink.ContactID.Value,
                        (contact, userLinks) => { contact.UserLinks = userLinks.Where(l => l.ContactID == contact.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allContactCompanyLinks,
                        contact => contact.ID,
                        link => link.ParentID,
                        (contact, companyContactLinks) => { contact.CompanyContactLinks = companyContactLinks.Where(l => l.ParentID == contact.ID).ToList(); }
                    ).ToList();
            return resultList;
        }

        public static List<CreditMemoDataSearchModel> parseCreditMemoDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<CreditMemoDataSearchModel>().ToList();
            var allContactRoles = gridReader.Read<OrderContactRole>().ToList();
            var allKeyDates = gridReader.Read<OrderKeyDate>().ToList();
            var allOrderOrderLinks = gridReader.Read<OrderOrderLink>().ToList();
            var allOrderEmployeeRoles = gridReader.Read<OrderEmployeeRole>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allContactRoles,
                        order => order.ID,
                        contactRole => contactRole.OrderID,
                        (order, contactRoles) => { order.ContactRoles = contactRoles.Where(l => l.OrderID == order.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allKeyDates,
                        order => order.ID,
                        keyDate => keyDate.OrderID,
                        (order, keyDates) => { order.Dates = keyDates.Where(l => l.OrderID == order.ID).ToList(); }
                    ).ToList();

            resultList = gridReader.MapChild
                    (
                        resultList,
                        allOrderOrderLinks,
                        order => order.ID,
                        orderOrderLink => orderOrderLink.OrderID,
                        (order, orderLinks) => { order.Links = orderLinks.Where(l => l.OrderID == order.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allOrderEmployeeRoles,
                                    order => order.ID,
                                    employeeRole => employeeRole.OrderID,
                                    (order, employeeRoles) => { order.EmployeeRoles = employeeRoles.Where(l => l.OrderID == order.ID).ToList(); }
                                ).ToList();
            return resultList;
        }

        public static List<CrmIndustrySearchModel> parseCrmIndustrySearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<CrmIndustrySearchModel>().ToList();
            return resultList;
        }

        public static List<CrmOriginSearchModel> parseCrmOriginSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<CrmOriginSearchModel>().ToList();
            return resultList;
        }

        public static List<CustomFieldDefinitionSearchModel> parseCustomFieldDefinitionSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<CustomFieldDefinitionSearchModel>().ToList();
            return resultList;
        }

        public static List<CustomFieldLayoutDefinitionSearchModel> parseCustomFieldLayoutDefinitionSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<CustomFieldLayoutDefinitionSearchModel>().ToList();
            return resultList;
        }

        public static List<DashboardDataSearchModel> parseDashboardDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<DashboardDataSearchModel>().ToList();
            return resultList;
        }

        public static List<DashboardWidgetDefinitionSearchModel> parseDashboardWidgetDefinitionSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<DashboardWidgetDefinitionSearchModel>().ToList();
            return resultList;
        }

        public static List<DestinationDataSearchModel> parseDestinationDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<DestinationDataSearchModel>().ToList();
            var allDestinationProfiles = gridReader.Read<DestinationProfile>().ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allDestinationProfiles,
                                    destination => destination.ID,
                                    destinationProfile => destinationProfile.DestinationID,
                                    (destination, employeeRoles) => { destination.DestinationProfiles = employeeRoles.Where(l => l.DestinationID == destination.ID).ToList(); }
                                ).ToList();
            return resultList;
        }

        public static List<DomainDataSearchModel> parseDomainDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<DomainDataSearchModel>().ToList();
            return resultList;
        }

        public static List<DomainEmailSearchModel> parseDomainEmailSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<DomainEmailSearchModel>().ToList();
            var allLocationLinks = gridReader.Read<DomainEmailLocationLink>().ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allLocationLinks,
                                    domainEmail => domainEmail.ID,
                                    locationLink => locationLink.ParentID,
                                    (domainEmail, locationLinks) => { domainEmail.LocationLinks = locationLinks.Where(l => l.ParentID == domainEmail.ID).ToList(); }
                                ).ToList();
            return resultList;
        }

        public static List<EmailAccountDataSearchModel> parseEmailAccountDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<EmailAccountDataSearchModel>().ToList();
            var allEmployeeTeams = gridReader.Read<EmployeeTeamLink>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allEmployeeTeams,
                        emailAccount => emailAccount.ID,
                        employeeTeam => employeeTeam.ParentID,
                        (emailAccount, employeeTeams) => { emailAccount.EmployeeTeams = employeeTeams.Where(l => l.ParentID == emailAccount.ID).ToList(); }
                    ).ToList();
            return resultList;
        }

        public static List<EmployeeDataSearchModel> parseEmployeeDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<EmployeeDataSearchModel>().ToList();
            var allEmployeeLocators = gridReader.Read<EmployeeLocator>().ToList();
            var allUserLinks = gridReader.Read<UserLink>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allEmployeeLocators,
                        employee => employee.ID,
                        locator => locator.ParentID,
                        (employee, locators) => { employee.EmployeeLocators = locators.Where(l => l.ParentID == employee.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allUserLinks,
                        employee => employee.ID,
                        userLink => userLink.EmployeeID.Value,
                        (employee, userLinks) => { employee.UserLinks = userLinks.Where(l => l.EmployeeID == employee.ID).ToList(); }
                    ).ToList();
            return resultList;
        }

        public static List<EmployeeTeamSearchModel> parseEmployeeTeamSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<EmployeeTeamSearchModel>().ToList();
            var allLocationLinks = gridReader.Read<EmployeeTeamLocationLink>().ToList();
            var allEmployeeLinks = gridReader.Read<EmployeeTeamEmployeeLink>().ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allLocationLinks,
                                    employeeTeam => employeeTeam.ID,
                                    locationLink => locationLink.ParentID,
                                    (employeeTeam, locationLinks) => { employeeTeam.EmployeeTeamLocationLinks = locationLinks.Where(l => l.ParentID == employeeTeam.ID).ToList(); }
                                ).ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allEmployeeLinks,
                                    employeeTeam => employeeTeam.ID,
                                    employeeLink => employeeLink.ParentID,
                                    (employeeTeam, employeeLinks) => { employeeTeam.EmployeeTeamLinks = employeeLinks.Where(l => l.ParentID == employeeTeam.ID).ToList(); }
                                ).ToList();
            return resultList;
        }

        public static List<EstimateDataSearchModel> parseEstimateDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<EstimateDataSearchModel>().ToList();
            var allContactRoles = gridReader.Read<OrderContactRole>().ToList();
            var allKeyDates = gridReader.Read<OrderKeyDate>().ToList();
            var allOrderOrderLinks = gridReader.Read<OrderOrderLink>().ToList();
            var allOrderEmployeeRoles = gridReader.Read<OrderEmployeeRole>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allContactRoles,
                        order => order.ID,
                        contactRole => contactRole.OrderID,
                        (order, contactRoles) => { order.ContactRoles = contactRoles.Where(l => l.OrderID == order.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allKeyDates,
                        order => order.ID,
                        keyDate => keyDate.OrderID,
                        (order, keyDates) => { order.Dates = keyDates.Where(l => l.OrderID == order.ID).ToList(); }
                    ).ToList();

            resultList = gridReader.MapChild
                    (
                        resultList,
                        allOrderOrderLinks,
                        order => order.ID,
                        orderOrderLink => orderOrderLink.OrderID,
                        (order, orderLinks) => { order.Links = orderLinks.Where(l => l.OrderID == order.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allOrderEmployeeRoles,
                                    order => order.ID,
                                    employeeRole => employeeRole.OrderID,
                                    (order, employeeRoles) => { order.EmployeeRoles = employeeRoles.Where(l => l.OrderID == order.ID).ToList(); }
                                ).ToList();
            return resultList;
        }

        public static List<LaborCategorySearchModel> parseLaborCategorySearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<LaborCategorySearchModel>().ToList();
            return resultList;
        }

        public static List<LaborDataSearchModel> parseLaborDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<LaborDataSearchModel>().ToList();
            var allLaborCategories = gridReader.Read<LaborCategoryLink>().ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allLaborCategories,
                                    labor => labor.ID,
                                    laborCategories => laborCategories.ParentID,
                                    (labor, laborCategories) => { labor.LaborCategoryLinks = laborCategories.Where(l => l.ParentID == labor.ID).ToList(); }
                                ).ToList();

            return resultList;
        }

        public static List<LocationDataSearchModel> parseLocationDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<LocationDataSearchModel>().ToList();
            var allLocators = gridReader.Read<LocationLocator>().ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allLocators,
                                    labor => labor.ID,
                                    locator => locator.ParentID,
                                    (labor, locators) => { labor.LocationLocators = locators.Where(l => l.ParentID == labor.ID).ToList(); }
                                ).ToList();
            return resultList;
        }

        public static List<MachineCategorySearchModel> parseMachineCategorySearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<MachineCategorySearchModel>().ToList();
            return resultList;
        }

        public static List<MachineDataSearchModel> parseMachineDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<MachineDataSearchModel>().ToList();
            var allLaborCategories = gridReader.Read<LaborCategoryLink>().ToList();
            var allMachineInstances = gridReader.Read<MachineInstance>().ToList();
            var allMachineProfiles = gridReader.Read<MachineProfile>().ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allLaborCategories,
                                    labor => labor.ID,
                                    laborCategories => laborCategories.ParentID,
                                    (labor, laborCategories) => { labor.MachineCategoryLinks = laborCategories.Where(l => l.ParentID == labor.ID).ToList(); }
                                ).ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allMachineInstances,
                                    machine => machine.ID,
                                    laborCategories => laborCategories.MachineID,
                                    (machine, laborCategories) => { machine.MachineInstances = laborCategories.Where(l => l.MachineID == machine.ID).ToList(); }
                                ).ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allMachineProfiles,
                                    machine => machine.ID,
                                    laborCategories => laborCategories.MachineID,
                                    (machine, laborCategories) => { machine.MachineProfiles = laborCategories.Where(l => l.MachineID == machine.ID).ToList(); }
                                ).ToList();
            return resultList;
        }

        public static List<MaterialCategorySearchModel> parseMaterialCategorySearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<MaterialCategorySearchModel>().ToList();
            return resultList;
        }

        public static List<MaterialDataSearchModel> parseMaterialDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<MaterialDataSearchModel>().ToList();
            var allLaborCategories = gridReader.Read<LaborCategoryLink>().ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allLaborCategories,
                                    labor => labor.ID,
                                    laborCategories => laborCategories.ParentID,
                                    (labor, laborCategories) => { labor.MaterialCategoryLinks = laborCategories.Where(l => l.ParentID == labor.ID).ToList(); }
                                ).ToList();

            return resultList;
        }

        public static List<MessageBodyTemplateSearchModel> parseMessageBodyTemplateSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<MessageBodyTemplateSearchModel>().ToList();
            return resultList;
        }

        public static List<MessageHeaderSearchModel> parseMessageHeaderSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<MessageHeaderSearchModel>().ToList();
            var allEmployeeTeams = gridReader.Read<MessageParticipantInfo>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allEmployeeTeams,
                        messageHeader => messageHeader.BodyID,
                        employeeTeam => employeeTeam.BodyID,
                        (messageHeader, messageParticipants) => { messageHeader.MessageParticipantInfos = messageParticipants.Where(l => l.BodyID == messageHeader.BodyID).ToList(); }
                    ).ToList();
            return resultList;
        }

        public static List<OrderDataSearchModel> parseOrderDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<OrderDataSearchModel>().ToList();
            var allContactRoles = gridReader.Read<OrderContactRole>().ToList();
            var allKeyDates = gridReader.Read<OrderKeyDate>().ToList();
            var allOrderOrderLinks = gridReader.Read<OrderOrderLink>().ToList();
            var allOrderEmployeeRoles = gridReader.Read<OrderEmployeeRole>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allContactRoles,
                        order => order.ID,
                        contactRole => contactRole.OrderID,
                        (order, contactRoles) => { order.ContactRoles = contactRoles.Where(l => l.OrderID == order.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allKeyDates,
                        order => order.ID,
                        keyDate => keyDate.OrderID,
                        (order, keyDates) => { order.Dates = keyDates.Where(l => l.OrderID == order.ID).ToList(); }
                    ).ToList();

            resultList = gridReader.MapChild
                    (
                        resultList,
                        allOrderOrderLinks,
                        order => order.ID,
                        orderOrderLink => orderOrderLink.OrderID,
                        (order, orderLinks) => { order.Links = orderLinks.Where(l => l.OrderID == order.ID).ToList(); }
                    ).ToList();
            resultList = gridReader.MapChild
                                (
                                    resultList,
                                    allOrderEmployeeRoles,
                                    order => order.ID,
                                    employeeRole => employeeRole.OrderID,
                                    (order, employeeRoles) => { order.EmployeeRoles = employeeRoles.Where(l => l.OrderID == order.ID).ToList(); }
                                ).ToList();
            return resultList;
        }

        public static List<OrderItemDataSearchModel> parseOrderItemDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<OrderItemDataSearchModel>().ToList();
            var allSurcharges = gridReader.Read<SurchargeDefLink>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allSurcharges,
                        orderItem => orderItem.ID,
                        surcharge => surcharge.ParentID,
                        (orderItem, surcharges) => { orderItem.Surcharges = surcharges.Where(l => l.ParentID == orderItem.ID).ToList(); }
                    ).ToList();
            return resultList;
        }

        public static List<PaymentApplicationDataSearchModel> parsePaymentApplicationDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<PaymentApplicationDataSearchModel>().ToList();
            return resultList;
        }

        public static List<PaymentMasterDataSearchModel> parsePaymentMasterDataSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<PaymentMasterDataSearchModel>().ToList();
            var allPaymentApplications = gridReader.Read<PaymentApplication>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allPaymentApplications,
                        payment => payment.ID,
                        applicaiton => applicaiton.MasterID,
                        (payment, apllications) => { payment.PaymentApplications = apllications.Where(l => l.MasterID == payment.ID).ToList(); }
                    ).ToList();
            return resultList;
        }

        public static List<QuickItemSearchModel> parseQuickItemSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<QuickItemSearchModel>().ToList();
            var allCategories = gridReader.Read<CategoryLink>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allCategories,
                        quickItem => quickItem.ID,
                        Category => Category.ParentID,
                        (quickItem, categories) => { quickItem.CategoryLinks = categories.Where(l => l.ParentID == quickItem.ID).ToList(); }
                    ).ToList();
            return resultList;
        }

        public static List<ReconciliationSearchModel> parseReconciliationSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<ReconciliationSearchModel>().ToList();
            return resultList;
        }

        public static List<RightsGroupListSearchModel> parseRightsGroupListSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<RightsGroupListSearchModel>().ToList();
            var allUserLinks = gridReader.Read<UserLink>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allUserLinks,
                        rightsGroup => rightsGroup.ID,
                        userLink => userLink.RightsGroupListID.Value,
                        (rightsGroup, userLinks) => { rightsGroup.UserLinks = userLinks.Where(l => l.RightsGroupListID.Value == rightsGroup.ID).ToList(); }
                    ).ToList();
            return resultList;
        }

        public static List<SurchargeDefSearchModel> parseSurchargeDefSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<SurchargeDefSearchModel>().ToList();
            return resultList;
        }

        public static List<TaxabilityCodeSearchModel> parseTaxabilityCodeSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<TaxabilityCodeSearchModel>().ToList();
            var allExemtionLinks = gridReader.Read<AccountingTaxCodeItemExemptionLInk>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allExemtionLinks,
                        taxCode => taxCode.ID,
                        exemptionLink => exemptionLink.ParentID,
                        (taxCode, userLinks) => { taxCode.TaxabilityCodeItemExemptionLinks = userLinks.Where(l => l.ParentID == taxCode.ID).ToList(); }
                    ).ToList();
            return resultList;
        }

        public static List<UserDraftSearchModel> parseUserDraftSearchModelResults(SqlMapper.GridReader gridReader)
        {
            var resultList = gridReader.Read<UserDraftSearchModel>().ToList();
            var allUserLinks = gridReader.Read<UserLink>().ToList();
            resultList = gridReader.MapChild
                    (
                        resultList,
                        allUserLinks,
                        userDraft => userDraft.UserLinkID,
                        userLink => userLink.ID,
                        (userDraft, userLinks) => { userDraft.UserLink = userLinks.FirstOrDefault(l => l.ID == userDraft.UserLinkID); }
                    ).ToList();
            return resultList;
        }



        public List<WildcardSearchModel> parseGridReaderResults(SqlMapper.GridReader gridReader)
        {
            throw new System.NotImplementedException();
        }
    }
}
