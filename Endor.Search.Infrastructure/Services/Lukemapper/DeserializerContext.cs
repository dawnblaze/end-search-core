﻿using Lucene.Net.Documents;
using Lucene.Net.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Search.infrastructure.Services.Lukemapper
{
    public class DeserializerContext<T>
    {
        public readonly IndexSearcher Searcher;
        private Func<Document, object> _deserializer;
        private Func<Func<Document, object>> getCachedDeserializer;

        public DeserializerContext(IndexSearcher searcher, Query query)
        {
            this.Searcher = searcher;
            //****: create lambda to generate deserializer method, then cache it
            //****: we do this here in case the underlying schema has changed we can regenerate...
            var identity = new Identity(searcher, query, typeof(T));
            var info = LukeMapper.GetDeserializerCacheInfo(identity);
            this.getCachedDeserializer = () =>
            {
                info.Deserializer = LukeMapper.GetDeserializer(typeof(T), searcher);
                LukeMapper.SetQueryCache(identity, info);
                return info.Deserializer;
            };

            //****: check info for deserializer, if null => run it.
            if (info.Deserializer == null)
            {
                this._deserializer = getCachedDeserializer();
            }
            else
            {
                this._deserializer = info.Deserializer;
            }
        }

        public T Deserialize(Document document)
        {
            T result;
            try
            {
                result = (T)this._deserializer(document);
            }
            catch (DataException)
            {
                // give it another shot, in case the underlying schema changed
                this._deserializer = this.getCachedDeserializer();
                result = (T)this._deserializer(document);
            }
            return result;
        }
    }
}
