﻿using Lucene.Net.Documents;
using Attributes = Endor.Search.Core.SharedKernal.LukeMapper.Attributes;

namespace Endor.Search.infrastructure.Services.Lukemapper
{
    public static class EnumExtensions
    {
        public static Field.Store ToFieldStore(this Attributes.Store store)
        {
            switch (store)
            {
                case Attributes.Store.NO: return Field.Store.NO;
                //case Store.COMPRESS: return Field.Store.COMPRESS;
                default: return Field.Store.YES;
            }
        }

        public static System.Reflection.FieldInfo ToFieldInfo(this Attributes.Store store)
        {
            switch (store)
            {
                case Attributes.Store.NO: return typeof(Field.Store).GetField("NO");
                //case Store.COMPRESS: return typeof(Field.Store).GetField("COMPRESS");
                default: return typeof(Field.Store).GetField("YES");
            }
        }

        public static System.Reflection.Emit.OpCode ToOpCode(this Attributes.Store store)
        {
            switch (store)
            {
                case Attributes.Store.NO:
                    return System.Reflection.Emit.OpCodes.Ldc_I4_1;
                //case Store.COMPRESS: return typeof(Field.Store).GetField("COMPRESS");
                default:
                    return System.Reflection.Emit.OpCodes.Ldc_I4_0;
            }
        }

        public static Field.Index ToFieldIndex(this Attributes.Index store)
        {
            switch (store)
            {
                case Attributes.Index.NO: return Field.Index.NO;
                case Attributes.Index.ANALYZED: return Field.Index.ANALYZED;
                case Attributes.Index.ANALYZED_NO_NORMS: return Field.Index.ANALYZED_NO_NORMS;
                case Attributes.Index.NOT_ANALYZED: return Field.Index.NOT_ANALYZED;
                default: return Field.Index.NOT_ANALYZED_NO_NORMS;
            }
        }

        public static System.Reflection.Emit.OpCode ToOpCode(this Attributes.Index store)
        {
            switch (store)
            {
                case Attributes.Index.NO:
                    return System.Reflection.Emit.OpCodes.Ldc_I4_0;
                case Attributes.Index.ANALYZED:
                    return System.Reflection.Emit.OpCodes.Ldc_I4_1;
                case Attributes.Index.ANALYZED_NO_NORMS:
                    return System.Reflection.Emit.OpCodes.Ldc_I4_4;
                case Attributes.Index.NOT_ANALYZED:
                    return System.Reflection.Emit.OpCodes.Ldc_I4_2;
                default:
                    return System.Reflection.Emit.OpCodes.Ldc_I4_3;
            }
        }

        public static System.Reflection.FieldInfo ToFieldInfo(this Attributes.Index store)
        {
            switch (store)
            {
                case Attributes.Index.NO: return typeof(Field.Index).GetField("NO");
                case Attributes.Index.ANALYZED: return typeof(Field.Index).GetField("ANALYZED");
                case Attributes.Index.ANALYZED_NO_NORMS: return typeof(Field.Index).GetField("ANALYZED_NO_NORMS");
                case Attributes.Index.NOT_ANALYZED: return typeof(Field.Index).GetField("NOT_ANALYZED");
                default: return typeof(Field.Index).GetField("NOT_ANALYZED_NO_NORMS");
            }
        }
    }

}
