﻿using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Util;
using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Lucene.Net.Search.FieldCache;

namespace Endor.Search.Infrastructure.Services.Lukemapper
{
    //https://github.com/apache/lucenenet/blob/3.0.3/src/core/Search/FieldComparatorSource.cs
    public class ConstantIntComparatorSource : FieldComparerSource
    {
        private int comparisonConstant;
        public ConstantIntComparatorSource(int _comparisonConstant)
        {
            comparisonConstant = _comparisonConstant;
        }
        public override FieldComparer NewComparer(string fieldname, int numHits, int sortPos, bool reversed)
        {
            return new ConstantIntComparator(comparisonConstant, numHits, fieldname, reversed);
        }
    }

    //https://github.com/apache/lucenenet/blob/3.0.3/src/core/Search/FieldComparator.cs
    public sealed class ConstantIntComparator : FieldComparer
    {
        private int comparisonConstant;

        private int[] values;
        private Int32s currentReaderValues;
        private string field;
        private IInt32Parser parser;
        /// <summary>
        /// 1 is normal sort, -1 is reverse sort
        /// </summary>
        private byte reverseCoefficient = 1;
        /// <summary>
        /// Value of bottom of queue
        /// </summary>
        private int bottom;

        internal ConstantIntComparator(int _comparisonConstant, int numHits, string field, bool reversed)
        {
            comparisonConstant = _comparisonConstant;
            values = new int[numHits];
            this.field = field;
            reverseCoefficient = (byte)(reversed ? -1 : 1);
            this.parser = new ReimplementedIntParser();
        }

        public override int CompareTop(int doc)
        {
            throw new System.NotSupportedException();
        }

        public override void SetTopValue(object value)
        {
            throw new System.NotSupportedException();
        }

        public override int Compare(int slot1, int slot2)
        {
            // TODO: there are sneaky non-branch ways to compute
            // -1/+1/0 sign
            // Cannot return values[slot1] - values[slot2] because that
            // may overflow
            int v1 = values[slot1];
            int v2 = values[slot2];
            if (v1 == comparisonConstant && v2 != comparisonConstant)
            {
                return 1 * reverseCoefficient;
            }
            else if (v2 == comparisonConstant && v1 != comparisonConstant)
            {
                return -1 * reverseCoefficient;
            }
            else
            {
                return 0;
            }
        }
        public override int CompareValues(object first, object second)
        {
            return ((IComparable)first).CompareTo(second);
        }

        public override int CompareBottom(int doc)
        {
            // TODO: there are sneaky non-branch ways to compute
            // -1/+1/0 sign
            // Cannot return bottom - values[slot2] because that
            // may overflow
            int v2 = currentReaderValues.Get(doc);
            if (bottom == comparisonConstant && v2 != comparisonConstant)
            {
                return 1 * reverseCoefficient;
            }
            else if (v2 == comparisonConstant && bottom != comparisonConstant)
            {
                return -1 * reverseCoefficient;
            }
            else
            {
                return 0;
            }
        }

        public override void Copy(int slot, int doc)
        {
            values[slot] = currentReaderValues.Get(doc);
        }

        public override FieldComparer SetNextReader(AtomicReaderContext context)
        {
            currentReaderValues = Lucene.Net.Search.FieldCache.DEFAULT.GetInt32s(context.AtomicReader, field, parser, false);
            return this;
        }

        public override void SetBottom(int bottom)
        {
            this.bottom = values[bottom];
        }

        public override IComparable this[int slot]
        {
            get { return (System.Int32)values[slot]; }
        }
    }

    class ReimplementedIntParser : IInt32Parser
    {
        public virtual int ParseInt(System.String value_Renamed)
        {
            return System.Int32.Parse(value_Renamed);
        }
        /*protected internal virtual System.Object ReadResolve()
        {
            return Lucene.Net.Search.FieldCache.DEFAULT_INT32_PARSER;
        }
        public override System.String ToString()
        {
            return typeof(FieldCache).FullName + ".DEFAULT_INT_PARSER";
        }*/
        /// <summary>
        /// NOTE: This was parseInt() in Lucene
        /// </summary>
        public int ParseInt32(BytesRef term)
        {

            try
            {
                return int.Parse(term.Utf8ToString(), NumberStyles.Integer, CultureInfo.InvariantCulture);
                //return (term.Bytes[term.Offset] - 'A') * 123456;
            } catch (Exception)
            {
                return Int32.MinValue;
            }
        }

        public TermsEnum TermsEnum(Terms terms)
        {
            return terms.GetIterator(null);
        }
    }
}
