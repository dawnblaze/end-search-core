﻿using Lucene.Net.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Search.infrastructure.Services.Lukemapper
{
    public class PagedQueryResult<T>
    {
        public readonly int TotalHits;
        public readonly int SkipN;
        public readonly int TakeN;
        private readonly TopDocs _docs;
        private readonly DeserializerContext<T> _context;

        public PagedQueryResult(TopDocs docs, DeserializerContext<T> ctx, int skipN, int takeN)
        {
            this._docs = docs;
            this._context = ctx;
            this.TotalHits = docs.TotalHits;
            this.SkipN = skipN;
            this.TakeN = takeN;
        }

        public IEnumerable<T> Results()
        {
            foreach (var document in _docs.ScoreDocs.Skip(this.SkipN).Take(this.TakeN).Select(sd => _context.Searcher.Doc(sd.Doc)))
            {
                yield return this._context.Deserialize(document);
            }
        }
    }
}
