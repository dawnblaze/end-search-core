﻿using Endor.Search.infrastructure.Services.Lukemapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.infrastructure.Services
{
    public class PagedSearchResult<S>
    {
        public int TotalHits { get; private set; }
        public S[] Results { get; private set; }

        public PagedSearchResult(PagedQueryResult<S> queryResult)
        {
            this.TotalHits = queryResult?.TotalHits ?? 0;
            this.Results = queryResult?.Results()?.ToArray() ?? new S[] { };
        }
    }
}
