﻿using Lucene.Net.Analysis.Standard;
using Version = Lucene.Net.Util.LuceneVersion;
using Lucene.Net.Search;
using Lucene.Net.Index;
using System;
using System.Collections.Generic;
using System.Linq;
using Lucene.Net.QueryParsers.Classic;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using Endor.Search.infrastructure.Services.Lukemapper;

namespace Endor.Search.infrastructure.Services
{
    public static class SearchableExtensions
    {
        internal const char ParsedQueryCanaryChar = ':';
        internal const string DefaultFieldForSearch = "Summary";

        private static QueryParser _standardAnalyzer;
        private static QueryParser StandardParser
        {
            get
            {
                if (_standardAnalyzer == null)
                    _standardAnalyzer = CreateStandardQueryParser();

                return _standardAnalyzer;
            }
        }
        internal static QueryParser CreateStandardQueryParser()
        {
            return new QueryParser(Version.LUCENE_48, DefaultFieldForSearch, new StandardAnalyzer(Version.LUCENE_48));
        }

        private static Dictionary<Type, string[]> _searchableFieldMap = new Dictionary<Type, string[]>();

        private static string[] SearchableFields(Type givenType)
        {
            if (!_searchableFieldMap.ContainsKey(givenType))
            {
                _searchableFieldMap[givenType] = GetSearchableFields(givenType);
            }

            return _searchableFieldMap[givenType];
        }

        public static string[] GetSearchableFields(Type givenType)
        {
            List<string> propertyNames = new List<string>();

            foreach (var prop in givenType.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
            {
                var attr = prop.GetCustomAttributes(typeof(LukeAttribute), false).FirstOrDefault() as LukeAttribute;

                if (attr != null && !attr.Ignore && attr.Index != Core.SharedKernal.LukeMapper.Attributes.Index.NO)
                {
                    propertyNames.Add(prop.Name);
                }
            }

            return propertyNames.ToArray();
        }

        public static string StripDashes(this string mystring)
        {
            return mystring.Replace("-", String.Empty);
        }

        public static Query ConstructQuery<S>(this Type type, string query, bool allowAllIndexes = false)
        {
            if (String.IsNullOrWhiteSpace(query) && allowAllIndexes)
            {
                return new MatchAllDocsQuery();
            }
            //use the lucene rules
            else if (query != null && query.Contains(ParsedQueryCanaryChar))
            {
                var queries = query.Split(new string[] { " AND " }, StringSplitOptions.None).ToList();
                //remove empty queries
                queries = queries.Where(strQuery => !String.IsNullOrWhiteSpace(strQuery)).ToList();
                //for search pages that are still using old fulltext format, prepend "Text:"
                queries = queries.Select(strQuery => strQuery.Contains(ParsedQueryCanaryChar) ? strQuery : "Text:" + strQuery).ToList();

                BooleanQuery booleanQuery = new BooleanQuery();

                //property queries, e.g. "ClassTypeID:5000"
                var propertyQueries = queries.Where(strQuery => !strQuery.Contains("Text:"));
                foreach (var q in propertyQueries)
                {
                    string strQuery = q ?? "";//make sure its not null
                    strQuery = strQuery.Trim();

                    if (strQuery.Contains(" OR "))
                    {//e.g. "(EmployeeIDs:101 OR EmployeeIDs:102)"

                        var eitherQuery = new BooleanQuery();

                        var orSearchStrings = strQuery.TrimStart('(')//orSearch will now look like ["EmployeeIDs:101", "EmployeeIDs:102"]
                                                .TrimEnd(')')
                                                .Split(new string[] { " OR " }, StringSplitOptions.None)
                                                .ToList();

                        foreach (var str in orSearchStrings)
                        {
                            var searchTerm = str.ToSearchTerm();
                            eitherQuery.Add(
                                    new WildcardQuery(new Term(searchTerm.PropertyName, searchTerm.SearchText)),
                                    Occur.SHOULD
                                    );
                        }

                        if (eitherQuery.Clauses.Count > 0)
                        {
                            booleanQuery.Add(eitherQuery, Occur.MUST);
                        }
                    }
                    else
                    {
                        var propertyAndSearchTerm = strQuery.Split(ParsedQueryCanaryChar);
                        var propertyName = propertyAndSearchTerm[0];
                        var searchText = propertyAndSearchTerm[1]
                                            .ToLower()
                                            .Trim()//not sure why but for some reason lucene wont return anything if searchterms have uppercase chars
                                            .StripDashes();

                        var propertyQuery = new BooleanQuery();
                        propertyQuery.Add(new WildcardQuery(new Term(propertyName, searchText)), Occur.SHOULD);
                        //if a sorter is available, use the sorter instead of the actual indexed property
                        propertyQuery.Add(new WildcardQuery(new Term(propertyName + "Sort", searchText)), Occur.SHOULD);

                        booleanQuery.Add(propertyQuery, Occur.MUST);
                    }
                }

                //im putting this outside of the foreach loop to ensure only one full text query is constructed
                var fullTextQuery = queries.FirstOrDefault(strQuery => strQuery.Contains("Text:"));
                //sample value "Text:Tarzan Jungle"
                if (fullTextQuery != null)
                {
                    fullTextQuery = fullTextQuery.Trim();
                    string[] propertyAndSearchTerm = fullTextQuery.Split(ParsedQueryCanaryChar);
                    string propertyName = propertyAndSearchTerm[0];
                    string searchText = propertyAndSearchTerm[1];
                    IEnumerable<string> keywords = searchText.Split(' ')//separate the keywords "Tarzan" and "Jungle"
                                            .Where(strText => !String.IsNullOrWhiteSpace(strText))
                                            .Select(strText => strText.ToLower().Trim().StripDashes());

                    foreach (string keyword in keywords)
                    {
                        string actualSearch = keyword;
                        if (!actualSearch.StartsWith("*"))
                            actualSearch = "*" + actualSearch;
                        if (!actualSearch.EndsWith("*"))
                            actualSearch += "*";

                        booleanQuery.Add(new WildcardQuery(new Term(DefaultFieldForSearch, actualSearch)), Occur.MUST);
                    }

                }

                //var qryToString = booleanQuery.ToString();
                //Console.WriteLine(qryToString);

                return booleanQuery;
            }
            else //construct a "match any term" query, but _only_ on this type's fields that qualify
            {
                BooleanQuery booleanQuery = new BooleanQuery();

                foreach (string searchablePropertyName in GetSearchableFields(type))
                {
                    booleanQuery.Add(new TermQuery(new Term(searchablePropertyName, query ?? "")), Occur.SHOULD);
                }

                return booleanQuery;
            }
        }

        public static Query ConstructQueryOLD<S>(this Type type, string query, bool allowAllIndexes = false)
        {
            if (String.IsNullOrWhiteSpace(query) && allowAllIndexes)
            {
                return new MatchAllDocsQuery();
            }
            //use the lucene rules
            else if (query != null && query.Contains(ParsedQueryCanaryChar))
            {
                if (query.Contains(":*"))
                    StandardParser.AllowLeadingWildcard = true;

                return StandardParser.Parse(query);
            }
            else //construct a "match any term" query, but _only_ on this type's fields that qualify
            {
                BooleanQuery booleanQuery = new BooleanQuery();

                foreach (string searchablePropertyName in GetSearchableFields(type))
                {
                    booleanQuery.Add(new TermQuery(new Term(searchablePropertyName, query ?? "")), Occur.SHOULD);
                }

                return booleanQuery;
            }
        }

        public static PagedSearchResult<S> ToSearchResults<S>(this PagedQueryResult<S> queryResult)
        {
            return new PagedSearchResult<S>(queryResult);
        }






        //
        struct SearchTerm
        {
            public string PropertyName { get; set; }
            public string SearchText { get; set; }
        }

        static SearchTerm ToSearchTerm(this String self)
        {
            var propertyAndSearchTerm = self.Split(ParsedQueryCanaryChar);
            var propertyName = propertyAndSearchTerm[0];
            var searchText = propertyAndSearchTerm[1];

            return new SearchTerm()
            {
                PropertyName = propertyName,
                SearchText = searchText.ToLower()
            };
        }

        public static bool IsSubclassOfRawGeneric(this Type toCheck, Type generic)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur)
                {
                    return true;
                }
                toCheck = toCheck.BaseType;
            }
            return false;
        }
    }
}
