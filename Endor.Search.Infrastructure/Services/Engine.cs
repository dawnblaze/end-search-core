﻿using Endor.Search.Core.Interfaces;
using Endor.Search.infrastructure.Services.Lukemapper;
using Endor.Tenant;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Lucene.Net.Store.Azure;
using Microsoft.Azure.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Version = Lucene.Net.Util.LuceneVersion;

namespace Endor.Search.infrastructure.Services
{
    public class Engine<S>
        where S : IAutomappableSearchModel<S>, new()
    {
        private const int DefaultQueryResultCount = 10;
        private const string ClassTypeIDFieldName = "ClassTypeID";
        public readonly bool UseDevelopmentStorage = false;
        public string StorageConnectionString { get; private set; }

        public AzureDirectory Directory { get; private set; }

        public Engine(short bid, string blobStorageConnectionString, bool useDevelopmentStorage = false)
        {
            this.UseDevelopmentStorage = useDevelopmentStorage;
            this.Directory = CreateAzureDirectory(GetSearchContainerName(bid), blobStorageConnectionString);

            this.StorageConnectionString = blobStorageConnectionString;
        }

        public Engine(string indexName, string blobStorageConnectionString, bool useDevelopmentStorage = false)
        {
            this.UseDevelopmentStorage = useDevelopmentStorage;
            this.Directory = CreateAzureDirectory(indexName, blobStorageConnectionString);

            this.StorageConnectionString = blobStorageConnectionString;
        }

        private AzureDirectory CreateAzureDirectory(string indexName, string blobStorageConnectionString)
        {
            return new AzureDirectory(GetAccount(blobStorageConnectionString), indexName, new RAMDirectory());
        }

        public static async Task<Engine<S>> Create(short bid, ITenantDataCache cache, bool useDevelopmentStorage = false)
        {
            TenantData data = await cache.Get(bid);

            if (data == null)
                return null;
            else
            {
                return new Engine<S>(bid, data.StorageConnectionString, useDevelopmentStorage);
            }
        }

        private string GetSearchContainerName(short bid)
        {
            return $"bid{bid}";
        }

        public void Write(S model)
        {
            model.Map();
            using (IndexWriter indexWriter = GetIndexWriter(this.Directory))
            {
                indexWriter.Write(model);
                indexWriter.Commit();
                indexWriter.Flush(true, true);
            }
        }
        public void Update(S searchModel)
        {
            searchModel.Map();
            using (IndexWriter indexWriter = GetIndexWriter(this.Directory))
            {
                indexWriter.Update(searchModel.GetSearchKeyTerm(), searchModel);
                indexWriter.Commit();
                indexWriter.Flush(true, true);
            }
        }

        public void UpdateMany(ICollection<S> models)
        {
            using (IndexWriter indexWriter = GetIndexWriter(this.Directory))
            {
                foreach (var searchModel in models)
                {
                    searchModel.Map();
                    indexWriter.Update(searchModel.GetSearchKeyTerm(), searchModel);
                }

                indexWriter.Commit();
                indexWriter.Flush(true, true);
            }
        }

        //used on full ct calls
        public void DeleteExistingAndWriteMany(int classTypeID, IEnumerable<S> models)
        {
            _writeMany(models, (writer) =>
            {
                writer.DeleteDocuments(new Term(ClassTypeIDFieldName, classTypeID.ToString()));
                writer.Commit();
            });
        }

        public void WriteMany(IEnumerable<S> models)
        {
            _writeMany(models);
        }

        private void _writeMany(IEnumerable<S> models, Action<IndexWriter> doBefore = null)
        {
            using (IndexWriter indexWriter = GetIndexWriter(this.Directory))
            {
                if (doBefore != null)
                    doBefore(indexWriter);

                foreach (var model in models.ToList())
                {
                    model.Map();
                    indexWriter.Write(model);
                }
                indexWriter.Commit();
                indexWriter.Flush(true, true);
            }
        }

        public void DeleteIfExists(int ctid, int id)
        {
            using (IndexWriter indexWriter = GetIndexWriter(this.Directory))
            {
                indexWriter.DeleteDocuments(GetCTIDAndIDClause(ctid, id));
                indexWriter.Commit();
                indexWriter.Flush(true, true);
            }
        }

        public void DeleteIfExistsMany(int ctid, ICollection<int> ids)
        {//DeleteIfExistsMany|start
            using (IndexWriter indexWriter = GetIndexWriter(this.Directory))
            {
                foreach (var id in ids)
                {
                    indexWriter.DeleteDocuments(GetCTIDAndIDClause(ctid, id));
                }

                indexWriter.Commit();
                indexWriter.Flush(true, true);
            }
        }//DeleteIfExistsMany|end

        private static BooleanQuery GetCTIDAndIDClause(int ctid, int id)
        {
            BooleanQuery whereCTIDAndID = new BooleanQuery();
            //don't include BID because we're "silo"'d - the whole index is only one BID
            //whereCTIDAndID.Add(new TermQuery(new Term("BID", this.bid.ToString())), Occur.MUST);
            whereCTIDAndID.Add(new TermQuery(new Term(ClassTypeIDFieldName, ctid.ToString())), Occur.MUST);
            whereCTIDAndID.Add(new TermQuery(new Term("ID", id.ToString())), Occur.MUST);
            return whereCTIDAndID;
        }

        public List<S> Query(string search, int hitsPerPage = DefaultQueryResultCount)
        {
            using (IndexReader reader = DirectoryReader.Open(this.Directory))
            {
                IndexSearcher searcher = new IndexSearcher(reader);

                Query q = typeof(S).ConstructQuery<S>(search, true);

                return searcher.Query<S>(q, hitsPerPage).ToList();
            }
        }

        public PagedSearchResult<S> Query(string search, int skip, int take, int maxHits = Int32.MaxValue, string sortBy = null)
        {
            using (IndexReader reader = DirectoryReader.Open(this.Directory))
            {
                IndexSearcher searcher = new IndexSearcher(reader);

                Query q = typeof(S).ConstructQuery<S>(search, true);
                return searcher.PagedQuery<S>(q, maxHits, skip, take, sortBy).ToSearchResults();
            }
        }

        public List<dynamic> QueryDynamic(string search, int hitsPerPage = DefaultQueryResultCount)
        {
            using (IndexReader reader = DirectoryReader.Open(this.Directory))
            {
                IndexSearcher searcher = new IndexSearcher(reader);

                Query q = GetDynamicUntypedQuery(search);

                return searcher.Query<dynamic>(q, hitsPerPage).ToList();
            }
        }

        private Query GetDynamicUntypedQuery(string query)
        {
            if (String.IsNullOrWhiteSpace(query))
            {
                return new MatchAllDocsQuery();
            }
            //use the lucene rules
            else if (query != null && query.Contains(SearchableExtensions.ParsedQueryCanaryChar))
            {
                return SearchableExtensions.CreateStandardQueryParser().Parse(query);
            }
            else
            {
                return new TermQuery(new Term(SearchableExtensions.DefaultFieldForSearch, query));
            }
        }

        private static IndexWriter GetIndexWriter(AzureDirectory azureDirectory)
        {
            return new IndexWriter(azureDirectory, new IndexWriterConfig(Version.LUCENE_48, new StandardAnalyzer(Version.LUCENE_48)));
        }

        private CloudStorageAccount GetAccount(string blobStorageConnectionString)
        {
            if (UseDevelopmentStorage)
            {
                return CloudStorageAccount.DevelopmentStorageAccount;
            }
            else
            {
                return CloudStorageAccount.Parse(blobStorageConnectionString);
            }
        }
    }
}
