﻿using Dapper;
using Endor.Search.Core;
using Endor.Search.Core.Interfaces;
using Endor.Search.Core.SharedKernal;
using Endor.Search.infrastructure.Services;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Endor.Search.infrastructure.Common
{
    public class DapperRepository : IDapperReadOnlyRepository
    {
        protected string _tableName;
        protected short _BID;
        private string _connectionString;
        private readonly IMemoryCache _cache;
        public virtual IDbConnection Connection()
        {
            return new SqlConnection(_connectionString);
        }

        public DapperRepository(short BID, string tableName, string connectionString, IMemoryCache cache)
        {
            this._BID = BID;
            this._tableName = tableName;
            this._connectionString = connectionString;
            this._cache = cache;
        }

        protected IQueryable<T> ApplySpecification<T>(IQueryable<T> inputQuery, ISpecification<T> spec) where T : class
        {
            var query = SpecificationEvaluator<T>.GetQuery(inputQuery, spec);

            return query;
        }

        public void SetTableName(string tableName)
        {
            _tableName = tableName;
        }

        public virtual List<T> List<T>(ISpecification<T> spec = null)
            where T : class
        {
            IEnumerable<T> items = null;

            using (IDbConnection cn = Connection())
            {
                cn.Open();
                items = cn.Query<T>("SELECT * FROM " + _tableName);
            }

            return ApplySpecification(items.AsQueryable(), spec).ToList();
        }

        public async Task<List<T>> GetSearchModelByIdAsync<T>(int[] IDs) where T : IAutomappableSearchModel<T>
        {
            var typeName = typeof(T).Name;
            var sqlString = "";
            if (!_cache.TryGetValue(typeName, out sqlString))
            {
                var tableName = ClassTypeIDMap.MapForClassType(typeof(T)).TableName;
                sqlString = CreateSqlStmt(typeof(T), tableName);
                _cache.Set(typeName, sqlString);
            }
            var resultList = new List<T>();
            using (var connection = this.Connection())
            {
                using (var multi = await connection.QueryMultipleAsync(sqlString,
                    new
                    {
                        BID = _BID,
                        IDs = IDs,
                    }))
                {
                    var method = typeof(ParseGridReaderResults).GetMethod("parse" + typeName + "Results");
                    var result = method.Invoke(null, new[] { multi });
                    resultList = (List<T>)result;
                }
            }
            return resultList;
        }

        private static string GetWhereClause(Type type)
        {
            var thisTableReference = type.Name.Replace("SearchModel", "");
            var bidProperty = type.GetProperty("BID");
            var hasDBIgnore = Attribute.IsDefined(bidProperty, typeof(DBIgnoreAttribute));
            var whereClause = " Where " + (!hasDBIgnore ? thisTableReference + ".BID = @BID and " : "") + thisTableReference + ".ID IN @IDs";
            var additionalSQLFilter = type.GetProperty("AdditionalSqlFilter")?.GetValue(null)?.ToString();
            if (additionalSQLFilter != null)
            {
                whereClause += " AND " + additionalSQLFilter;
            }
            return whereClause;
        }

        private static string CreateSqlStmt(Type type, string baseTableName)
        { 
            var thisTableReference = type.Name.Replace("SearchModel", "");
            var tableName = baseTableName + " " + thisTableReference;
            var whereClause = GetWhereClause(type);
            var selectStatement = "";
            var joinStatement = " ";
            var GetProperties = type.GetProperties();
            var additionalStmts = ""; //used for child collection or complex mapping logic
            foreach (var property in GetProperties)
            {
                //Ignore anything with the DBIgnore attribute
                var hasDBIgnore = Attribute.IsDefined(property, typeof(DBIgnoreAttribute));
                if (!hasDBIgnore)
                {
                    var propertyName = property.Name;
                    var hasParentTableMapping = Attribute.IsDefined(property, typeof(ParentFieldMapAttribute));
                    if (hasParentTableMapping)
                    {
                        var parentFieldMappingAttribute = (ParentFieldMapAttribute)Attribute.GetCustomAttribute(property, typeof(ParentFieldMapAttribute));
                        selectStatement += (parentFieldMappingAttribute.GetParentTableIdentifier + "." + parentFieldMappingAttribute.GetParentPropertyName + " AS " + propertyName + ",");
                        if (!joinStatement.Contains(parentFieldMappingAttribute.GetParentTableIdentifier)) //don't include if it's already included
                            joinStatement += parentFieldMappingAttribute.GetJoinStmt(thisTableReference);
                    }
                    else
                    {
                        var hasChildCollectionMapping = Attribute.IsDefined(property, typeof(ChildCollectionAttribute));
                        var hasLinkCollectionAttribute = Attribute.IsDefined(property, typeof(LinkCollectionAttribute));
                        var hasSiblingCollectionAttribrute = Attribute.IsDefined(property, typeof(SiblingCollectionAttribute));
                        var hasAdditionalParentAttribute = Attribute.IsDefined(property, typeof(AdditionalParentAttribute));
                        var hasCustomSqlAttribute = Attribute.IsDefined(property, typeof(CustomSqlAttribute));

                        if ((hasSiblingCollectionAttribrute)||(hasChildCollectionMapping)||(hasLinkCollectionAttribute)|| hasAdditionalParentAttribute || (hasCustomSqlAttribute))
                        {
                            if (hasCustomSqlAttribute) {
                                var customFieldAttribute = (CustomSqlAttribute)Attribute.GetCustomAttribute(property, typeof(CustomSqlAttribute));
                                additionalStmts += ";\n" + customFieldAttribute.GetCustomStmt;
                            } else 
                                additionalStmts += CreateAdditionalSqlStmt(property,baseTableName);
                        } else
                        { 

                            var hasMapDBFieldAttibute = Attribute.IsDefined(property, typeof(MapDBFieldAttribute));
                            if (hasMapDBFieldAttibute)
                            {
                                var mapDBFieldAttribute = (MapDBFieldAttribute)Attribute.GetCustomAttribute(property, typeof(MapDBFieldAttribute));
                                selectStatement += thisTableReference + "." + mapDBFieldAttribute.DBFieldName + " AS " + propertyName + ",";
                            }
                            else
                                selectStatement += thisTableReference + "." + propertyName + " AS " + propertyName + ",";
                        }
                    }
                }
            }
            selectStatement = selectStatement.TrimEnd(',');
            var sqlString = "Select " + selectStatement + " FROM " + tableName + joinStatement + whereClause;
            return sqlString + additionalStmts;
        }

        private static string CreateAdditionalSqlStmt(PropertyInfo property, string baseTableName)
        {
            var hasChildCollectionMapping = Attribute.IsDefined(property, typeof(ChildCollectionAttribute));
            var hasLinkCollectionAttribute = Attribute.IsDefined(property, typeof(LinkCollectionAttribute));
            var hasSiblingCollectionAttribrute = Attribute.IsDefined(property, typeof(SiblingCollectionAttribute));
            var hasAdditionalParentAttribute = Attribute.IsDefined(property, typeof(AdditionalParentAttribute));

            if (hasChildCollectionMapping)
            {
                var childCollectionAttribute = (ChildCollectionAttribute)Attribute.GetCustomAttribute(property, typeof(ChildCollectionAttribute));
                var tuple = GetChildTableProperties(property, "source", baseTableName);
                var childSelectStmt = tuple.Item1;
                var joinStmt = tuple.Item2;
                return ";\n Select " + childSelectStmt + childCollectionAttribute.GetFromStmt(baseTableName, joinStmt, "source");

            }
            if (hasLinkCollectionAttribute)
            {
                var linkCollectionAttribute = (LinkCollectionAttribute)Attribute.GetCustomAttribute(property, typeof(LinkCollectionAttribute));
                var childSelectStmt = GetChildTableProperties(property,"st").Item1 + "," + linkCollectionAttribute.GetExtraSelectParamters();
                return ";\n Select " + childSelectStmt + linkCollectionAttribute.GetFromStmt(baseTableName);
            }

            if (hasSiblingCollectionAttribrute)
            {
                var siblingCollectionAttribute = (SiblingCollectionAttribute)Attribute.GetCustomAttribute(property, typeof(SiblingCollectionAttribute));
                var siblingSelectStmt = GetChildTableProperties(property).Item1;
                return ";\n Select " + siblingSelectStmt + siblingCollectionAttribute.GetFromStmt(baseTableName);
            }
            if (hasAdditionalParentAttribute)
            {
                var additionalParentAttribute = (AdditionalParentAttribute)Attribute.GetCustomAttribute(property, typeof(AdditionalParentAttribute));
                var tuple = GetChildTableProperties(property, "source", baseTableName);
                var siblingSelectStmt = tuple.Item1;
                var joinStmt = tuple.Item2;
                return ";\n Select " + siblingSelectStmt + additionalParentAttribute.GetFromStmt(baseTableName, joinStmt, "source");
            }
            throw new Exception("Attibute type was not parsed correctly");
        }
        /*
         * return a Tuple of select statement,join statement
         */
        private static Tuple<string,string> GetChildTableProperties(PropertyInfo property, string childColumnPrefix = "", string baseTableName = "")
        {

            var childObjectType = property.PropertyType;
            if (childObjectType.IsGenericType)
                childObjectType = ListArgument(property.PropertyType);
            var childProperties = childObjectType.GetProperties();
            var childSelectStmt = "";
            var joinStmt = "";
            foreach (var childProperty in childProperties)
            {
                var hasDBIgnore = Attribute.IsDefined(childProperty, typeof(DBIgnoreAttribute));
                if (hasDBIgnore)
                {
                    //do Nothing
                }
                else
                {
                    var hasParentTableMapping = Attribute.IsDefined(childProperty, typeof(ParentFieldMapAttribute));
                    if (hasParentTableMapping)
                    {
                        var parentFieldMappingAttribute = (ParentFieldMapAttribute)Attribute.GetCustomAttribute(childProperty, typeof(ParentFieldMapAttribute));
                        childSelectStmt += (parentFieldMappingAttribute.GetParentTableIdentifier + "." + parentFieldMappingAttribute.GetParentPropertyName + " AS " + childProperty.Name + ",");
                        joinStmt += parentFieldMappingAttribute.GetJoinStmt(childColumnPrefix);
                    }
                    else
                        childSelectStmt += String.IsNullOrWhiteSpace(childColumnPrefix) ? childProperty.Name + "," : childColumnPrefix + '.' + childProperty.Name + ",";
                }
            }
            childSelectStmt = childSelectStmt.TrimEnd(',');
            return Tuple.Create<string,string>(childSelectStmt, joinStmt);
        }

        private static Type ListArgument(Type type)
        {
            var listType = typeof(List<>);
            var collectionType = typeof(ICollection<>);
            var thisType = type.GetGenericTypeDefinition();
            if ((listType != thisType) && (collectionType != thisType))
                throw new Exception("Only List<T> are allowed");
            return type.GetGenericArguments()[0];
        }

        public async Task<List<int>> GetAllIDsForClassType(int bid, Type type)
        {
            var tableName = ClassTypeIDMap.MapForClassType(type).TableName;
            List<int> ids = new List<int>();
            using (var connection = this.Connection())
            {
                var additionalSQLFilter = type.GetProperty("AdditionalSqlFilter")?.GetValue(null)?.ToString();
                var thisTableReference = type.Name.Replace("SearchModel", "");
                var whereClause = $" where BID = {bid}";
                var tableIdentifier = " " + thisTableReference;
                if (additionalSQLFilter != null)
                {
                    whereClause += " AND " + additionalSQLFilter; 
                }
                var selectStmt = " Select ID FROM " + tableName + tableIdentifier + whereClause;
                ids = (await connection.QueryAsync<int>(selectStmt)).ToList();
            }
            return ids;
        }
    }

    public interface IDapperReadOnlyRepository : IReadOnlyRepository
    {
        public void SetTableName(string tableName);

        List<T> List<T>(ISpecification<T> spec) where T : class;

        IDbConnection Connection();

        Task<List<int>> GetAllIDsForClassType(int bid, Type type);
    }
}
