﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class EmailAccountSearchModelTests : BaseEntitySearchTestClass
    {
        [TestMethod]
        public async Task TestEmailAccountSearchModel()
        {
            var repo = ((TestDapperRepository)GetDapperRepository());

            //Arrange
            var locationID = GetValidLocationID(repo);

            await DeleteSqlExec(repo, ChildObjectTableInfo.EmailAccountTeamLinkTableName, "TeamID", defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.DomainEmailLocationLinkTableName, "DomainID", defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.EmployeeTeamTableName, ChildObjectTableInfo.IDColumn, defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.EmailAccountTableName, "DomainEmailID", defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.EmailAccountTableName, ChildObjectTableInfo.IDColumn, defaultParam); await DeleteSqlExec(repo, ChildObjectTableInfo.DomainEmailAccountTableName, ChildObjectTableInfo.IDColumn, defaultParam);

            var expected4 = new
            {
                BID = BID,
                ID = IDToUse,
                IsActive = true,
                DomainName = "Test",
                CreatedDate = DateTime.Now,
                ModifiedDT = DateTime.Now,
                ProviderType = 0
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.DomainEmailAccountTableName, expected4);
            
            var expected = new 
            {
                BID = BID,
                ID = IDToUse,
                StatusType = 2,
                DomainEmailID = IDToUse,
                UserName = "Test",
                DomainName = "Test Domain",
                //Credentials = "test",
                AliasUserNames = "test",
                DisplayName = "test",
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.EmailAccountTableName, expected);

            var expected2 = new
            {
                BID = BID,
                ID = IDToUse,
                IsActive = true,
                Name = "Test"
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.EmployeeTeamTableName, expected2);

            var expected3 = new
            {
                BID = BID,
                EmailAccountID = IDToUse,
                TeamID = IDToUse
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.EmailAccountTeamLinkTableName, expected3);

            var resultList = await repo.GetSearchModelByIdAsync<EmailAccountDataSearchModel>(new int[] { IDToUse });

            var engy = new Engine<EmailAccountDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            engy.Write(resultList.FirstOrDefault());

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query("BID:1");
            Assert.IsTrue(singleResults.Count == 1);

            singleResults = engy.Query("BID:1 AND test");
            Assert.IsTrue(singleResults.Count == 1);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.IsTrue(actual.ClassTypeID > 0);
            Assert.AreEqual(expected.ID, actual.ID);
            Assert.AreEqual(expected.DisplayName, actual.DisplayName);

            var NoResults = engy.Query("BID:0 AND test");
            Assert.IsTrue(NoResults.Count == 0);

            NoResults = engy.Query("BID:1 AND NotFound");
            Assert.IsTrue(NoResults.Count == 0);
        }
    }
}
