﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class MachineCategorySearchModelTests : BaseEntitySearchTestClass
    {
        [TestMethod]
        public async Task TestMachineCategorySearchModel()
        {
            var repo = ((TestDapperRepository)GetDapperRepository());

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.MachineCategoryTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            var expected = new
            {
                BID = BID,
                ID = IDToUse,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                Name = "UNITTESTMachineCategorySearchModel"
            };

            await InsertSqlExec(repo, ChildObjectTableInfo.MachineCategoryTableName, expected);

            var resultList = await repo.GetSearchModelByIdAsync<MachineCategorySearchModel>(new int[] { IDToUse });
            var engy = new Engine<MachineCategorySearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());
            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            var singleResults = engy.Query($"BID:1 AND Name:UNITTESTMachineCategorySearchModel");
            Assert.IsTrue(singleResults.Count == 1);
            var noResult = engy.Query($"BID:1 AND Name:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ID, actual.ID);
        }
    }
}
