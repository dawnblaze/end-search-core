﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class CustomFieldSearchModelTests : BaseEntitySearchTestClass
    {
        [TestMethod]
        public async Task TestCustomFieldDefinitionSearchModel() 
        {
            var repo = ((TestDapperRepository)GetDapperRepository());
            string tableName = ChildObjectTableInfo.CustomFieldDefinitionTableName;

            await DeleteSqlExec(repo,
                tableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            await DeleteSqlExec(repo,
                tableName,
                ChildObjectTableInfo.IDColumn,
                new
                {
                    BID,
                    ID = IDToUse + 1,
                });

            await DeleteSqlExec(repo,
                tableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            var expected = new
            {
                BID,
                ID = IDToUse,
                AllowMultipleValues = 0,
                AppliesToClassTypeID = 2000,
                DataType = 2,
                Description = "Generated from Unit Testing",
                HasValidators = 0,
                IsActive = 1,
                IsSystem = 0,
                Label = "UNIT TEST Custom Field Definition Search Model",
                ModifiedDT = DateTime.UtcNow,
                Name = "UNITTESTCustomFieldDefinitionSearchModel",
                IsRequired = 0,
                DisplayType = 1,
                ElementType = 1,
                ElementUseCount = 0,
                IsDisabled = 0
            };

            await InsertSqlExec(repo, tableName, expected);

            var resultList = await repo.GetSearchModelByIdAsync<CustomFieldDefinitionSearchModel>(new int[] { IDToUse });
            var engy = new Engine<CustomFieldDefinitionSearchModel>(myTestRunIndexName,
                MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());
            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            var singleResults = engy.Query($"BID:1 AND Name:UNITTESTCustomFieldDefinitionSearchModel");
            Assert.IsTrue(singleResults.Count == 1);
            var noResult = engy.Query($"BID:1 AND Name:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ID, actual.ID);
        }

        [TestMethod]
        public async Task TestCustomFieldLayoutDefinitionSearchModel() 
        {
            var repo = ((TestDapperRepository)GetDapperRepository());
            string tableName = ChildObjectTableInfo.CustomFieldLayoutDefinitionTableName;

            await DeleteSqlExec(repo,
                tableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            await DeleteSqlExec(repo,
                tableName,
                ChildObjectTableInfo.IDColumn,
                new
                {
                    BID,
                    ID = IDToUse + 1,
                });

            await DeleteSqlExec(repo,
                tableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            var expected = new
            {
                BID,
                ID = IDToUse,
                ModifiedDT = DateTime.UtcNow,
                IsActive = 1,
                IsSystem = 0,
                AppliesToClassTypeID = 2000,
                IsAllTab = 0,
                IsSubTab = 0,
                SortIndex = 1,
                Name = "UNITTESTCustomFieldLayoutDefinitionSearchModel",
            };

            await InsertSqlExec(repo, tableName, expected);

            var resultList = 
                await repo.GetSearchModelByIdAsync<CustomFieldLayoutDefinitionSearchModel>(new int[] { IDToUse });
            var engy = new Engine<CustomFieldLayoutDefinitionSearchModel>(myTestRunIndexName,
                MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query($"BID:1 AND UNITTESTCustomFieldLayoutDefinitionSearchModel");
            Assert.IsTrue(singleResults.Count == 1);

            var noResult = engy.Query($"BID:1 AND Name:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ID, actual.ID);
        }
    }
}
