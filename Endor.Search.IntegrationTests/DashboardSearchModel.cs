﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class DashboardSearchModel : BaseEntitySearchTestClass
    {
        [TestMethod]
        public async Task TestDashboardDataSearchModel() 
        {
            var repo = ((TestDapperRepository)GetDapperRepository());
            string tableName = ChildObjectTableInfo.DashboardDataTableName;

            await DeleteSqlExec(repo, tableName, IDColumn, defaultParam);

            await DeleteSqlExec(repo,  tableName, IDColumn,
                new
                {
                    BID,
                    ID = IDToUse + 1,
                });

            await DeleteSqlExec(repo, tableName, IDColumn, defaultParam);

            var expected = new
            {
                BID,
                ID = IDToUse,
                Cols = 4,
                Description = "Generated from Unit Test",
                IsActive = 1,
                LastUpdatedDT = DateTime.UtcNow,
                ModifiedDT = DateTime.UtcNow,
                Module = 1,
                Name = "UNITTESTDashboardDataSearchModel",
                Rows = 10
            };

            await InsertSqlExec(repo, tableName, expected);

            var resultList = await repo.GetSearchModelByIdAsync<DashboardDataSearchModel>(new int[] { IDToUse });
            var engy = new Engine<DashboardDataSearchModel>(myTestRunIndexName,
                MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());
            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            var singleResults = engy.Query($"BID:1 AND Name:UNITTESTDashboardDataSearchModel");
            Assert.IsTrue(singleResults.Count == 1);
            var noResult = engy.Query($"BID:1 AND Name:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ID, actual.ID);
        }

        [TestMethod]
        public async Task TestDashboardWidgetDefinitionSearchModel() 
        {
            var repo = ((TestDapperRepository)GetDapperRepository());
            string tableName = ChildObjectTableInfo.DashboardWidgetDefinitionTableName;
            object specializedParam = new { ID = IDToUse };

            await DeleteWithoutBIDSqlExec(repo, tableName, IDColumn, specializedParam);

            var expected = new
            {
                ID = IDToUse,
                DefaultCols = 1,
                DefaultName = "UNITTESTDashboardWidgetDefinitionSearchModel",
                DefaultRefreshIntervalInMin = 240,
                DefaultRows = 1,
                HasImage = 0,
                MaxCols = 4,
                MaxRows = 3,
                MinCols = 1,
                MinRows = 1,
                ModifiedDT = DateTime.UtcNow
            };

            await InsertSqlExec(repo, tableName, expected);

            var resultList = await repo.GetSearchModelByIdAsync<DashboardWidgetDefinitionSearchModel>(new int[] { IDToUse });
            var engy = new Engine<DashboardWidgetDefinitionSearchModel>(myTestRunIndexName,
                MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());
            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            var singleResults = engy.Query($"DefaultName:UNITTESTDashboardWidgetDefinitionSearchModel");
            Assert.IsTrue(singleResults.Count == 1);
            var noResult = engy.Query($"DefaultName:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ID, actual.ID);
        }
    }
}
