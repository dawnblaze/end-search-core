﻿using Endor.Search.infrastructure.Common;
using Endor.Search.IntegrationTests.Common;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;

namespace Endor.Search.IntegrationTests
{
    public class BaseEntitySearchTestClass : BaseTestFixture
    {
        protected const string TestRunPrefix = "testrun";
        protected string myTestRunIndexName = TestRunPrefix;
        private static Task completedTask = Task.FromResult(false);
        public static short BID = 1;
        public static short IDToUse = -100;
        public string IDColumn = ChildObjectTableInfo.IDColumn;

        public object defaultParam = new
        {
            BID = BID,
            ID = IDToUse,
        };

        [TestInitialize]
        public void Startup()
        {
            myTestRunIndexName += DateTime.UtcNow.Ticks.ToString();
            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();
            _cache = serviceProvider.GetService<IMemoryCache>();
        }

        /*
        public static TransactionHeaderData GetTestTransactionData(int number, OrderTransactionType transactionType)
        {
            if (transactionType == OrderTransactionType.Order)
            {
                return new OrderData()
                {
                    BID = 1,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    LocationID = 1,
                    TransactionType = (byte)transactionType,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = number,
                    FormattedNumber = "TEST-" + number,
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                    TaxGroupID = 0
                };

            }
            else if (transactionType == OrderTransactionType.Estimate)
            {
                return new EstimateData()
                {
                    BID = 1,
                    ClassTypeID = (int)ClassType.Estimate,
                    ModifiedDT = DateTime.UtcNow,
                    LocationID = 1,
                    TransactionType = (byte)transactionType,
                    OrderStatusID = OrderOrderStatus.EstimatePending,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = number,
                    FormattedNumber = "TEST-" + number,
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                    TaxGroupID = 0
                };
            }
            else if (transactionType == OrderTransactionType.Memo)
            {
                return new CreditMemoData()
                {
                    BID = 1,
                    ClassTypeID = (int)ClassType.CreditMemo,
                    ModifiedDT = DateTime.UtcNow,
                    LocationID = 1,
                    TransactionType = (byte)transactionType,
                    OrderStatusID = OrderOrderStatus.CreditMemoUnposted,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = number,
                    FormattedNumber = "TEST-" + number,
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                    TaxGroupID = 0
                };
            }
            return null;
        }*/

        protected static async Task<Task> DeleteSqlExec(TestDapperRepository repo, string tableName, string IDColumnName, object param)
        {
            string deleteQuery = $"Delete From {tableName} where BID = @BID and {IDColumnName}=@ID";
            var deleteResult = await repo.ExecuteSqlRawAsync(deleteQuery, param);
            return completedTask;
        }
        protected static async Task<Task> DeleteWithoutBIDSqlExec(TestDapperRepository repo, string tableName, string IDColumnName, object param)
        {
            string deleteQuery = $"Delete From {tableName} WHERE {IDColumnName}=@ID";
            var deleteResult = await repo.ExecuteSqlRawAsync(deleteQuery, param);
            return completedTask;
        }

        protected static async Task<Task> InsertSqlExec(TestDapperRepository repo, string tableName, object param)
        {

            PropertyInfo[] properties = param.GetType().GetProperties();
            string fields = "";
            string values = "";
            foreach (var property in properties)
            {
                fields += "[" + property.Name + "],";
                values += "@" + property.Name + ",";
            }
            fields = fields.TrimEnd(',');
            values = values.TrimEnd(',');
            string insertQuery = $"INSERT INTO {tableName}({fields}) VALUES ({values})";
            var insertResult = await repo.ExecuteSqlRawAsync(insertQuery, param);
            return completedTask;
        }

        protected static async Task<Task> CleanupGLAccount(TestDapperRepository repo, Object defaultParam)
        {
            #region AccountingGLAccountTableName FK references
            await DeleteSqlExec(repo,
                ChildObjectTableInfo.DestinationDataTableName,
                "ExpenseAccountID",
                defaultParam);
            await DeleteSqlExec(repo,
                ChildObjectTableInfo.DestinationDataTableName,
                "IncomeAccountID",
                defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.OrderItemComponentTableName,
                "ExpenseAccountID",
                defaultParam);
            await DeleteSqlExec(repo,
                ChildObjectTableInfo.OrderItemComponentTableName,
                "IncomeAccountID",
                defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.OrderItemSurchargeTableName,
                "IncomeAccountID",
                defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.PartLaborDataTableName,
                "ExpenseAccountID",
                defaultParam);
            await DeleteSqlExec(repo,
                ChildObjectTableInfo.PartLaborDataTableName,
                "IncomeAccountID",
                defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.PartMachineDataTableName,
                "ExpenseAccountID",
                defaultParam);
            await DeleteSqlExec(repo,
                ChildObjectTableInfo.PartMachineDataTableName,
                "IncomeAccountID",
                defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.PartMaterialDataTableName,
                "ExpenseAccountID",
                defaultParam);
            await DeleteSqlExec(repo,
                ChildObjectTableInfo.PartMaterialDataTableName,
                "IncomeAccountID",
                defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.AssemblyDataTableName,
                "IncomeAccountID",
                defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.SurchargeDefTableName,
                "IncomeAccountID",
                defaultParam);
            #endregion

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.AccountingGLAccountTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            return completedTask;
        }
    }
}
