﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class LaborDataSearchModelTests : BaseEntitySearchTestClass
    {
        [TestMethod]
        public async Task TestLaborDataSearchModel()
        {
            var repo = ((TestDapperRepository)GetDapperRepository());
            
            await CleanupGLAccount(repo, defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.PartLaborDataTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            var expected = new
            {
                BID = BID,
                ID = IDToUse,
                ExportGLAccountType = 10,
                GLAccountType = 10,
                ModifiedDT = DateTime.UtcNow,
                Name = "UNITTESTAccountingGLAccount",
                Number = 1000,
                IsActive = 1,
                CanEdit = 0
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.AccountingGLAccountTableName, expected);

            var expected2 = new
            {
                BID = BID,
                ID = IDToUse,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                ExpenseAccountID = IDToUse,
                HasImage = false,
                IncomeAccountID = IDToUse,
                InvoiceText = "",
                Name = "UNITTESTLaborDataSearchModel"
            };

            await InsertSqlExec(repo, ChildObjectTableInfo.PartLaborDataTableName, expected2);

            var resultList = await repo.GetSearchModelByIdAsync<LaborDataSearchModel>(new int[] { IDToUse });
            var engy = new Engine<LaborDataSearchModel>(myTestRunIndexName,
                MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());
            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            var singleResults = engy.Query($"BID:1 AND Name:UNITTESTLaborDataSearchModel");
            Assert.IsTrue(singleResults.Count == 1);
            var noResult = engy.Query($"BID:1 AND Name:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected2.ID, actual.ID);
        }
    }
}
