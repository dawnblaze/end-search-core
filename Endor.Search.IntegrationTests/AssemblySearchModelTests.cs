﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class AssemblySearchModelTests : BaseEntitySearchTestClass
    {
        [TestMethod]
        public async Task TestAssemblyDataSearchModel()
        {
            var repo = ((TestDapperRepository)GetDapperRepository());

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.AssemblyDataTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.AssemblyDataTableName,
                ChildObjectTableInfo.IDColumn,
                new
                {
                    BID = BID,
                    ID = IDToUse + 1,
                });

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.AssemblyDataTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            var expected = new
            {
                BID = BID,
                ID = IDToUse,
                HasImage = false,
                IsActive = false,
                ModifiedDT = DateTime.UtcNow,
                Name = "UNITTESTAssemblyDataSearchModel"
            };

            await InsertSqlExec(repo, ChildObjectTableInfo.AssemblyDataTableName, expected);

            var resultList = await repo.GetSearchModelByIdAsync<AssemblyDataSearchModel>(new int[] { IDToUse });
            var engy = new Engine<AssemblyDataSearchModel>(myTestRunIndexName,
                MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());
            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            var singleResults = engy.Query($"BID:1 AND Name:UNITTESTAssemblyDataSearchModel");
            Assert.IsTrue(singleResults.Count == 1);
            var noResult = engy.Query($"BID:1 AND Name:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);
            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(12040, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);
        }

        [TestMethod]
        public async Task TestAssemblyCategorySearchModel() 
        {
            var IDToUse = -100;
            var BID = 1;
            var repo = ((TestDapperRepository) GetDapperRepository());

            object defaultParam = new
            {
                BID = BID,
                ID = IDToUse,
            };

            await DeleteSqlExec(repo, 
                ChildObjectTableInfo.AssemblyCategoryTableName, 
                ChildObjectTableInfo.IDColumn, 
                defaultParam);

            await DeleteSqlExec(repo, 
                ChildObjectTableInfo.AssemblyCategoryTableName, 
                ChildObjectTableInfo.IDColumn, 
                new
                {
                    BID = BID,
                    ID = IDToUse + 1,
                });

            await DeleteSqlExec(repo, 
                ChildObjectTableInfo.AssemblyCategoryTableName, 
                ChildObjectTableInfo.IDColumn, 
                defaultParam);

            var expected = new
            {
                BID = BID,
                ID = IDToUse,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                Name = "UNITTESTAssemblyCategorySearchModel",
                Description = "Generated from Unit Testing"
            };

            await InsertSqlExec(repo, ChildObjectTableInfo.AssemblyCategoryTableName, expected);

            var resultList = await repo.GetSearchModelByIdAsync<AssemblyCategorySearchModel>(new int[] { IDToUse });
            var engy = new Engine<AssemblyCategorySearchModel>(myTestRunIndexName,
                MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());
            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            var singleResults = engy.Query($"BID:1 AND Name:UNITTESTAssemblyCategorySearchModel");
            Assert.IsTrue(singleResults.Count == 1);
            var noResult = engy.Query($"BID:1 AND Name:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);
            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(12042, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);
        }
    }
}
