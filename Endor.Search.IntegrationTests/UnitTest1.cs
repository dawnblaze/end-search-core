using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Lucene.Net.Search;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class BasicTests : BaseEntitySearchTestClass
    {
        private const int NumberOfBusinesses = 6;

        [TestMethod]
        public async Task TestRemainingSearchModels()
        {
            var IDToUse = -100;
            var BID = 1;
            var repo = ((TestDapperRepository)GetDapperRepository());

            object defaultParam = new
            {
                BID = BID,
                ID = IDToUse,
            };

            var assemblyCategoryResults = await repo.GetSearchModelByIdAsync<AssemblyCategorySearchModel>(new int[] { IDToUse });

            var assemblyDataResults = await repo.GetSearchModelByIdAsync<AssemblyCategorySearchModel>(new int[] { IDToUse });

            var boardDefinitionDataResults = await repo.GetSearchModelByIdAsync<BoardDefinitionDataSearchModel>(new int[] { IDToUse });

            var creditMemoDataResults = await repo.GetSearchModelByIdAsync<CreditMemoDataSearchModel>(new int[] { IDToUse });

            var crmIndustryResults = await repo.GetSearchModelByIdAsync<CrmIndustrySearchModel>(new int[] { IDToUse });

            var crmOriginResults = await repo.GetSearchModelByIdAsync<CrmOriginSearchModel>(new int[] { IDToUse });

            var customFieldDefinition = await repo.GetSearchModelByIdAsync<CustomFieldDefinitionSearchModel>(new int[] { IDToUse });

            var customFieldLayoutDefinition = await repo.GetSearchModelByIdAsync<CustomFieldLayoutDefinitionSearchModel>(new int[] { IDToUse });

            var dashboardDataResults = await repo.GetSearchModelByIdAsync<DashboardDataSearchModel>(new int[] { IDToUse });

            var dashboardWidgetDefinitionResults = await repo.GetSearchModelByIdAsync<DashboardWidgetDefinitionSearchModel>(new int[] { IDToUse });

            var destinationDataResults = await repo.GetSearchModelByIdAsync<DestinationDataSearchModel>(new int[] { IDToUse });

            var domainDataResults = await repo.GetSearchModelByIdAsync<DomainDataSearchModel>(new int[] { IDToUse });

            var domainEmailResults = await repo.GetSearchModelByIdAsync<DomainEmailSearchModel>(new int[] { IDToUse });

            var employeeTeamResults = await repo.GetSearchModelByIdAsync<EmployeeTeamSearchModel>(new int[] { IDToUse });

            var estimateDataResults = await repo.GetSearchModelByIdAsync<EstimateDataSearchModel>(new int[] { IDToUse });

            var laborCategoryResult = await repo.GetSearchModelByIdAsync<LaborCategorySearchModel>(new int[] { IDToUse });

            var laborDataResults = await repo.GetSearchModelByIdAsync<LaborDataSearchModel>(new int[] { IDToUse });

            var locationDataResults = await repo.GetSearchModelByIdAsync<LocationDataSearchModel>(new int[] { IDToUse });

            var machineCategoryResults = await repo.GetSearchModelByIdAsync<MachineCategorySearchModel>(new int[] { IDToUse });

            var machineDataResults = await repo.GetSearchModelByIdAsync<MachineDataSearchModel>(new int[] { IDToUse });

            var materialCategoryResults = await repo.GetSearchModelByIdAsync<MaterialCategorySearchModel>(new int[] { IDToUse });

            var materialDataResults = await repo.GetSearchModelByIdAsync<MaterialDataSearchModel>(new int[] { IDToUse });

            var messageBodyTemplate = await repo.GetSearchModelByIdAsync<MessageBodyTemplateSearchModel>(new int[] { IDToUse });

            var messageHeaderResults = await repo.GetSearchModelByIdAsync<MessageHeaderSearchModel>(new int[] { IDToUse });

            var orderDataResults = await repo.GetSearchModelByIdAsync<OrderDataSearchModel>(new int[] { IDToUse });

            var orderItemResults = await repo.GetSearchModelByIdAsync<OrderItemDataSearchModel>(new int[] { IDToUse });

            var paymentApplicationResults = await repo.GetSearchModelByIdAsync<PaymentApplicationDataSearchModel>(new int[] { IDToUse });

            var paymentMasterResults = await repo.GetSearchModelByIdAsync<PaymentMasterDataSearchModel>(new int[] { IDToUse });

            var quickItemResults = await repo.GetSearchModelByIdAsync<QuickItemSearchModel>(new int[] { IDToUse });

            var reconciliationResults = await repo.GetSearchModelByIdAsync<ReconciliationSearchModel>(new int[] { IDToUse });

            var rigthsGroupListResults = await repo.GetSearchModelByIdAsync<RightsGroupListSearchModel>(new int[] { IDToUse });

            var surchargeDefResults = await repo.GetSearchModelByIdAsync<SurchargeDefSearchModel>(new int[] { IDToUse });

            var taxabilityCodeResults = await repo.GetSearchModelByIdAsync<TaxabilityCodeSearchModel>(new int[] { IDToUse });

        }

        /*
        [TestMethod]
        public void TestManyWildcardSearchable()
        {
            WriteTestRecords(out TestWildcardAtomModel expected);

            var engRead = new Engine<TestWildcardAtomModel, int, TestWildcardSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            var allResults = engRead.Query("");
            Assert.IsTrue(allResults.Count == NumberOfBusinesses);

            var singleResults = engRead.Query("BID:1 AND test");
            Assert.IsTrue(singleResults.Count == 1);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);
        }
        */
        [TestMethod]
        public async Task TestEmployeeData()
        {
            var IDToUse = -100;
            var BID = 1;
            var repo = ((TestDapperRepository)GetDapperRepository());

            //Arrange
            var locationID = GetValidLocationID(repo);

            object defaultParam = new
            {
                BID = BID,
                ID = IDToUse,
            };

            await DeleteSqlExec(repo, ChildObjectTableInfo.UserLinkTableName, "EmployeeID",defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.EmployeeDataTableName, ChildObjectTableInfo.IDColumn, defaultParam);

            var expected = new
            {
                BID = BID,
                ID = IDToUse,
                First = "foo",
                Last = "barino",
                LocationID = locationID
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.EmployeeDataTableName, expected);

            var expected2 = new
            {
                BID = BID,
                AuthUserID = IDToUse,
                EmployeeID = IDToUse,
                DisplayName = "Test",
                UserAccessType = 49,
                UserName = "TestMe"
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.UserLinkTableName, expected2);

            var resultList = await repo.GetSearchModelByIdAsync<EmployeeDataSearchModel>(new int[] { IDToUse });

            var engy = new Engine<EmployeeDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            engy.Write(resultList.FirstOrDefault());

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query("BID:1");
            Assert.IsTrue(singleResults.Count == 1);

            singleResults = engy.Query("BID:1 AND foo");
            Assert.IsTrue(singleResults.Count == 1);
            
            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.IsTrue(actual.ClassTypeID>0);
            Assert.AreEqual(expected.ID, actual.ID);
            Assert.AreEqual(expected2.UserName, actual.UserName);

            var NoResults = engy.Query("BID:0 AND foo");
            Assert.IsTrue(NoResults.Count == 0);

            NoResults = engy.Query("BID:1 AND NotFound");
            Assert.IsTrue(NoResults.Count == 0);
        }
        /*
        [TestMethod]
        public void TestEmployeeDataMultipleWordQuery()
        {
            var engy = new Engine<EmployeeData, short, EmployeeDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            int employeeCTID = (int)ClassType.Employee;
            EmployeeData expectedBarino = new EmployeeData()
            {
                BID = 1,
                ClassTypeID = employeeCTID,
                ID = 3,
                LongName = "foo ayy grue barino",
                First = "foo",
                Last = "barino"
            };
            engy.Write(expectedBarino);
            EmployeeData expectedBarfuzz = new EmployeeData()
            {
                BID = 1,
                ClassTypeID = employeeCTID,
                ID = 3,
                LongName = "foo bee grue barfuzz",
                First = "foo",
                Last = "barfuzz"
            };
            engy.Write(expectedBarfuzz);

            var allResults = engy.Query("");
            Assert.AreEqual(2, allResults.Count);

            var singleResults = engy.Query(GetEffectiveSearch("foo barino", employeeCTID));
            Assert.AreEqual(1, singleResults.Count);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expectedBarino.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expectedBarino.ID, actual.ID);

            singleResults = engy.Query(GetEffectiveSearch("barfuzz", employeeCTID));
            Assert.AreEqual(1, singleResults.Count);

            actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expectedBarfuzz.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expectedBarfuzz.ID, actual.ID);

            singleResults = engy.Query(GetEffectiveSearch("grue bee", employeeCTID));
            Assert.AreEqual(1, singleResults.Count);

            actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expectedBarfuzz.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expectedBarfuzz.ID, actual.ID);


            singleResults = engy.Query(GetEffectiveSearch("foo bee", employeeCTID));
            Assert.AreEqual(1, singleResults.Count);

            actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expectedBarfuzz.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expectedBarfuzz.ID, actual.ID);
        }


        [TestMethod]
        public void TestBoardDefinitionData()
        {
            var engy = new Engine<BoardDefinitionData, short, BoardDefinitionDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            BoardDefinitionData expected = new BoardDefinitionData()
            {
                BID = 1,
                ClassTypeID = 1500,
                ID = 3,
                Name = "foo barino",
            };
            engy.Write(expected);

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query("BID:1 AND foo");
            Assert.IsTrue(singleResults.Count == 1);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);

            singleResults = engy.Query("BID:1 AND fox");
            Assert.IsTrue(singleResults.Count == 0);
        }

        [TestMethod]
        public void TestOneWildcardSearchable()
        {
            TestWildcardAtomModel expected = WriteTestRecord(1);

            var engRead = new Engine<TestWildcardAtomModel, int, TestWildcardSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            AssertHasAtLeastOneDocument(engRead);

            const string basicQuery = "BID:1 AND test";
            AssertDynamicQueryHasAtLeastOneDocument(engRead, basicQuery);

            var results = engRead.Query(basicQuery);
            Assert.IsNotNull(results);
            Assert.IsTrue(results.Count == 1);

            var actual = results.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);
        }

        [TestMethod]
        public void TestDestinationDataSearchModel()
        {
            var engy = new Engine<DestinationData, short, DestinationDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            DestinationData expected = new DestinationData()
            {
                BID = 1,
                ClassTypeID = ClassType.Destination.ID(),
                ID = 3,
                Name = "foo barino",
                Description = "foo",
                SKU = "barino",
                ActiveProfileCount = 4,
                Profiles = new List<DestinationProfile>()
                {
                    new DestinationProfile()
                    {
                        Name = "Profile1"
                    },
                    new DestinationProfile()
                    {
                        Name = "Profile2"
                    }
                }
            };
            engy.Write(expected);

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query("BID:1 AND foo");
            Assert.IsTrue(singleResults.Count == 1);

            var singleResultsForActiveProfile = engy.Query("Profiles:Profile1,Profile2");
            Assert.IsTrue(singleResultsForActiveProfile.Count == 1);

            var activeProfileCount = engy.Query("ActiveProfileCount:4");
            Assert.IsTrue(activeProfileCount.Count == 1);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);
        }

        [TestMethod]
        public void TestMachineDataSearchModel()
        {
            var engy = new Engine<MachineData, short, MachineDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            MachineData expected = new MachineData()
            {
                BID = 1,
                ClassTypeID = ClassType.Machine.ID(),
                ID = 3,
                Name = "foo barino",
                Description = "foo",
                SKU = "barino",
                ActiveInstanceCount = 2,
                ActiveProfileCount = 4,
                Profiles = new List<MachineProfile>()
                {
                    new MachineProfile()
                    {
                        Name = "Profile1"
                    },
                    new MachineProfile()
                    {
                        Name = "Profile2"
                    }
                },
                Instances = new List<MachineInstance>()
                {
                    new MachineInstance()
                    {
                        Name = "Instance1"
                    }
                }



            };
            engy.Write(expected);

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query("BID:1 AND foo");
            Assert.IsTrue(singleResults.Count == 1);

            var singleResultsForActiveProfile = engy.Query("Profiles:Profile1,Profile2");
            Assert.IsTrue(singleResultsForActiveProfile.Count == 1);

            var singleResultsForActiveInstance = engy.Query("Instances:Instance1");
            Assert.IsTrue(singleResultsForActiveInstance.Count == 1);

            var activeInstanceCount = engy.Query("ActiveInstanceCount:2");
            Assert.IsTrue(activeInstanceCount.Count == 1);

            var activeProfileCount = engy.Query("ActiveProfileCount:4");
            Assert.IsTrue(activeProfileCount.Count == 1);


            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);
        }

        [TestMethod]
        public void TestMaterialDataSearchModel()
        {
            var engy = new Engine<MaterialData, int, MaterialDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            MaterialData expected = new MaterialData()
            {
                BID = 1,
                ClassTypeID = ClassType.Machine.ID(),
                ID = 3,
                Name = "foo barino",
                Description = "foo",
                SKU = "barino"
            };
            engy.Write(expected);

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query("BID:1 AND foo");
            Assert.IsTrue(singleResults.Count == 1);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);
        }

        [TestMethod]
        public async Task IndexAllMaterialDataSearch()
        {
            await Task.Yield();
            Assert.Inconclusive();
            //var engy = new Engine<MaterialData, int, MaterialDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            //var ctx = MockTenantDataCache.GetContext(1);

            //if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
            //{
            //    await ctx.Database.MigrateAsync();
            //}

            //var queryX = ctx.BusinessData.Where(x => x.BID == 1);
            //var query = ctx.MaterialData.Where(x => x.BID == 1);
            //var all = await query.ToArrayAsync();

            //engy.DeleteExistingAndWriteMany(Convert.ToInt32(ClassType.Material), all);
        }

        [TestMethod]
        public async Task IndexAllOrderDataSearch()
        {
            await Task.Yield();
            Assert.Inconclusive();
            //var engy = new Engine<OrderData, int, OrderDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            //var ctx = MockTenantDataCache.GetContext(1);

            //if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
            //{
            //    await ctx.Database.MigrateAsync();
            //}

            //var query = ctx.OrderData.Where(x => x.BID == 1);
            //var includes = new OrderDataSearchModel().IncludesForMap;
            //foreach (string include in includes)
            //    query = query.Include(include);
            //var all = await query.ToArrayAsync();

            //engy.DeleteExistingAndWriteMany(Convert.ToInt32(ClassType.Order), all);

            //string search = "ClassTypeID:10000";
            //var result = engy.Query(search, 10);
            //Assert.IsNotNull(result);
            //Assert.AreNotEqual(0, result.Count);
        }

        [TestMethod]
        public async Task IndexOrderDataAmounts()
        {
            await Task.Yield();
            Assert.Inconclusive();
            //var engy = new Engine<OrderData, int, OrderDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            //var ctx = MockTenantDataCache.GetContext(1);

            //if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
            //{
            //    await ctx.Database.MigrateAsync();
            //}

            //var query = ctx.OrderData.Where(x => x.BID == 1 && x.PriceTotal != null && x.PriceTotal >= 0);

            //if (query != null && query.Count() > 0){
            //    var includes = new OrderDataSearchModel().IncludesForMap;
            //    foreach (string include in includes)
            //        query = query.Include(include);
            //    var all = await query.ToArrayAsync();

            //    engy.DeleteExistingAndWriteMany(Convert.ToInt32(ClassType.Order), all);

            //    string search = "ClassTypeID:10000";
            //    var result = engy.Query(search, 10);
            //    Assert.IsNotNull(result);
            //    Assert.AreNotEqual(0, result.Count);
            //    Assert.IsTrue(result.All(x => x.PriceTotal >= 0));
            //}
        }

        [TestMethod]
        public async Task IndexAllEstimateDataSearch()
        {
            await Task.Yield();
            Assert.Inconclusive();
            //var engy = new Engine<EstimateData, int, EstimateDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            //var ctx = MockTenantDataCache.GetContext(1);

            //if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
            //{
            //    await ctx.Database.MigrateAsync();
            //}

            ////var dates = ctx.OrderKeyDate.Where(x => x.BID == 1).ToArray();
            ////var roles = ctx.OrderEmployeeRole.Where(x => x.BID == 1).ToArray();
            //var query = ctx.EstimateData.Where(x => x.BID == 1);
            //var includes = new EstimateDataSearchModel().IncludesForMap;
            //foreach (string include in includes)
            //    query = query.Include(include);
            //var all = await query.ToArrayAsync();

            //engy.DeleteExistingAndWriteMany(Convert.ToInt32(ClassType.Estimate), all);
        }

        [TestMethod]
        public void TestDomainEmailSearchModel()
        {
            var engy = new Engine<DomainEmail, short, DomainEmailSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            DomainEmail expected = new DomainEmail()
            {
                BID = 1,
                ClassTypeID = ClassType.DomainEmail.ID(),
                ID = 3,
                DomainName = "foo.bar",
                LocationLinks = new List<DomainEmailLocationLink>()
                {
                    new DomainEmailLocationLink()
                    {
                        Location = new LocationData()
                        {
                            Name = "loc1"
                        }
                    },
                    new DomainEmailLocationLink()
                    {
                        Location = new LocationData()
                        {
                            Name = "loc2"
                        }
                    }
                }
            };
            engy.Write(expected);

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query("BID:1 AND foo.bar");
            Assert.IsTrue(singleResults.Count == 1);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);

            singleResults = engy.Query("BID:1 AND loc2");
            Assert.IsTrue(singleResults.Count == 1);
        }

        

        public TestContext TestContext { get; set; }

        [TestMethod]
        public async Task TestEmployeeUserLinkMapping()
        {
            await Task.Yield();
            Assert.Inconclusive();
            ////setup: make sure migrated
            //var ctx = MockTenantDataCache.GetContext(1);
            //if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
            //{
            //    await ctx.Database.MigrateAsync();
            //}

            //var someEmployee = await ctx.EmployeeData.FirstOrDefaultAsync();
            //Assert.IsNotNull(someEmployee);
            //var employees = (await ctx.EmployeeData.Include("UserLinks").ToListAsync());
            //var linkedEmployee = employees.Where(x => x.UserLinks != null && x.UserLinks.Count > 0).FirstOrDefault();
            //var IDString = (new EmployeeDataSearchModel().Map(linkedEmployee)).UserIDs;
            //Assert.IsFalse(String.IsNullOrWhiteSpace(IDString));
            //Assert.IsTrue(IDString.Contains(linkedEmployee.UserLinks.FirstOrDefault().AuthUserID.ToString()));
        }

        [TestMethod]
        public void TestOrderData()
        {
            var engy = new Engine<OrderData, int, OrderDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            OrderData order1 = (OrderData)GetTestTransactionData(1000, OrderTransactionType.Order);
            order1.OrderStatusID = OrderOrderStatus.OrderVoided;
            OrderData order2 = (OrderData)GetTestTransactionData(1001, OrderTransactionType.Order);
            OrderData order3 = (OrderData)GetTestTransactionData(1002, OrderTransactionType.Order);

            engy.WriteMany(new List<OrderData>() { order1, order2, order3 });

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 3);

            var multipleResults = engy.Query("Test- AND IsVoided:False");
            Assert.AreEqual(2, multipleResults.Count);

            var singleResult = engy.Query("Test- AND IsVoided:True");
            Assert.AreEqual(1, singleResult.Count);

            string serializedResult = JsonConvert.SerializeObject(singleResult.First());
            JToken resultToken = JToken.Parse(serializedResult);

            Assert.IsNotNull(resultToken["ID"]);
            Assert.IsNotNull(resultToken["ProductionDueDate"]);
            Assert.IsNotNull(resultToken["FollowUpDate"]);
            Assert.IsNotNull(resultToken["IsApproved"]);
            Assert.IsNotNull(resultToken["IsLost"]);
        }

        [TestMethod]
        public void TestCreditMemoData()
        {
            var engy = new Engine<CreditMemoData, int, CreditMemoDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            CreditMemoData order1 = (CreditMemoData)GetTestTransactionData(1000, OrderTransactionType.Memo);
            order1.OrderStatusID = OrderOrderStatus.OrderVoided;
            CreditMemoData order2 = (CreditMemoData)GetTestTransactionData(1001, OrderTransactionType.Memo);
            CreditMemoData order3 = (CreditMemoData)GetTestTransactionData(1002, OrderTransactionType.Memo);

            engy.WriteMany(new List<CreditMemoData>() { order1, order2, order3 });

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 3);

            var multipleResults = engy.Query("Test- AND IsVoided:False");
            Assert.AreEqual(2, multipleResults.Count);

            var singleResult = engy.Query("Test- AND IsVoided:True");
            Assert.AreEqual(1, singleResult.Count);

            string serializedResult = JsonConvert.SerializeObject(singleResult.First());
            JToken resultToken = JToken.Parse(serializedResult);

            Assert.IsNotNull(resultToken["ID"]);
            Assert.IsNull(resultToken["ProductionDueDate"]);
            Assert.IsNull(resultToken["FollowUpDate"]);
            Assert.IsNull(resultToken["IsApproved"]);
            Assert.IsNull(resultToken["IsLost"]);
        }

        [TestMethod]
        public void TestEstimateData()
        {
            var engy = new Engine<EstimateData, int, EstimateDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            EstimateData order1 = (EstimateData)GetTestTransactionData(1000, OrderTransactionType.Estimate);
            order1.OrderStatusID = OrderOrderStatus.OrderVoided;
            EstimateData order2 = (EstimateData)GetTestTransactionData(1001, OrderTransactionType.Estimate);
            EstimateData order3 = (EstimateData)GetTestTransactionData(1002, OrderTransactionType.Estimate);

            engy.WriteMany(new List<EstimateData>() { order1, order2, order3 });

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 3);

            var multipleResults = engy.Query("Test- AND IsVoided:False");
            Assert.AreEqual(2, multipleResults.Count);

            var singleResult = engy.Query("Test- AND IsVoided:True");
            Assert.AreEqual(1, singleResult.Count);

            string serializedResult = JsonConvert.SerializeObject(singleResult.First());
            JToken resultToken = JToken.Parse(serializedResult);

            Assert.IsNotNull(resultToken["ID"]);
            Assert.IsNotNull(resultToken["ProductionDueDate"]);
            Assert.IsNotNull(resultToken["FollowUpDate"]);
            Assert.IsNotNull(resultToken["IsApproved"]);
            Assert.IsNotNull(resultToken["IsLost"]);
        }

        [TestMethod]
        public void TestOrderItemData()
        {
            var engy = new Engine<OrderItemData, int, OrderItemDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            OrderItemData expected = new OrderItemData()
            {
                BID = 1,
                ID = 3,
                Name = "foo barino",
            };
            engy.Write(expected);

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query("BID:1 AND foo");
            Assert.IsTrue(singleResults.Count == 1);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ID, actual.ID);
        }
        */
        [TestMethod]
        public void TestConstantComparatorSort()
        {
            var engine = new Engine<QuickItemSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            //var engy = new Engine<QuickItemData, int, QuickItemDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            QuickItemSearchModel expected1 = new QuickItemSearchModel()
            {
                BID = 1,
                ID = 1,
                CompanyID = 1,
                Name = "i work at location one",
                //Company = new CompanyData()
            };
            engine.Write(expected1);
            QuickItemSearchModel expected1b = new QuickItemSearchModel()
            {
                BID = 1,
                ID = 2,
                CompanyID = 1,
                Name = "i work at location one b",
                //Company = new CompanyData()
            };
            engine.Write(expected1b);
            QuickItemSearchModel expected2 = new QuickItemSearchModel()
            {
                BID = 1,
                ID = 10,
                CompanyID = 2,
                Name = "i work at location two",
                //Company = new CompanyData()
            };
            engine.Write(expected2);
            QuickItemSearchModel expected2b = new QuickItemSearchModel()
            {
                BID = 1,
                ID = 11,
                CompanyID = 2,
                Name = "i work at location two b",
                //Company = new CompanyData()
            };
            engine.Write(expected2b);
            QuickItemSearchModel comboBreaker = new QuickItemSearchModel()
            {
                BID = 1,
                ID = -1,
                CompanyID = null,
                Name = "COMBOBREAKER",
            };
            engine.Write(comboBreaker);

            var allResults = engine.Query("", 0, 10, sortBy: null);
            Assert.IsNotNull(allResults.Results.FirstOrDefault(x => x.ID == expected1.ID), "test objects were not written to index");
            Assert.IsNotNull(allResults.Results.FirstOrDefault(x => x.ID == expected1b.ID), "test objects were not written to index");
            Assert.IsNotNull(allResults.Results.FirstOrDefault(x => x.ID == expected2.ID), "test objects were not written to index");
            Assert.IsNotNull(allResults.Results.FirstOrDefault(x => x.ID == expected2b.ID), "test objects were not written to index");
            Assert.IsNotNull(allResults.Results.FirstOrDefault(x => x.ID == comboBreaker.ID), "test objects were not written to index");

            var sortedResults = engine.Query("", 0, 10, sortBy: $"-CompanyID:{(byte)SortFieldType.CUSTOM}_1,ID:{(byte)SortFieldType.INT32}");
            Assert.IsTrue(sortedResults.Results[0].ID == expected1.ID, "test objects were not sorted correctly");
            Assert.IsTrue(sortedResults.Results[1].ID == expected1b.ID, "test objects were not sorted correctly");
            Assert.IsTrue(sortedResults.Results[2].ID == comboBreaker.ID, "test objects were not sorted correctly");
            Assert.IsTrue(sortedResults.Results[3].ID == expected2.ID, "test objects were not sorted correctly");
            Assert.IsTrue(sortedResults.Results[4].ID == expected2b.ID, "test objects were not sorted correctly");

            sortedResults = engine.Query("", 0, 10, sortBy: $"-CompanyID:{(byte)SortFieldType.CUSTOM}_2,ID:{(byte)SortFieldType.INT32}");
            Assert.IsTrue(sortedResults.Results[0].ID == expected2.ID, "test objects were not sorted correctly");
            Assert.IsTrue(sortedResults.Results[1].ID == expected2b.ID, "test objects were not sorted correctly");
            Assert.IsTrue(sortedResults.Results[2].ID == comboBreaker.ID, "test objects were not sorted correctly");
            Assert.IsTrue(sortedResults.Results[3].ID == expected1.ID, "test objects were not sorted correctly");
            Assert.IsTrue(sortedResults.Results[4].ID == expected1b.ID, "test objects were not sorted correctly");
        }

        /*
        [TestMethod]
        public async Task StressTestQuery()
        {
            await Task.Yield();
            Assert.Inconclusive();
            ////setup: make sure migrated
            //var ctx = MockTenantDataCache.GetContext(1);
            //if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
            //{
            //    await ctx.Database.MigrateAsync();
            //}

            ////setup: make sure ctx doesn't blow up
            //var queryX = ctx.BusinessData.Where(x => x.BID == 1);
            //Assert.IsNotNull(queryX.ToList());

            ////setup: make sure material data is up to date
            //var query = ctx.MaterialData.Where(x => x.BID == 1);
            //var all = await query.ToArrayAsync();
            //Assert.IsTrue(all.Length > 0, "Cannot stress test, there are no Material records to index and query. Please insert some Materials");
            //var engy = new Engine<MaterialData, int, MaterialDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            //engy.DeleteExistingAndWriteMany(Convert.ToInt32(ClassType.Material), all);

            ////actual test: create engines and query all, repeatedly, logging time to create engines and query
            //const int NumberOfRuns = 17;
            //long totalQueryTime = 0;
            //int totalResultsQueried = 0;
            //Stopwatch watch = new Stopwatch();
            //for (int i = 0; i < NumberOfRuns; i++)
            //{
            //    watch.Reset();
            //    watch.Start();
            //    var materialEngine = new Engine<MaterialData, int, MaterialDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            //    watch.Stop();
            //    TestContext.WriteLine("Took {0}ms to create an engine", watch.ElapsedMilliseconds);
            //    watch.Reset();
            //    watch.Start();
            //    var results = materialEngine.Query($"ClassTypeID:{ Convert.ToInt32(ClassType.Material.ID()) } AND IsActive:true");
            //    watch.Stop();
            //    TestContext.WriteLine("Took {0}ms to query engine", watch.ElapsedMilliseconds);
            //    totalQueryTime += watch.ElapsedMilliseconds;
            //    TestContext.WriteLine("Queried {0} results", results.Count);
            //    totalResultsQueried += results.Count;
            //}
            //TestContext.WriteLine("Avg records per query {0}ms", totalResultsQueried / NumberOfRuns);
            //TestContext.WriteLine("Avg query time per record {0}ms", totalQueryTime / (long)totalResultsQueried);
            //TestContext.WriteLine("Avg query time per test run {0}ms", totalQueryTime / (long)NumberOfRuns);
        }

        private void WriteTestRecords(out TestWildcardAtomModel firstRecord)
        {
            firstRecord = null;
            for (int i = 0; i < NumberOfBusinesses; i++)
            {
                var written = WriteTestRecord(Convert.ToInt16(i + 1));

                if (i == 0)
                    firstRecord = written;
            }
        }

        private TestWildcardAtomModel WriteTestRecord(short BID, string index = null)
        {
            var entity = new TestWildcardAtomModel()
            {
                BID = BID,
                ID = 10,
                ClassTypeID = 1000,
                TestSummary = "test write read",
            };

            var engWrite = new Engine<TestWildcardAtomModel, int, TestWildcardSearchModel>(index ?? this.myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engWrite.Write(entity);

            return entity;
        }

        private static void AssertDynamicQueryHasAtLeastOneDocument<T, I, S>(Engine<T, I, S> engRead, string basicQuery)
            where T : IAtom<I>
            where I : struct, IConvertible
            where S : IAutomappedAutogeneratedChainableSearchModel<S, T, I>, new()
        {
            var dynamics = engRead.Query(basicQuery);
            Assert.IsTrue(dynamics.Count > 0);
        }

        private static void AssertHasAtLeastOneDocument<T, I, S>(Engine<T, I, S> engRead)
            where T : IAtom<I>
            where I : struct, IConvertible
            where S : IAutomappedAutogeneratedChainableSearchModel<S, T, I>, new()
        {
            var all = engRead.Query("");
            Assert.IsTrue(all.Count > 0);
        }

        [TestMethod]
        public void TestAssemblyTypeDataSearch()
        {
            AssemblyData expected1 = new AssemblyData()
            {
                BID = 1,
                ClassTypeID = 1500,
                ID = 3,
                AssemblyType = AssemblyType.Product,
                Name = "Assembly.Test",
                IsActive = true
            };
            AssemblyData expected2 = new AssemblyData()
            {
                BID = 1,
                ClassTypeID = 1500,
                ID = 3,
                AssemblyType = AssemblyType.MachineTemplate,
                Name = "Machine.Template.Test",
                IsActive = true
            };
            AssemblyData expected3 = new AssemblyData()
            {
                BID = 1,
                ClassTypeID = 1500,
                ID = 3,
                AssemblyType = AssemblyType.Destination,
                Name = "Destination.Template.Test",
                IsActive = true
            };

            var engy = new Engine<AssemblyData, int, AssemblyDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.WriteMany(new List<AssemblyData>() { expected1, expected2, expected3 });

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 3);

            var singleResults = engy.Query($"AssemblyType:{(int)AssemblyType.Product}");
            Assert.IsTrue(singleResults.Count == 1);
            Assert.AreEqual(expected1.Name, singleResults.FirstOrDefault().Name);

            singleResults = engy.Query($"AssemblyType:{(int)AssemblyType.MachineTemplate}");
            Assert.IsTrue(singleResults.Count == 1);
            Assert.AreEqual(expected2.Name, singleResults.FirstOrDefault().Name);

            singleResults = engy.Query($"AssemblyType:{(int)AssemblyType.Destination}");
            Assert.IsTrue(singleResults.Count == 1);
            Assert.AreEqual(expected3.Name, singleResults.FirstOrDefault().Name);
        }

        [TestMethod]
        public void TestPaymentMasterDataWithSingleWordDisplayNumber()
        {
            this._testPaymentMasterData("Cash");
        }

        [TestMethod]
        public void TestPaymentMasterDataWithWordWithDashDisplayNumber()
        {
            this._testPaymentMasterData("Check-1234");
        }

        [TestMethod]
        public void TestPaymentMasterDataWithWordWithDoubleDashDisplayNumber()
        {
            this._testPaymentMasterData("Check-1234-5678");
        }

        [TestMethod]
        public void TestPaymentMasterDataWithSpaceDashSpaceDisplayNumber()
        {
            this._testPaymentMasterData("Check - 1234");
        }

        public void _testPaymentMasterData(string expectedDisplayNumber)
        {
            var engy = new Engine<PaymentMaster, int, PaymentMasterDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            DateTime expectedDateTime = new DateTime();
            string expectedDateTimeStr = expectedDateTime.ToString("MM/dd/yyyy - hh:mm tt");
            int expectedPaymentMethodID = Convert.ToInt32(PaymentMethodType.Check);

            OrderData orderData = new OrderData()
            {
                BID = 1,
                ClassTypeID = 10000,
                ID = 1668,
                FormattedNumber = "BR-O-1668"
            };

            PaymentApplication paymentApplication = new PaymentApplication()
            {
                BID = 1,
                ClassTypeID = 8011,
                ID = 3,
                Notes = "test note",
                Amount = 100,
                ValidToDT = expectedDateTime,
                AccountingDT = expectedDateTime,
                DisplayNumber = expectedDisplayNumber,
                LocationID = 1,
                Location = new LocationData(),
                CompanyID = 1,
                Company = new CompanyData(),
                ReceivedLocationID = 1,
                CurrencyType = (byte)AccountingCurrencyType.USDollar,
                PaymentTransactionType = (byte)PaymentTransactionType.Credit_Adjustment,
                PaymentMethodID = expectedPaymentMethodID,
                OrderID = 1668,
                Order = orderData,
                EnteredByEmployeeID = 100,
                EnteredByEmployee = new EmployeeData(),
                EnteredByContactID = 100,
                EnteredByContact = new ContactData()
            };

            var paymentApplications = new List<PaymentApplication>();
            paymentApplications.Add(paymentApplication);
            //paymentApplications.Add(paymentApplication2);

            PaymentMaster expected = new PaymentMaster()
            {
                BID = 1,
                ClassTypeID = 8010,
                ID = 3,
                Notes = "test note",
                Amount = 100,
                ValidToDT = expectedDateTime,
                AccountingDT = expectedDateTime,
                DisplayNumber = expectedDisplayNumber,
                LocationID = 1,
                Location = new LocationData(),
                CompanyID = 1,
                Company = new CompanyData(),
                ReceivedLocationID = 1,
                CurrencyType = (byte)AccountingCurrencyType.USDollar,
                PaymentTransactionType = (byte)PaymentTransactionType.Credit_Adjustment,
                PaymentApplications = paymentApplications
            };
            engy.Write(expected);
            int ctid = Convert.ToInt32(ClassType.PaymentMaster);

            var allResults = engy.Query(GetEffectiveSearch("", ctid));
            Assert.IsTrue(allResults.Count == 1);

            var bogusQuery = engy.Query(GetEffectiveSearch("Text:nosuchdataindexed", ctid));
            Assert.IsTrue(bogusQuery.Count == 0);

            AssertPayment(engy, $"Text:{expectedDateTimeStr.Replace('-', ' ').Replace(":", " ")}", expected);

            AssertPayment(engy, $"Text:{orderData.FormattedNumber.Replace('-', ' ').Replace(":", " ")}", expected);

            AssertPayment(engy, $"Text:{expectedDisplayNumber.Replace('-', ' ').Replace(":", " ")}", expected);
        }

        private void AssertPayment(Engine<PaymentMaster, int, PaymentMasterDataSearchModel> engy, string search, PaymentMaster expected)
        {
            int ctid = Convert.ToInt32(ClassType.PaymentMaster);
            string query = GetEffectiveSearch(search, ctid);
            PaymentMasterDataSearchModel actual = engy.Query($"{query}").FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);
        }

        [TestMethod]
        public void TestPaymentApplicationData()
        {
            var engy = new Engine<PaymentApplication, int, PaymentApplicationDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            PaymentApplication expected = new PaymentApplication()
            {
                BID = 1,
                ClassTypeID = 8011,
                ID = 3,
                Notes = "test note",
                Amount = 100,
                ValidToDT = new DateTime(),
                AccountingDT = new DateTime(),
                DisplayNumber = "AMEX - 1234",
                LocationID = 1,
                Location = new LocationData(),
                CompanyID = 1,
                Company = new CompanyData(),
                ReceivedLocationID = 1,
                CurrencyType = (byte)AccountingCurrencyType.USDollar,
                PaymentTransactionType = (byte)PaymentTransactionType.Credit_Adjustment,
                EnteredByEmployeeID = 100,
                EnteredByEmployee = new EmployeeData(),
                EnteredByContactID = 100,
                EnteredByContact = new ContactData()

            };
            engy.Write(expected);

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query("BID:1 AND AMEX");
            Assert.IsTrue(singleResults.Count == 1);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);

            singleResults = engy.Query("BID:1 AND yes");
            Assert.IsTrue(singleResults.Count == 0);
        }


        [TestMethod]
        public void MaterialDataReturnsMaterialCategoriesProperty()
        {
            //var engy = new Engine<MaterialData, int, MaterialDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            //MaterialData expected = new MaterialData()
            //{
            //    BID = 1,
            //    ID = -88,
            //    ClassTypeID = 12000,
            //    ConsumptionUnit = Unit.Each,
            //    Description = "asdf",
            //    EstimatingConsumptionMethod = 0,
            //    EstimatingCost = 1.0m,
            //    EstimatingCostingMethod = 0,
            //    EstimatingPrice = 1.0m,
            //    ExpenseAccountID = 5120,
            //    HasImage = true,
            //    IncomeAccountID = 4000,
            //    InventoryAccountID = 1500,
            //    InvoiceText = "Diamonds",
            //    IsActive = true,
            //    ModifiedDT = DateTime.Parse("2019-04-24T06:16:19.46"),
            //    Name = "Diamonds",
            //    PhysicalMaterialType = MaterialPhysicalType.Discrete,
            //    QuantityInSet = 1,
            //    SKU = "4456",
            //    TrackInventory = false,
            //    TaxCodeID = 10,
            //    MaterialCategoryLinks = new HashSet<MaterialCategoryLink>(){
            //        new MaterialCategoryLink()
            //        {
            //            BID =1,
            //            PartID = -88,
            //            MaterialCategory = new MaterialCategory()
            //            {
            //                BID =1,
            //                ID = -89,
            //                Name = "MaterialCatX",
            //            }
            //        }
            //    }
            //};

            //MaterialData expected2 = new MaterialData()
            //{
            //    BID = 1,
            //    ID = -89,
            //    ClassTypeID = 12000,
            //    ConsumptionUnit = Unit.Each,
            //    Description = "asdf2",
            //    EstimatingConsumptionMethod = 0,
            //    EstimatingCost = 1.0m,
            //    EstimatingCostingMethod = 0,
            //    EstimatingPrice = 1.0m,
            //    ExpenseAccountID = 5120,
            //    HasImage = true,
            //    IncomeAccountID = 4000,
            //    InventoryAccountID = 1500,
            //    InvoiceText = "Diamonds",
            //    IsActive = true,
            //    ModifiedDT = DateTime.Parse("2019-04-24T06:16:19.46"),
            //    Name = "Diamonds",
            //    PhysicalMaterialType = MaterialPhysicalType.Discrete,
            //    QuantityInSet = 1,
            //    SKU = "4456",
            //    TrackInventory = false,
            //    TaxCodeID = 10,
            //    MaterialCategoryLinks = new HashSet<MaterialCategoryLink>(){
            //        new MaterialCategoryLink()
            //        {
            //            BID =1,
            //            PartID = -89,
            //            MaterialCategory = new MaterialCategory()
            //            {
            //                BID =1,
            //                ID = -90,
            //                Name = "MaterialCatX",
            //            }
            //        }
            //    }
            //};

            //engy.Write(expected);
            //engy.Write(expected2);
            //var allResults = engy.Query("");
            //Assert.AreEqual(2, allResults.Count);

            //var multipleResults = engy.Query(GetEffectiveSearch("Text:Diamonds",ClassType.Material.ID()));
            //Assert.AreEqual(2, multipleResults.Count);

            //var singleResult = multipleResults.FirstOrDefault();
            //Assert.IsNotNull(singleResult);
            //Assert.AreEqual(expected.BID, singleResult.BID);
            //Assert.AreEqual(expected.ClassTypeID, singleResult.ClassTypeID);
            //Assert.AreEqual(expected.ID, singleResult.ID);

            //singleResult = engy.Query(GetEffectiveSearch("Categories:MaterialCatX", ClassType.Material.ID())).FirstOrDefault();
            //Assert.IsNotNull(singleResult.Categories);
            //Assert.AreEqual(expected.MaterialCategoryLinks.First().MaterialCategory.Name, singleResult.Categories);

            //var multipleSorted = engy.Query("", 0, 15, sortBy: "MaterialCategoriesSort:3");
            //Assert.IsNotNull(multipleSorted);
            //Assert.AreEqual(2, multipleSorted.Results.Count());

            //Assert.AreEqual(expected.MaterialCategoryLinks.First().MaterialCategory.Name, multipleSorted.Results[1].Categories);
            //Assert.AreEqual(expected2.MaterialCategoryLinks.First().MaterialCategory.Name, multipleSorted.Results[0].Categories);
        }

        [TestMethod]
        public void MachineWithCategoryAndSortProperty()
        {
            var engy = new Engine<MachineData, short, MachineDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            MachineData expected1 = new MachineData()
            {
                BID = 1,
                ClassTypeID = ClassType.Machine.ID(),
                ID = -88,
                Name = "Machine data test1",
                Description = "the quick brown fox",
                SKU = "foo",
                ActiveInstanceCount = 2,
                ActiveProfileCount = 4,
                MachineCategoryLinks = new HashSet<MachineCategoryLink>(){
                    new MachineCategoryLink()
                    {
                        BID = 1,
                        PartID = -88,
                        MachineCategory = new MachineCategory()
                        {
                            BID = 1,
                            ID = -77,
                            Name = "MyCat",
                        }
                    }
                },
                Profiles = new List<MachineProfile>()
                {
                    new MachineProfile()
                    {
                        Name = "Profile1"
                    },
                    new MachineProfile()
                    {
                        Name = "Profile1-1"
                    }
                },
                Instances = new List<MachineInstance>()
                {
                    new MachineInstance()
                    {
                        Name = "Instance1"
                    }
                }
            };

            MachineData expected2 = new MachineData()
            {
                BID = 1,
                ClassTypeID = ClassType.Machine.ID(),
                ID = -77,
                Name = "Machine data test2",
                Description = "the quick brown fox",
                SKU = "foo",
                ActiveInstanceCount = 2,
                ActiveProfileCount = 4,
                MachineCategoryLinks = new HashSet<MachineCategoryLink>(){
                    new MachineCategoryLink()
                    {
                        BID = 1,
                        PartID = -77,
                        MachineCategory = new MachineCategory()
                        {
                            BID = 1,
                            ID = -78,
                            Name = "Category",
                        }
                    }
                },
                Profiles = new List<MachineProfile>()
                {
                    new MachineProfile()
                    {
                        Name = "Profile2"
                    },
                    new MachineProfile()
                    {
                        Name = "Profile2-1"
                    }
                },
                Instances = new List<MachineInstance>()
                {
                    new MachineInstance()
                    {
                        Name = "Instance2"
                    }
                }
            };

            MachineData expected3 = new MachineData()
            {
                BID = 1,
                ClassTypeID = ClassType.Machine.ID(),
                ID = -77,
                Name = "Machine data test3",
                Description = "the quick brown fox",
                SKU = "foo",
                ActiveInstanceCount = 2,
                ActiveProfileCount = 4,
                MachineCategoryLinks = new HashSet<MachineCategoryLink>(){
                    new MachineCategoryLink()
                    {
                        BID = 1,
                        PartID = -77,
                        MachineCategory = new MachineCategory()
                        {
                            BID = 1,
                            ID = -79,
                            Name = "Category",
                        }
                    }
                },
                Profiles = new List<MachineProfile>()
                {
                    new MachineProfile()
                    {
                        Name = "Profile3"
                    },
                    new MachineProfile()
                    {
                        Name = "Profile3-1"
                    }
                },
                Instances = new List<MachineInstance>()
                {
                    new MachineInstance()
                    {
                        Name = "Instanc3"
                    }
                }
            };

            engy.WriteMany(new List<MachineData>() { expected1, expected2, expected3 });

            var allResults = engy.Query("");

            Assert.AreEqual(3, allResults.Count);

            var multipleResults = engy.Query("BID:1 AND MachineCategoryNames:Category");
            Assert.AreEqual(2, multipleResults.Count);
            multipleResults = engy.Query("BID:1 AND Category");
            Assert.AreEqual(2, multipleResults.Count);

            var multipleSorted = engy.Query("BID:1", 0, 15, sortBy: "MachineCategoriesSort:3");
            Assert.IsNotNull(multipleSorted);
            Assert.AreEqual(3, multipleSorted.Results.Count());
        }

        [TestMethod]
        public void AssemblyWithCategoryAndSortProperty()
        {
            var engy = new Engine<AssemblyData, int, AssemblyDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            AssemblyData expected1 = new AssemblyData()
            {
                BID = 1,
                ClassTypeID = 1500,
                ID = -77,
                AssemblyType = AssemblyType.Product,
                Name = "AssemblyTesttest",
                AssemblyCategoryLinks = new HashSet<AssemblyCategoryLink>()
                {
                    new AssemblyCategoryLink()
                    {
                        BID = 1,
                        PartID = -77,
                        AssemblyCategory = new AssemblyCategory()
                        {
                            BID = 1,
                            ID = -90,
                            Name = "MyAssemblyCat",
                        }
                    }
                },
                IsActive = true
            };

            AssemblyData expected2 = new AssemblyData()
            {
                BID = 1,
                ClassTypeID = 1500,
                ID = -78,
                AssemblyType = AssemblyType.Product,
                Name = "AssemblyTesttest",
                AssemblyCategoryLinks = new HashSet<AssemblyCategoryLink>()
                {
                    new AssemblyCategoryLink()
                    {
                        BID = 1,
                        PartID = -78,
                        AssemblyCategory = new AssemblyCategory()
                        {
                            BID = 1,
                            ID = -91,
                            Name = "AssemblyCategory",
                        }
                    }
                },
                IsActive = true
            };

            AssemblyData expected3 = new AssemblyData()
            {
                BID = 1,
                ClassTypeID = 1500,
                ID = -79,
                AssemblyType = AssemblyType.Product,
                Name = "AssemblyTesttest",
                AssemblyCategoryLinks = new HashSet<AssemblyCategoryLink>()
                {
                    new AssemblyCategoryLink()
                    {
                        BID = 1,
                        PartID = -79,
                        AssemblyCategory = new AssemblyCategory()
                        {
                            BID = 1,
                            ID = -92,
                            Name = "AssemblyCategory",
                        }
                    }
                },
                IsActive = true
            };

            engy.WriteMany(new List<AssemblyData>() { expected1, expected2, expected3 });

            var allResults = engy.Query("");

            Assert.AreEqual(3, allResults.Count);

            var multipleResults = engy.Query("BID:1 AND AssemblyCategoryNames:AssemblyCategory");
            Assert.AreEqual(2, multipleResults.Count);
            multipleResults = engy.Query("BID:1 AND AssemblyCategory");
            Assert.AreEqual(2, multipleResults.Count);


            var multipleSorted = engy.Query("BID:1", 0, 15, sortBy: "AssemblyCategoriesSort:3");
            Assert.IsNotNull(multipleSorted);
            Assert.AreEqual(3, multipleSorted.Results.Count());
        }


        [TestMethod]
        public void LaborWithCategoryAndSortProperty()
        {
            LaborData expected1 = new LaborData()
            {
                BID = 1,
                ID = -77,
                Name = "Test",
                LaborCategoryLinks = new List<LaborCategoryLink>()
                {
                    new LaborCategoryLink()
                    {
                        LaborCategory = new LaborCategory()
                        {
                            Name = "MyCat"
                        }
                    }
                }
            };

            LaborData expected2 = new LaborData()
            {
                BID = 1,
                ID = -76,
                Name = "Test2",
                LaborCategoryLinks = new List<LaborCategoryLink>()
                {
                    new LaborCategoryLink()
                    {
                        LaborCategory = new LaborCategory()
                        {
                            Name = "LaborCategory"
                        }
                    }
                }
            };

            LaborData expected3 = new LaborData()
            {
                BID = 1,
                ID = -75,
                Name = "Test3",
                LaborCategoryLinks = new List<LaborCategoryLink>()
                {
                    new LaborCategoryLink()
                    {
                        LaborCategory = new LaborCategory()
                        {
                            Name = "LaborCategory"
                        }
                    }
                }
            };

            var engy = new Engine<LaborData, int, LaborDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.WriteMany(new List<LaborData>() { expected1, expected2, expected3 });

            var allResults = engy.Query("");

            Assert.AreEqual(3, allResults.Count);

            var multipleResults = engy.Query("BID:1 AND LaborCategoryNames:LaborCategory");
            Assert.AreEqual(2, multipleResults.Count);
            multipleResults = engy.Query("BID:1 AND LaborCategory");
            Assert.AreEqual(2, multipleResults.Count);


            var multipleSorted = engy.Query("BID:1", 0, 15, sortBy: "LaborCategoriesSort:3");
            Assert.IsNotNull(multipleSorted);
            Assert.AreEqual(3, multipleSorted.Results.Count());
        }

        

        [TestMethod]
        public void TestMultipleUpdateManyDoesNotCreateDuplicates()
        {
            short testBID = 1;

            var company1 = new CompanyData()
            {
                BID = testBID,
                ID = -99,
                Name = "A TEST COMPANY",
                IsActive = true,
                IsAdHoc = false,
                StatusID = (byte)CompanyStatus.Prospect
            };
            var company2 = new CompanyData()
            {
                BID = testBID,
                ID = -98,
                Name = "B TEST COMPANY",
                IsActive = true,
                IsAdHoc = false,
                StatusID = (byte)CompanyStatus.Customer
            };
            var company3 = new CompanyData()
            {
                BID = testBID,
                ID = -97,
                Name = "C TEST COMPANY",
                IsActive = false,
                IsAdHoc = true,
                StatusID = (byte)CompanyStatus.Lead
            };

            var engine = new Engine<CompanyData, int, CompanyDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engine.UpdateMany(new List<CompanyData>() { company1, company2, company3 });
            var allResults = engine.Query("");
            Assert.AreEqual(3, allResults.Count);
            engine.UpdateMany(new List<CompanyData>() { company1, company2, company3 });
            allResults = engine.Query("");
            Assert.AreEqual(3, allResults.Count);
        }

        [TestMethod]
        public void TestMessageBodyTemplate()
        {
            var engy = new Engine<MessageBodyTemplate, short, MessageBodyTemplateSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            MessageBodyTemplate expected = new MessageBodyTemplate()
            {
                BID = 1,
                ClassTypeID = 14220,
                ID = 3,
                Subject = "Email",
                Body = "This the body template of {{Order.OrderNumber}}"
            };
            engy.Write(expected);

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query("Subject:Email AND Body:template");
            Assert.IsTrue(singleResults.Count == 1);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);
        }

        [TestMethod]
        public void TestReconciliation()
        {
            var engy = new Engine<Reconciliation, int, ReconciliationSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            Reconciliation expected = new Reconciliation()
            {
                BID = 1,
                ID = 3,
                Description = "foo barino",
            };
            engy.Write(expected);

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

            var singleResults = engy.Query("BID:1 AND foo");
            Assert.IsTrue(singleResults.Count == 1);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ID, actual.ID);
        }*/
    }
}
