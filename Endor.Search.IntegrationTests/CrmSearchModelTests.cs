﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class CrmSearchModelTests : BaseEntitySearchTestClass
    {
        [TestMethod]
        public async Task TestCrmIndustrySearchModel() 
        {
            var repo = ((TestDapperRepository)GetDapperRepository());

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.CrmIndustryTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.CrmIndustryTableName,
                ChildObjectTableInfo.IDColumn,
                new
                {
                    BID,
                    ID = IDToUse + 1,
                });

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.CrmIndustryTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            var expected = new
            {
                BID = BID,
                ID = IDToUse,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                Name = "UNITTESTCrmIndustrySearchModel"
            };

            await InsertSqlExec(repo, ChildObjectTableInfo.CrmIndustryTableName, expected);

            var resultList = await repo.GetSearchModelByIdAsync<CrmIndustrySearchModel>(new int[] { IDToUse });
            var engy = new Engine<CrmIndustrySearchModel>(myTestRunIndexName,
                MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());
            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            var singleResults = engy.Query($"BID:1 AND Name:UNITTESTCrmIndustrySearchModel");
            Assert.IsTrue(singleResults.Count == 1);
            var noResult = engy.Query($"BID:1 AND Name:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ID, actual.ID);
        }

        [TestMethod]
        public async Task TestCrmOriginSearchModel() 
        {
            var repo = ((TestDapperRepository)GetDapperRepository());

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.CrmOriginTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.CrmOriginTableName,
                ChildObjectTableInfo.IDColumn,
                new
                {
                    BID,
                    ID = IDToUse + 1,
                });

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.CrmOriginTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            var expected = new
            {
                BID,
                ID = IDToUse,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                Name = "UNITTESTCrmOriginSearchModel"
            };

            await InsertSqlExec(repo, ChildObjectTableInfo.CrmOriginTableName, expected);

            var resultList = await repo.GetSearchModelByIdAsync<CrmOriginSearchModel>(new int[] { IDToUse });
            var engy = new Engine<CrmOriginSearchModel>(myTestRunIndexName,
                MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());
            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            var singleResults = engy.Query($"BID:1 AND Name:UNITTESTCrmOriginSearchModel");
            Assert.IsTrue(singleResults.Count == 1);
            var noResult = engy.Query($"BID:1 AND Name:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ID, actual.ID);
        }
    }
}
