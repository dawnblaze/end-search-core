﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class BoardDefinitionSearchModelTests : BaseEntitySearchTestClass
    {

        [TestMethod]
        public async Task TestBoardDefinitionSearchModel()
        {
            var repo = ((TestDapperRepository)GetDapperRepository());

            await DeleteSqlExec(repo, ChildObjectTableInfo.BoardDefinitionDataTableName, "ID", defaultParam);

            var board = new
            {
                BID = BID,
                ID = IDToUse,
                Name = "Test",
                IsSystem = false,
                IsActive = false,
                DataType = 0,
                LimitToRoles = 0,
                IsAlwaysShown = 0,
                CummRunDurationSec = 0.0001
            };

            await InsertSqlExec(repo, ChildObjectTableInfo.BoardDefinitionDataTableName, board);

            var resultList = await repo.GetSearchModelByIdAsync<BoardDefinitionDataSearchModel>(new int[] { IDToUse });

            var engine = new Engine<BoardDefinitionDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            engine.WriteMany(resultList);

            var allResults = engine.Query("");

            var multipleResults = engine.Query($"BID:1 AND Name:Test");
            Assert.AreEqual(0.0001m, multipleResults.First().CummRunDurationSec);
            Assert.AreEqual(1, multipleResults.Count);
        }
    }
}
