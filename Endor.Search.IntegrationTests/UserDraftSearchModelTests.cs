﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class UserDraftSearchModelTests : BaseEntitySearchTestClass
    {
        [TestMethod]
        public async Task TestUserDraftSearchModel()
        {
            var IDToUse = -100;
            var BID = 1;
            var repo = ((TestDapperRepository)GetDapperRepository());
            object defaultParam = new
            {
                BID = BID,
                ID = IDToUse,
            };
            
            await DeleteSqlExec(repo, ChildObjectTableInfo.UserDraftTableName, ChildObjectTableInfo.IDColumn, defaultParam);

            var userdraft = new
            {
                BID = 1,
                ID = IDToUse,
                ExpirationDT = DateTime.Now,
                UserLinkID = 1,
                Description = "Teddy Bear Banner",
                ObjectCTID = 10000,
                MetaData = "{\"Company\": { \"Name\":\"Test\"}}"
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.UserDraftTableName, userdraft);

            var resultList = await repo.GetSearchModelByIdAsync<UserDraftSearchModel>(new int[] { IDToUse });

            var engy = new Engine<UserDraftSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            engy.Write(resultList.FirstOrDefault());

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            Assert.AreEqual("Test", allResults.FirstOrDefault().CompanyName);

        }
    }
}
