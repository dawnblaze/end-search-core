﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class ContactSearchModelTests : BaseEntitySearchTestClass
    {
        
        [TestMethod]
        public async Task TestContactWithCompanyContactLinks()
        {
            var repo = ((TestDapperRepository)GetDapperRepository());

            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, "ContactID", defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, "ContactID", new
            {
                BID = BID,
                ID = IDToUse + 1,
            });
            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, "ContactID", new
            {
                BID = BID,
                ID = IDToUse + 2,
            });
            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, ChildObjectTableInfo.IDColumn, defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, ChildObjectTableInfo.IDColumn, new
            {
                BID = BID,
                ID = IDToUse + 1,
            });
            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, ChildObjectTableInfo.IDColumn, new
            {
                BID = BID,
                ID = IDToUse + 2,
            });
            await DeleteSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, ChildObjectTableInfo.IDColumn, defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, ChildObjectTableInfo.IDColumn, new
            {
                BID = BID,
                ID = IDToUse + 1,
            });
            await DeleteSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, ChildObjectTableInfo.IDColumn, new
            {
                BID = BID,
                ID = IDToUse + 2,
            });


            var company1 = new
            {
                BID = BID,
                ID = IDToUse,
                Name = "A TEST COMPANY",
                IsActive = true,
                IsAdHoc = false,
                StatusID = 1
            };
            var company2 = new
            {
                BID = BID,
                ID = IDToUse + 1,
                Name = "B TEST COMPANY",
                IsActive = true,
                IsAdHoc = false,
                StatusID = 1
            };
            var company3 = new
            {
                BID = BID,
                ID = IDToUse + 2,
                Name = "C TEST COMPANY",
                IsActive = false,
                IsAdHoc = true,
                StatusID = 1
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, company1);
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, company2);
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, company3);

            var expected1 = new
            {
                BID = BID,
                ID = IDToUse,
                First = "A TEST",
                Last = "SUBJECT",
                StatusID = 2,
            };
            var expected2 = new
            {
                BID = BID,
                ID = IDToUse + 1,
                First = "B TEST",
                Last = "SUBJECT",
                StatusID = 4
            };
            var expected3 = new
            {
                BID = BID,
                ID = IDToUse + 2,
                First = "C TEST",
                Last = "SUBJECT",
                StatusID = 1
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, expected1);
            await InsertSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, expected2);
            await InsertSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, expected3);

            var CompanyContactLink1 = new
            {
                BID = BID,
                ContactID = expected1.ID,
                CompanyID = company1.ID,
                Roles = ContactRole.Primary
            };

            var CompanyContactLink2 = new
            {
                BID = BID,
                ContactID = expected2.ID,
                CompanyID = company1.ID,
                Roles = ContactRole.Billing,
            };
            var CompanyContactLink3 = new
            {
                BID = BID,
                ContactID = expected2.ID,
                CompanyID = company2.ID,
                Roles = ContactRole.Primary | ContactRole.Billing,
            };
            var CompanyContactLink4 = new
            {
                BID = BID,
                ContactID = expected3.ID,
                CompanyID = company3.ID,
                Roles = ContactRole.Inactive,
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, CompanyContactLink1);
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, CompanyContactLink2);
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, CompanyContactLink3);
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, CompanyContactLink4);

            var resultList = await repo.GetSearchModelByIdAsync<ContactDataSearchModel>(new int[] { IDToUse, IDToUse+1,IDToUse+2 });

            var engine = new Engine<ContactDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            engine.WriteMany(resultList);

            var allResults = engine.Query("");

            Assert.AreEqual(3, allResults.Count);

            var multipleResults = engine.Query("BID:1 AND IsActive:true");
            Assert.AreEqual(2, multipleResults.Count);
            multipleResults = engine.Query("BID:1 AND IsActive:false");
            Assert.AreEqual(1, multipleResults.Count);
            multipleResults = engine.Query($"BID:1 AND StatusID:4"); //Customer
            Assert.AreEqual(1, multipleResults.Count);
            multipleResults = engine.Query($"BID:1 AND StatusID:1");//Lead
            Assert.AreEqual(1, multipleResults.Count);
            multipleResults = engine.Query($"BID:1 AND StatusID:2");//Prospect
            Assert.AreEqual(1, multipleResults.Count);
            multipleResults = engine.Query("BID:1 AND CompanyIDs:-98");
            Assert.AreEqual(1, multipleResults.Count);
            multipleResults = engine.Query("BID:1 AND CompanyIDs:-100");
            Assert.AreEqual(2, multipleResults.Count);
        }
    }
}
