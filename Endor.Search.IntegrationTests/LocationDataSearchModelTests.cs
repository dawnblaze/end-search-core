﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class LocationDataSearchModelTests : BaseEntitySearchTestClass
    {
        [TestMethod]
        public async Task TestLocationDataSearchModel()
        {
            var repo = ((TestDapperRepository)GetDapperRepository());

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.LocationTableName,
                ChildObjectTableInfo.IDColumn,
                new
                {
                    BID,
                    ID = IDToUse + 354,
                });

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.LocationTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            var expected = new
            {
                BID = BID,
                ID = IDToUse + 354,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                TimeZoneID = 10,
                Name = "UNITTESTLocationDataSearchModel",
                LegalName = "UNITTESTLocationDataSearchModel"
            };

            await InsertSqlExec(repo, ChildObjectTableInfo.LocationTableName, expected);

            var resultList = await repo.GetSearchModelByIdAsync<LocationDataSearchModel>(new int[] { IDToUse + 354 });
            var engy = new Engine<LocationDataSearchModel>(myTestRunIndexName,
                MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());
            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            var singleResults = engy.Query($"BID:1 AND Name:UNITTESTLocationDataSearchModel");
            Assert.IsTrue(singleResults.Count == 1);
            var noResult = engy.Query($"BID:1 AND Name:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ID, actual.ID);
        }
    }
}

