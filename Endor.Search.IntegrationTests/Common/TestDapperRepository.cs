﻿using Dapper;
using Endor.Search.infrastructure.Common;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Caching.Memory;
using System.Data;
using System.Threading.Tasks;
using static Endor.Search.IntegrationTests.Common.UnitTestHelpers;

namespace Endor.Search.IntegrationTests.Common
{
    public class TestDapperRepository : DapperRepository, IDapperReadOnlyRepository
    {
        public override IDbConnection Connection()
        {
            var tenantCache = new MockTenantDataCache();
            return new SqlConnection(tenantCache.Get(_BID).Result.BusinessDBConnectionString);
        }

        public TestDapperRepository(IMemoryCache cache)
            : base(BID, "", "", cache)
        {
            _BID = BID;
        }

        public async Task<int> ExecuteSqlRawAsync(string sql, params object[] parameters)
        {
            var param = new DynamicParameters();
            foreach (var p in parameters)
            {
                if (p is IDataParameter)
                {
                    var DbP = (IDataParameter)p;
                    param.Add(DbP.ParameterName, DbP.Value);
                }
            }
            using (IDbConnection cn = Connection())
            {
                cn.Open();
                return await cn.ExecuteAsync(sql, param);
            }
        }

        public async Task<int> ExecuteSqlRawAsync(string sql, object param)
        {
            using (IDbConnection cn = Connection())
            {
                cn.Open();
                return await cn.ExecuteAsync(sql, param);
            }
        }
    }
}
