﻿using Endor.Tenant;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Search.IntegrationTests.Common
{
    public class MockTenantDataCache : ITenantDataCache
    {
        public static Dictionary<string, string> GetLocalSettings()
        {
            string file = "..\\..\\..\\..\\test-config.json"; //put this inside end-messaging folder
            if (!File.Exists(file))
            {
                File.WriteAllText(file, @"{ 
""BusinessDBConnectionString"":""Data Source=localhost\\SQLEXPRESS;Initial Catalog=\""Dev.Endor.Business.DB1\"";User ID=sa;Password=pass123456"",
""StorageConnectionString"":""UseDevelopmentStorage=true"",
""StorageURL"":""http://127.0.0.1:10000/devstoreaccount1/""
}");
            }

            string text = File.ReadAllText(file);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(text);
            return result;
        }

        private static TenantData Mock = new TenantData()
        {
            APIURL = "https://endorapi.localcyriousdevelopment.com:5002/",
            ReportingURL = "",
            MessagingURL = "https://endormessaging.localcyriousdevelopment.com:5004/",
            LoggingURL = "https://endorlog.localcyriousdevelopment.com:5003/",
            BackgroundEngineURL = "https://endorbgengine.localcyriousdevelopment.com:5006/",
            //StorageURL = GetLocalSettings()["StorageURL"],
            StorageConnectionString = GetLocalSettings()["StorageConnectionString"],
            //IndexStorageConnectionString = GetLocalSettings()["StorageConnectionString"],
            BusinessDBConnectionString = GetLocalSettings()["BusinessDBConnectionString"],
            LoggingDBConnectionString = "",
            MessagingDBConnectionString = "",
            SystemReportBConnectionString = "",
            SystemDataDBConnectionString = "",
        };

        public Task<TenantData> Get(short bid)
        {
            if (bid == 1 || bid == 2)
                return Task.FromResult(Mock);
            else
                return null;
        }

        public void InvalidateCache()
        {
        }

        public void InvalidateCache(short bid)
        {
        }
    }
}
