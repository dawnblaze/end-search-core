﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Endor.Search.IntegrationTests.Common
{
    public class UnitTestHelpers
    {
        public const short BID = 1;
        public const byte AID = 1;
        public const short AuthUserLinkID = 1;
        public const int AuthUserID = 1;
        public const string AuthUserName = "TestUser";
        public const short AuthEmployeeID = 1;
        public const short AccessType = 49;

    }
}
