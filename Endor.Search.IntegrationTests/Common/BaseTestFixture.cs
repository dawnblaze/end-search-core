﻿using Endor.Search.infrastructure.Common;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using Endor.Common;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Endor.Search.IntegrationTests.Common
{
    public class BaseTestFixture
    {
        protected IMemoryCache _cache;

        protected IDapperReadOnlyRepository GetDapperRepository(IMemoryCache memoryCache)
        {
            return new TestDapperRepository(memoryCache);
        }

        protected byte GetValidLocationID(IDapperReadOnlyRepository repository)
        {
            repository.SetTableName("[dbo].[Location.Data]");
            return (repository.List<TestEntity>(new GetValidIDByBIDSpecification<TestEntity>(UnitTestHelpers.BID))).First().IDAsByte;
        }

        protected int GetValidEmployeeID(IDapperReadOnlyRepository repository)
        {
            repository.SetTableName("[dbo].[Employee.Data]");
            return (repository.List<TestEntity>(new GetValidIDByBIDSpecification<TestEntity>(UnitTestHelpers.BID))).First().ID;
        }
        protected string CTIDQueryClause(int ctid)
        {
            return $"ClassTypeID:{ ctid }";
        }

        protected string GetEffectiveSearch(string search, int ctid)
        {
            if (!String.IsNullOrWhiteSpace(search))
                //remove spaces before a *
                search = new System.Text.RegularExpressions.Regex("\\s+\\*").Replace(search, "\\*");

            if (String.IsNullOrWhiteSpace(search))
                search = CTIDQueryClause(ctid);
            else
            {
                search = $"{CTIDQueryClause(ctid)} AND {search}";
            }
            return search;
        }

        protected IDapperReadOnlyRepository GetDapperRepository()
        {
            return new TestDapperRepository(_cache);
        }
    }
}
