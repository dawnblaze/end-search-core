﻿using Endor.Search.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.IntegrationTests.Common
{
    public class TestEntity : BaseIdentityEntity
    {
        public override int ClassTypeID { get => ClassTypeIDValue; set { } }

        public const int ClassTypeIDValue = 0;

    }
}
