﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class CreditMemoDataSearchModelTests : BaseEntitySearchTestClass
    {
        [TestMethod]
        public async Task TestCreditMemoDataSearchModel()
        {
            var repo = ((TestDapperRepository)GetDapperRepository());

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.OrderDataTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.OrderDataTableName,
                ChildObjectTableInfo.IDColumn,
                new
                {
                    BID = BID,
                    ID = IDToUse + 1,
                });

            await DeleteSqlExec(repo,
                ChildObjectTableInfo.OrderDataTableName,
                ChildObjectTableInfo.IDColumn,
                defaultParam);

            var expected =  new
            {
                BID,
                ID = IDToUse,
                LocationID = 1,
                TransactionType = 8,
                OrderStatusID = 81,
                OrderStatusStartDT = DateTime.UtcNow,
                Number = 1000,
                FormattedNumber = "UNT-TST",
                PriceTaxRate = 0.01m,
                PaymentPaid = 0m,
                TaxGroupID = 1,
                PriceProductTotal = 100m,
                PriceTax = 5m,
                CompanyID = 1,
                PickupLocationID = 1,
                ProductionLocationID = 1,
                DestinationType = 0,
                HasDocuments = false,
                HasOrderLinks = false,
                HasSingleDestination = true,
                PriceTaxableOV = 0,
                TaxGroupOV = 0
            };

            string insertQuery = $@"INSERT INTO {ChildObjectTableInfo.OrderDataTableName}
                (BID
, ID
, LocationID
, TransactionType
, OrderStatusID
, OrderStatusStartDT
, Number
, FormattedNumber
, [Price.TaxRate]
, TaxGroupID
, [Price.ProductTotal]
, [Price.Tax]
, CompanyID
, PickupLocationID
, ProductionLocationID
, DestinationType
, HasDocuments
, HasOrderLinks
, HasSingleDestination
, [Price.TaxableOV]
, TaxGroupOV) 
VALUES 
(
{expected.BID}
, {expected.ID}
, {expected.LocationID}
, {expected.TransactionType}
, {expected.OrderStatusID}
, '{expected.OrderStatusStartDT}'
, {expected.Number}
, '{expected.FormattedNumber}'
, {expected.PriceTaxRate}
, {expected.TaxGroupID}
, {expected.PriceProductTotal}
, {expected.PriceTax}
, {expected.CompanyID}
, {expected.PickupLocationID}
, {expected.ProductionLocationID}
, {expected.DestinationType}
, 0
, 0
, 0
, 0
, {expected.TaxGroupOV}
)";
            await repo.ExecuteSqlRawAsync(insertQuery, expected);

            var resultList = await repo.GetSearchModelByIdAsync<CreditMemoDataSearchModel>(new int[] { IDToUse });
            var engy = new Engine<CreditMemoDataSearchModel>(myTestRunIndexName,
                MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);
            engy.Write(resultList.FirstOrDefault());
            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);
            var singleResults = engy.Query($"BID:1 AND FormattedNumber:UNT-TST");
            Assert.IsTrue(singleResults.Count == 1);

            var noResult = engy.Query($"BID:1 AND FormattedNumber:NoExistentSearchModel");
            Assert.IsTrue(noResult.Count == 0);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ID, actual.ID);
        }
    }
}
