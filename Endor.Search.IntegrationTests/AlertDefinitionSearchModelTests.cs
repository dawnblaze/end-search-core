﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class AlertDefinitionSearchModelTests : BaseEntitySearchTestClass
    {
        [TestMethod]
        public async Task TestAlertDefinitionSearchModel()
        {
            var repo = ((TestDapperRepository)GetDapperRepository());

            await DeleteSqlExec(repo, ChildObjectTableInfo.AlertDefinitionActionTableName, ChildObjectTableInfo.IDColumn, defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.AlertDefinitionActionTableName, ChildObjectTableInfo.IDColumn, new
            {
                BID = BID,
                ID = IDToUse+1,
            });
            await DeleteSqlExec(repo, ChildObjectTableInfo.AlertDefinitionTableName, ChildObjectTableInfo.IDColumn, defaultParam);

            var employeeID = GetValidEmployeeID(repo);

            var expected = new
            {
                BID = BID,
                ID = IDToUse,
                Name = "Test",
                TriggerID = 1,
                DataType = 1,
                EmployeeID = employeeID,
                HasImage = false,
                IsActive = true,
                IsSystem = false,
                
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.AlertDefinitionTableName, expected);

            var action1 = new
            {
                BID = BID,
                ID = IDToUse,
                AlertDefinitionID = IDToUse,
                ActionDefinitionID = 1,
                Body = "Test email",
                SubjectHasMergeFields = false,
                BodyHasMergeFields = false,
                DataType = 1,
                SortIndex = 1
            };
            var action2 = new
            {
                BID = BID,
                ID = IDToUse+1,
                AlertDefinitionID = IDToUse,
                ActionDefinitionID = 2,
                Body = "Test sms",
                SubjectHasMergeFields = false,
                BodyHasMergeFields = false,
                DataType = 1,
                SortIndex = 1,
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.AlertDefinitionActionTableName, action1);
            await InsertSqlExec(repo, ChildObjectTableInfo.AlertDefinitionActionTableName, action2);

            var resultList = await repo.GetSearchModelByIdAsync<AlertDefinitionSearchModel>(new int[] { IDToUse });

            var engy = new Engine<AlertDefinitionSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            engy.Write(resultList.FirstOrDefault());

            var allResults = engy.Query("");
            Assert.IsTrue(allResults.Count == 1);

             //test: can search on TriggerCategoryType
             var singleResults = engy.Query($"BID:1 AND Company");
            Assert.IsTrue(singleResults.Count == 1);

            var actual = singleResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            //Assert.AreEqual(expected.BID, actual.BID);
            //Assert.AreEqual(expected.ClassTypeID, actual.ClassTypeID);
            Assert.AreEqual(expected.ID, actual.ID);
            //test: search results have alerts
            Assert.IsTrue(actual.Alerts.Contains("Email"));

            //Commenting this out for now, not sure why the query is failing and it can't search on TriggerReadable
            //test: can search on TriggerReadable
            //singleResults = engy.Query("BID:1 AND created");
            //Assert.IsTrue(singleResults.Count == 1);
        }
    }
}
