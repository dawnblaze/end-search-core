﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.IntegrationTests
{
    [TestClass]
    public class CompanySearchModelTests : BaseEntitySearchTestClass
    {

        [TestMethod]
        public async Task CompanyWithContactContactLinks()
        {
            var repo = ((TestDapperRepository)GetDapperRepository());

            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, "ContactID", defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, "ContactID", new
            {
                BID = BID,
                ID = IDToUse + 1,
            });
            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, "ContactID", new
            {
                BID = BID,
                ID = IDToUse + 2,
            });
            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, ChildObjectTableInfo.IDColumn, defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, ChildObjectTableInfo.IDColumn, new
            {
                BID = BID,
                ID = IDToUse + 1,
            });
            await DeleteSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, ChildObjectTableInfo.IDColumn, new
            {
                BID = BID,
                ID = IDToUse + 2,
            });
            await DeleteSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, ChildObjectTableInfo.IDColumn, defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, ChildObjectTableInfo.IDColumn, new
            {
                BID = BID,
                ID = IDToUse + 1,
            });
            await DeleteSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, ChildObjectTableInfo.IDColumn, new
            {
                BID = BID,
                ID = IDToUse + 2,
            });


            var company1 = new
            {
                BID = BID,
                ID = IDToUse,
                Name = "A TEST COMPANY",
                IsActive = true,
                IsAdHoc = false,
                StatusID = 1
            };
            var company2 = new
            {
                BID = BID,
                ID = IDToUse + 1,
                Name = "B TEST COMPANY",
                IsActive = true,
                IsAdHoc = false,
                StatusID = 1
            };
            var company3 = new
            {
                BID = BID,
                ID = IDToUse + 2,
                Name = "C TEST COMPANY",
                IsActive = false,
                IsAdHoc = true,
                StatusID = 1
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, company1);
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, company2);
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyDataTableName, company3);

            var expected1 = new
            {
                BID = BID,
                ID = IDToUse,
                First = "A TEST",
                Last = "SUBJECT",
                StatusID = 2,
            };
            var expected2 = new
            {
                BID = BID,
                ID = IDToUse + 1,
                First = "B TEST",
                Last = "SUBJECT",
                StatusID = 4
            };
            var expected3 = new
            {
                BID = BID,
                ID = IDToUse + 2,
                First = "C TEST",
                Last = "SUBJECT",
                StatusID = 1
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, expected1);
            await InsertSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, expected2);
            await InsertSqlExec(repo, ChildObjectTableInfo.ContactDataTableName, expected3);

            var CompanyContactLink1 = new
            {
                BID = BID,
                ContactID = expected1.ID,
                CompanyID = company1.ID,
                Roles = ContactRole.Primary
            };

            var CompanyContactLink2 = new
            {
                BID = BID,
                ContactID = expected2.ID,
                CompanyID = company1.ID,
                Roles = ContactRole.Billing,
            };
            var CompanyContactLink3 = new
            {
                BID = BID,
                ContactID = expected2.ID,
                CompanyID = company2.ID,
                Roles = ContactRole.Primary | ContactRole.Billing,
            };
            var CompanyContactLink4 = new
            {
                BID = BID,
                ContactID = expected3.ID,
                CompanyID = company3.ID,
                Roles = ContactRole.Inactive,
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, CompanyContactLink1);
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, CompanyContactLink2);
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, CompanyContactLink3);
            await InsertSqlExec(repo, ChildObjectTableInfo.CompanyContactLinkTableName, CompanyContactLink4);

            var resultList = await repo.GetSearchModelByIdAsync<CompanyDataSearchModel>(new int[] { IDToUse, IDToUse + 1, IDToUse + 2 });

            var engine = new Engine<CompanyDataSearchModel>(myTestRunIndexName, MockTenantDataCache.GetLocalSettings()["StorageConnectionString"]);

            engine.WriteMany(resultList);

            var allResults = engine.Query("");

            Assert.AreEqual(2, allResults.Count);

            var multipleResults = engine.Query("BID:1 AND IsActive:true");
            Assert.AreEqual(2, multipleResults.Count);
            multipleResults = engine.Query("BID:1 AND IsActive:false");
            Assert.AreEqual(0, multipleResults.Count);
        }
    }

    public enum ContactRole : byte
    {
        Inactive = 0,
        Primary = 1,
        Billing = 2,
        Owner = 4,
        Observer = 128
    }
}
