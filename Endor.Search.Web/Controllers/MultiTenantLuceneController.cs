﻿using Endor.Logging.Client;
using Endor.Security;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.web.Controllers
{
    public class MultiTenantLuceneController : Controller
    {

        #region Private Helper Methods
        protected bool ValidateBID(out short bid)
        {
            short? _bid = User.BID();

            if (!_bid.HasValue || _bid.Value < 0)
            {
                bid = default(short);
                return false;
            }
            bid = _bid.Value;
            return true;
        }

        protected IActionResult InvalidBIDMessage()
        {
            return Unauthorized();
        }
        #endregion
    }
}
