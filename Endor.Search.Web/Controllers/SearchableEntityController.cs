﻿using Endor.Search.Core;
using Endor.Search.Core.Interfaces;
using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Common;
using Endor.Search.infrastructure.Services;
using Endor.Search.web.Common;
using Endor.Search.web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Security;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public abstract class SearchableEntityController<S> : SearchableEntityController
        where S : IAutomappableSearchModel<S>, new()
    {
        protected ITenantDataCache _tenantCache;
        protected IMemoryCache _cache;
        /// <summary>
        /// Logger
        /// </summary>
        protected RemoteLogger _logger;
        /// <summary>
        /// RTM Client
        /// </summary>
        protected IRTMPushClient _rtmClient;
        /// <summary>
        /// HTTPContext
        /// </summary>
        protected IHttpContextAccessor _httpCtx;

        private Lazy<SearchableEntityService<S>> _Service; 
        public SearchableEntityService<S> EntityService;


        protected string ConnectionString;
        protected short BID;
        protected IDapperReadOnlyRepository _dapperRepository;


        public virtual int ClassTypeID { get => ClassTypeIDMap.CTIDForClassType( typeof(S) ); }
        public SearchableEntityController(IHttpContextAccessor httpCtx)
        {
            var serviceProvider = httpCtx.HttpContext.RequestServices;
            _cache = (IMemoryCache)serviceProvider.GetService(typeof(IMemoryCache));
            _tenantCache = (ITenantDataCache)serviceProvider.GetService(typeof(ITenantDataCache));
            _logger = (RemoteLogger)serviceProvider.GetService(typeof(RemoteLogger));
            _rtmClient = (IRTMPushClient)serviceProvider.GetService(typeof(IRTMPushClient));
            _httpCtx = httpCtx;
            CommonInitialization();
        }

        private void CommonInitialization()
        {
            ValidateBID(out BID);
            _Service = new Lazy<SearchableEntityService<S>>(() => new SearchableEntityService<S>(_logger, _tenantCache, _rtmClient, ClassTypeID));
            EntityService = _Service.Value;
            ConnectionString = _tenantCache.Get(BID).Result.BusinessDBConnectionString;
            _dapperRepository = new DapperRepository(BID, "", ConnectionString, _cache);
        }

        #region public endpoints
        // GET api/<modelname>?search=<search>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]string search)
        {
            short bid;
            if (!ValidateBID(out bid))
                return InvalidBIDMessage();

            List<S> result = await EntityService.GetListAsync(bid, search);

            return Ok(result);
        }

        // GET api/<modelname>/page?search=<search>
        [HttpGet("page")]
        public async Task<IActionResult> GetPageAsync([FromQuery]PageQueryParameters param)
        {
            short bid;
            if (!ValidateBID(out bid))
                return InvalidBIDMessage();

            PagedSearchResult<S> result = await EntityService.GetPageAsync(bid, param?.skip ?? 0, param?.perPage, param?.search);

            return Ok(result);
        }

        // POST api/<modelname>/indexSingle
        [HttpPost("indexSingle")]
        public async Task<IActionResult> Post([FromBody]S m)
        {
            short bid;
            if (!ValidateBID(out bid))
                return InvalidBIDMessage();

            await EntityService.IndexSingle(bid, m);
            return Ok();
        }

        // POST api/<modelname>/indexMany
        [HttpPost("indexMany")]
        public async Task<IActionResult> PostMany([FromBody]IEnumerable<S> ms)
        {
            short bid;
            if (!ValidateBID(out bid))
                return InvalidBIDMessage();

            await EntityService.IndexMany(bid, ms);

            return Ok();
        }

        // DELETE api/<modelname>/<id>
        [HttpDelete("deletesingle")]
        public async Task<IActionResult> Delete(int id)
        {
            short bid;
            if (!ValidateBID(out bid))
                return InvalidBIDMessage();

            await EntityService.DeleteSingle(bid, id);

            return Ok();
        }
        
        [HttpDelete("deleteall")]
        public async Task<IActionResult> DeleteAll()
        {
            short bid;
            if (!ValidateBID(out bid))
                return InvalidBIDMessage();

            await EntityService.DeleteAll(bid);

            return Ok();
        }
        #endregion


        #region Private Helper Methods
        private bool ValidateBID(out short bid)
        {
            short? _bid = _httpCtx.HttpContext.User.BID();

            if (!_bid.HasValue || _bid.Value < 0)
            {
                bid = default(short);
                return false;
            }
            bid = _bid.Value;
            return true;
        }

        private IActionResult InvalidBIDMessage()
        {
            return Unauthorized();
        }

        internal async Task<List<S>> GetListAsync(short bid, string search)
        {
            await _logger.Information(bid, $"Search called for {typeof(S)} with the search string of \"{search}\".");

            search = GetEffectiveSearch(search);

            var engine = await Engine<S>.Create(bid, _tenantCache, false);
            
            return engine.Query(search, EndorLuceneConstants.DefaultHits);
        }

        internal async Task<PagedSearchResult<S>> GetPageAsync(short bid, int skip, int? perPage = null, string search = null, string sortBy = null)
        {
            await _logger.Information(bid, $"Search page called for {typeof(S)}.");

            search = GetEffectiveSearch(search);
            int effectivePerPage = perPage ?? EndorLuceneConstants.DefaultPerPage;
            var engine = await Engine<S>.Create(bid, _tenantCache, false);

            return engine.Query(search, skip, effectivePerPage, sortBy: sortBy);
        }

        
        private string GetEffectiveSearch(string search)
        {
            if (!String.IsNullOrWhiteSpace(search))
                //remove spaces before a *
                search = new System.Text.RegularExpressions.Regex("\\s+\\*").Replace(search, "\\*");

            if (String.IsNullOrWhiteSpace(search))
                search = CTIDQueryClause();
            else
            {
                search = $"{CTIDQueryClause()} AND {search}";
            }
            return search;
        }

        private string CTIDQueryClause()
        {
            return $"ClassTypeID:{ ClassTypeID }";
        }

        internal override async Task<object> GetSimpleObjectAsync(short bid, string mySearchString)
        {
            return (await EntityService.GetListAsync(bid, mySearchString)).Select(x => x.ToSimpleSearchModelDTO());
        }

        internal override async Task<object> GetObjectAsync(short bid, string mySearchString)
        {
            return await EntityService.GetListAsync(bid, mySearchString);
        }

        internal override async Task<object> GetObjectByPageAsync(short bid, int page, int perPage, string mySearchString, string sortBy = null)
        {
            return await EntityService.GetPageAsync(bid, page, perPage, mySearchString, sortBy);
        }
        #endregion
    }

    public abstract class SearchableEntityController : Controller
    {
        public virtual SearchableEntityService Service => null;

        internal abstract Task<object> GetSimpleObjectAsync(short bid, string mySearchString);

        internal abstract Task<object> GetObjectAsync(short bid, string mySearchString);

        internal abstract Task<object> GetObjectByPageAsync(short bid, int page, int perPage, string mySearchString, string sortBy = null);
    }
}