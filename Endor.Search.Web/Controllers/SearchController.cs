﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.web.Common;
using Endor.Search.web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Endor.Search.web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class SearchController : MultiTenantLuceneController
    {
        private static string myNamespace;

        internal static List<string> AvailableSearchEntityRoutes = new List<string>();
        internal static List<string> MisnamedSearchableEntityControllers = new List<string>();

        public SearchController(RemoteLogger logger, ITenantDataCache cache, IMemoryCache memoryCache, IRTMPushClient rtmClient, IHttpContextAccessor httpCtx)
        {
            myNamespace = typeof(SearchController).Namespace;
            this._logger = logger;
            this._cache = cache;
            this._memoryCache = memoryCache;
            this._httpCtx = httpCtx;
            this._rtmClient = rtmClient;
        }

        /// <summary>
        /// Logger
        /// </summary>
        protected readonly RemoteLogger _logger;
        /// <summary>
        /// Logger
        /// </summary>
        protected readonly ITenantDataCache _cache;
        /// <summary>
        /// RTM Client
        /// </summary>
        protected IRTMPushClient _rtmClient;
        /// <summary>
        /// HTTPContext
        /// </summary>
        protected IHttpContextAccessor _httpCtx;
        protected IMemoryCache _memoryCache;

        private Lazy<SearchService> _Service => new Lazy<SearchService>(() => new SearchService(_logger, _cache, _httpCtx));
        public SearchService Service => _Service.Value;

        // GET: api/search?search=<query>&ctid=<classTypeID>&top=20
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]string search, [FromQuery]int? ctid = null, [FromQuery]int top = EndorLuceneConstants.DefaultHits, [FromQuery] bool simple = false)
        {
            short bid;
            if (!ValidateBID(out bid))
                return InvalidBIDMessage();

            //var result = await Service.Search(bid, search, ctid, top, simple);
            if (!ctid.HasValue)
            {
                List<WildcardSearchModel> results = await new SearchableWildcardService(_cache).GetAsync(bid, search, top);

                return Ok(results);
            }
            else if (simple)
            {
                return await InvokeOnControllerByCTIDAsync(bid, ctid.Value, nameof(SearchableEntityController.GetSimpleObjectAsync), new object[] { bid, search });
            }
            else
            {
                return await InvokeOnControllerByCTIDAsync(bid, ctid.Value, nameof(SearchableEntityController.GetObjectAsync), new object[] { bid, search });
            }
        }

        // GET: api/search/page/<pageNumber>?perPage=20&search=<query>&ctid=<classTypeID>
        [HttpGet]
        [Route("page")]
        public async Task<IActionResult> GetPage([FromQuery] int skip, [FromQuery]string search, [FromQuery]int perPage = EndorLuceneConstants.DefaultPerPage, [FromQuery]int? ctid = null, [FromQuery]string sortBy = null)
        {
            short bid;
            if (!ValidateBID(out bid))
                return InvalidBIDMessage();

            if (!ctid.HasValue)
            {
                PagedSearchResult<WildcardSearchModel> result = await new SearchableWildcardService(_cache).GetPageAsync(bid, skip, perPage, search);

                return Ok(result);
            }
            else
            {
                return await InvokeOnControllerByCTIDAsync(bid, ctid.Value, nameof(SearchableEntityController.GetObjectByPageAsync), new object[] { bid, skip, perPage, search, sortBy });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("entities")]
        public IActionResult GetEntities()
        {
            return Ok(AvailableSearchEntityRoutes);
        }

        private async Task<IActionResult> InvokeOnControllerByCTIDAsync(short bid, int ctid, string methodName, object[] parameters)
        {
            try
            {
                object result = await Service.InvokeOnControllerByCTIDAsync(bid, myNamespace, ctid, methodName, parameters);
                
                if (result == null)
                {
                    return Ok();
                }
                else
                {
                    return Ok(result);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
