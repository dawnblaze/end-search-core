﻿using Endor.Search.Core;
using Endor.Search.Core.Models;
using Microsoft.AspNetCore.Http;

namespace Endor.Search.web.Controllers
{
    public class AlertDefinitionController : SearchableEntityController<AlertDefinitionSearchModel>
    {
        public AlertDefinitionController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class AssemblyCategoryController : SearchableEntityController<AssemblyCategorySearchModel>
    {
        public AssemblyCategoryController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class AssemblyDataController : SearchableEntityController<AssemblyDataSearchModel>
    {
        public AssemblyDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class BoardDefinitionDataController : SearchableEntityController<BoardDefinitionDataSearchModel>
    {
        public BoardDefinitionDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class CompanyDataController : SearchableEntityController<CompanyDataSearchModel>
    {
        public CompanyDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class ContactDataController : SearchableEntityController<ContactDataSearchModel>
    {
        public ContactDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class CreditMemoDataController : SearchableEntityController<CreditMemoDataSearchModel>
    {
        public CreditMemoDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class CrmIndustryController : SearchableEntityController<CrmIndustrySearchModel>
    {
        public CrmIndustryController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class CrmOriginController : SearchableEntityController<CrmOriginSearchModel>
    {
        public CrmOriginController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class CustomFieldDefinitionController : SearchableEntityController<CustomFieldDefinitionSearchModel>
    {
        public CustomFieldDefinitionController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class CustomFieldLayoutDefinitionController : SearchableEntityController<CustomFieldLayoutDefinitionSearchModel>
    {
        public CustomFieldLayoutDefinitionController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class DashboardDataController : SearchableEntityController<DashboardDataSearchModel>
    {
        public DashboardDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class DashboardWidgetDefinitionController : SearchableEntityController<DashboardWidgetDefinitionSearchModel>
    {
        public DashboardWidgetDefinitionController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class DestinationDataController : SearchableEntityController<DestinationDataSearchModel>
    {
        public DestinationDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class DomainDataController : SearchableEntityController<DomainDataSearchModel>
    {
        public DomainDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class DomainEmailController : SearchableEntityController<DomainEmailSearchModel>
    {
        public DomainEmailController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class EmailAccountDataController : SearchableEntityController<EmailAccountDataSearchModel>
    {
        public EmailAccountDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class EmployeeDataController : SearchableEntityController<EmployeeDataSearchModel>
    {
        public EmployeeDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class EmployeeTeamController : SearchableEntityController<EmployeeTeamSearchModel>
    {
        public EmployeeTeamController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class EstimateDataController : SearchableEntityController<EstimateDataSearchModel>
    {
        public EstimateDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class LaborCategoryController : SearchableEntityController<LaborCategorySearchModel>
    {
        public LaborCategoryController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class LaborDataController : SearchableEntityController<LaborDataSearchModel>
    {
        public LaborDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class LocationDataController : SearchableEntityController<LocationDataSearchModel>
    {
        public LocationDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class MachineCategoryController : SearchableEntityController<MachineCategorySearchModel>
    {
        public MachineCategoryController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class MachineDataController : SearchableEntityController<MachineDataSearchModel>
    {
        public MachineDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class MaterialCategoryController : SearchableEntityController<MaterialCategorySearchModel>
    {
        public MaterialCategoryController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class MaterialDataController : SearchableEntityController<MaterialDataSearchModel>
    {
        public MaterialDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class MessageBodyTemplateController : SearchableEntityController<MessageBodyTemplateSearchModel>
    {
        public MessageBodyTemplateController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class MessageHeaderController : SearchableEntityController<MessageHeaderSearchModel>
    {
        public MessageHeaderController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class OrderDataController : SearchableEntityController<OrderDataSearchModel>
    {
        public OrderDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class OrderItemDataController : SearchableEntityController<OrderItemDataSearchModel>
    {
        public OrderItemDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class PaymentApplicationDataController : SearchableEntityController<PaymentApplicationDataSearchModel>
    {
        public PaymentApplicationDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class PaymentMasterDataController : SearchableEntityController<PaymentMasterDataSearchModel>
    {
        public PaymentMasterDataController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class QuickItemController : SearchableEntityController<QuickItemSearchModel>
    {
        public QuickItemController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class ReconciliationController : SearchableEntityController<ReconciliationSearchModel>
    {
        public ReconciliationController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class RightsGroupListController : SearchableEntityController<RightsGroupListSearchModel>
    {
        public RightsGroupListController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class SurchageDefController : SearchableEntityController<SurchargeDefSearchModel>
    {
        public SurchageDefController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class TaxabilityCodeController : SearchableEntityController<TaxabilityCodeSearchModel>
    {
        public TaxabilityCodeController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }

    public class UserDraftController : SearchableEntityController<UserDraftSearchModel>
    {
        public UserDraftController(IHttpContextAccessor httpContext) : base(httpContext) { }
    }





}
