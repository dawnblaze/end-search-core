﻿using Endor.Search.Core;
using Endor.Search.Core.Interfaces;
using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Common;
using Endor.Search.infrastructure.Services;
using Endor.Search.web.Common;
using Endor.Search.web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Security;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Endor.Search.web.Controllers
{
    /// <summary>
    /// A class to query searchable entities without knowing the type ahead of time
    /// </summary>
    [Route("api/omni")]
    public class SearchableWildcardController : MultiTenantLuceneController
    {
        private Lazy<SearchableWildcardService> _Service => new Lazy<SearchableWildcardService>(() => new SearchableWildcardService(_cache));
        public SearchableWildcardService Service => _Service.Value;

        protected ITenantDataCache _cache;

        public SearchableWildcardController(ITenantDataCache cache)
        {
            this._cache = cache;
        }

        [HttpGet]
        // GET api/omni?search=<search>
        public async Task<IActionResult> Get([FromQuery]string search, [FromQuery]int top = EndorLuceneConstants.DefaultHits)
        {
            short bid;
            if (!ValidateBID(out bid))
                return InvalidBIDMessage();

            List<WildcardSearchModel> result = await Service.GetAsync(bid, search, top);

            return Ok(result);
        }

        [HttpGet("page")]
        public async Task<IActionResult> GetPage([FromQuery]PageQueryParameters parameters)
        {
            short bid;
            if (!ValidateBID(out bid))
                return InvalidBIDMessage();

            PagedSearchResult<WildcardSearchModel> result = await Service.GetPageAsync(bid, parameters?.skip ?? 0, parameters?.perPage, parameters?.search);
            return Ok(result);
        }
    }
}
