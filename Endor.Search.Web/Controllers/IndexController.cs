﻿using Endor.Search.Core;
using Endor.Search.Core.Interfaces;
using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Common;
using Endor.Search.infrastructure.Services;
using Endor.Search.web.Common;
using Endor.Search.web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Security;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace Endor.Search.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    [InternalFilter]
    public class IndexController : ControllerBase
    {
        protected readonly ITenantDataCache _tenantCache;
        protected readonly IMemoryCache _cache;
        /// <summary>
        /// Logger
        /// </summary>
        protected readonly RemoteLogger _logger;
        /// <summary>
        /// RTM Client
        /// </summary>
        protected readonly IRTMPushClient _rtmClient;
        /// <summary>
        /// RTM Client
        /// </summary>
        protected readonly ILuceneIndexerOptions _indexerOptions;
        protected readonly EndorOptions _endorOptions;

        protected readonly string ConnectionString;
        protected readonly short? BID;
        protected readonly IDapperReadOnlyRepository _dapperRepository;
        public IndexController(IMemoryCache cache, ITenantDataCache tenantCache, RemoteLogger logger, IRTMPushClient rtmClient, ILuceneIndexerOptions indexerOptions, IHttpContextAccessor httpCtx)
        {
            _cache = cache;
            _tenantCache = tenantCache;
            _logger = logger;
            _rtmClient = rtmClient;
            _indexerOptions = indexerOptions;
            BID = short.Parse(httpCtx.HttpContext.User?.Claims.FirstOrDefault(x => x.Type == Endor.Common.ClaimNameConstants.BID).Value);
            ConnectionString = _tenantCache.Get(BID.Value).Result.BusinessDBConnectionString;
            _dapperRepository = new DapperRepository(BID.Value, "", ConnectionString, _cache);
        }

        [HttpGet]
        [Route("{bid}/{ctid}")]
        public async Task<IActionResult> IndexMany(short bid, int ctid)
        {
            Type type;
            try
            {
                type = ClassTypeIDMap.ClassTypeForCTID(ctid);
            }
            catch (Exception)
            {
                return Ok($"No model exists for ctid:" + ctid.ToString());
            }

            MethodInfo method = this.GetType().GetMethod("GenericIndex", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(short), typeof(int), typeof(Type) }, null);
            MethodInfo generic = method.MakeGenericMethod(type);
            var error = await (Task<bool>)generic.Invoke(this, new object[] { bid, ctid, type });

            if (error)
                return StatusCode((int)HttpStatusCode.InternalServerError);
            else
                return Ok();
        }

        [HttpGet]
        [Route("{bid}/{ctid}/multiple/{csvID}")]
        public async Task<IActionResult> IndexMultipleID(short bid, int ctid, string csvID)
        {
            var ids = csvID.Split(',').Select(strID => Convert.ToInt32(strID)).ToList();
            if (ids.Count > 0)
            {
                Type type;
                try
                {
                    type = ClassTypeIDMap.ClassTypeForCTID(ctid);
                }
                catch (Exception)
                {
                    return Ok($"No model exists for ctid:" + ctid.ToString());
                }

                MethodInfo method = this.GetType().GetMethod("GenericIndex", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(short), typeof(int), typeof(List<int>) }, null);
                MethodInfo generic = method.MakeGenericMethod(type);
                var error = await (Task<bool>)generic.Invoke(this, new object[] { bid, ctid, ids });

                if (error)
                    return StatusCode((int)HttpStatusCode.InternalServerError);
                else
                    return Ok();
            }
            else
            {
                return await IndexMany(bid, ctid);
            }
        }

        [HttpGet]
        [Route("{bid}/{ctid}/{id}")]
        public async Task<IActionResult> IndexSingle(short bid, int ctid, int id)
        {
            Type type;
            try
            {
                type = ClassTypeIDMap.ClassTypeForCTID(ctid);
            } catch (Exception)
            {
                return Ok($"No model exists for ctid:"+ctid.ToString());
            }
            MethodInfo method = this.GetType().GetMethod("GenericIndex", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(short), typeof(int), typeof(List<int>) }, null);
            MethodInfo generic = method.MakeGenericMethod(type);
            var error = await (Task<bool>)generic.Invoke(this, new object[] { bid, ctid, new List<int>() { id } });

            if (error)
                return StatusCode((int)HttpStatusCode.InternalServerError);
            else
                return Ok();
        }

        private async Task<bool> GenericIndex<S>(short bid, int ctid, Type type)
            where S : IAutomappableSearchModel<S>, new()
        {
            var service = new SearchableEntityService<S>(_logger, _tenantCache, _rtmClient, ctid);
            await service.DeleteAll(bid);

            var IDs = await _dapperRepository.GetAllIDsForClassType(bid,type);            
            var error = await GenericIndex<S>(bid, ctid, IDs.ToList());
            
            return error;
        }
        private async Task<bool> GenericIndex<S>(short bid, int ctid, List<int> ids)
            where S : IAutomappableSearchModel<S>, new()
        {
            var service = new SearchableEntityService<S>(_logger, _tenantCache, _rtmClient, ctid);

            IEnumerable<S> datasetAll;
            
            datasetAll = await _dapperRepository.GetSearchModelByIdAsync<S>(ids.ToArray());

            var IDsMissing = ids.Where(p => !datasetAll.Select(s => s.ID).Any(p2 => p2 == p));
            foreach (var id in IDsMissing)
            {
                await service.DeleteSingle(bid, id);
            }
            
            await service.IndexMany(bid, datasetAll);
            return false;
        }

        private async Task<Engine<S>> GetEngine<S>(short bid)
            where S : IAutomappableSearchModel<S>, new()
        {
            return await Engine<S>.Create(bid, _tenantCache, false);
        }
    }
}
