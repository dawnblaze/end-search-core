﻿using Endor.Search.Core.Interfaces;
using Endor.Search.infrastructure.Services;
using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.web.Services
{
    public abstract class SearchableService
    {
        #region Helpers
        public async Task<Engine<S>> GetEngine<S>(short bidFromRoute, ITenantDataCache cache)
            where S : IAutomappableSearchModel<S>, new()
        {
            bool useLocalStorage = false;
            return await Engine<S>.Create(bidFromRoute, cache, useLocalStorage);
        }
        #endregion Helpers
    }
}
