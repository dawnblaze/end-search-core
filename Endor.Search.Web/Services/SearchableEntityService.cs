﻿using Endor.Search.Core;
using Endor.Search.Core.Interfaces;
using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.web.Common;
using Endor.AzureStorage;
using Endor.DocumentStorage.Models;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tenant;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Endor.Search.web.Services
{
    public abstract class SearchableEntityService : SearchableService
    {
        protected static ConcurrentDictionary<short, SemaphoreSlim> getEngineSemaphores = new ConcurrentDictionary<short, SemaphoreSlim>();

        protected static readonly object _LockObject = new object();
        protected static SemaphoreSlim GetSemaphore(short bid)
        {
            lock (_LockObject)
            {
                if (getEngineSemaphores.ContainsKey(bid))
                    return getEngineSemaphores[bid];

                SemaphoreSlim result = new SemaphoreSlim(1, 1);
                getEngineSemaphores[bid] = result;

                return result;
            }
        }
    }

    public class SearchableEntityService<S> : SearchableEntityService
        where S : IAutomappableSearchModel<S>, new()
    {
        /// <summary>
        /// Logger
        /// </summary>
        protected readonly RemoteLogger _logger;
        /// <summary>
        /// Logger
        /// </summary>
        protected readonly ITenantDataCache _cache;
        /// <summary>
        /// RTM Client
        /// </summary>
        protected readonly IRTMPushClient _rtmClient;
        protected readonly int _ctid;

        public SearchableEntityService(RemoteLogger logger, ITenantDataCache cache, IRTMPushClient rtmClient, int ctid)
        {
            this._logger = logger;
            this._cache = cache;
            this._rtmClient = rtmClient;
            this._ctid = ctid;
        }

        #region Logic

        internal async Task<PagedSearchResult<S>> GetPageAsync(short bid, int skip, int? perPage = null, string search = null, string sortBy = null)
        {
            await _logger.Information(bid, $"Search page called for {typeof(S)}.");

            search = GetEffectiveSearch(search);
            int effectivePerPage = perPage ?? EndorLuceneConstants.DefaultPerPage;

            return (await GetEngine<S>(bid, _cache)).Query(search, skip, effectivePerPage, sortBy: sortBy);
        }

        internal async Task<List<S>> GetListAsync(short bid, string search)
        {
            await _logger.Information(bid, $"Search called for {typeof(S)} with the search string of \"{search}\".");

            search = GetEffectiveSearch(search);

            return (await GetEngine<S>(bid, _cache)).Query(search, EndorLuceneConstants.DefaultHits);
        }

        internal async Task IndexSingle(short bid, S m)
        {
            await _logger.Information(bid, $"Search indexSingle called for {typeof(S)} with ID {m.ID}.");

            SemaphoreSlim semaphore = GetSemaphore(bid);
            await semaphore.WaitAsync();

            try
            {
                var engine = await GetEngine<S>(bid, _cache);


                if (typeof(S) == typeof(MessageHeaderSearchModel))
                {
                    await this.GetMessageBodyFromBlob(m as MessageHeaderSearchModel, engine.StorageConnectionString);
                }

                if (typeof(S) == typeof(UserDraftSearchModel))
                {
                    await this.GetObjectJSONFromBlob(m as UserDraftSearchModel, engine.StorageConnectionString);
                }

                engine.Write(m);

                await PostRefresh(bid, engine, m);
            }
            catch (Exception e)
            {
                await _logger.Error(bid, $"Error in indexSingle for {typeof(S)} with ID {m.ID}.", e);
                throw;
            }
            finally
            {
                semaphore.Release();
            }
        }

        internal async Task IndexMany(short bid, IEnumerable<S> ms)
        {
            await _logger.Information(bid, $"Search indexMany called for {typeof(S)}.");

            SemaphoreSlim semaphore = GetSemaphore(bid);
            await semaphore.WaitAsync();

            try
            {
                var engine = await GetEngine<S>(bid, _cache);
                int ctid = _ctid;

                // updatemany appears to create new ones if needed, and just update existing ones
                // whereas writemany appears to always create new ones, even if there are existing ones
                engine.UpdateMany(ms.ToList());

                foreach (S m in ms)
                    await PostRefresh(bid, engine, m);
            }
            catch (Exception e)
            {
                await _logger.Error(bid, $"Error in indexMany for {typeof(S)}.", e);
                throw;
            }
            finally
            {
                semaphore.Release();
            }
        }

        internal async Task DeleteAll(short bid)
        {
            await _logger.Information(bid, $"Search delete called for {typeof(S)}.");

            SemaphoreSlim semaphore = GetSemaphore(bid);
            await semaphore.WaitAsync();

            try
            {
                var engine = await GetEngine<S>(bid, _cache);
                int ctid = _ctid;

                // use empty IEnumerable<M> here to delete all documents
                engine.DeleteExistingAndWriteMany(ctid, new List<S>());
            }
            catch (Exception e)
            {
                await _logger.Error(bid, $"Error in delete all for {typeof(S)}.", e);
                throw;
            }
            finally
            {
                semaphore.Release();
            }
        }

        internal async Task Update(short bid, S model)
        {
            await _logger.Information(bid, $"Search update called for {typeof(S)} with ID {model.ID}.");

            SemaphoreSlim semaphore = GetSemaphore(bid);
            await semaphore.WaitAsync();

            try
            {
                var engine = await GetEngine<S>(bid, _cache);

                if (typeof(S) == typeof(UserDraftSearchModel))
                {
                    await this.GetObjectJSONFromBlob(model as UserDraftSearchModel, engine.StorageConnectionString);
                }

                engine.Update(model);

                await PostRefresh(bid, engine, model);
            }
            catch (Exception e)
            {
                await _logger.Error(bid, $"Error in Put for {typeof(S)} with ID {model.ID}.", e);
                throw;
            }
            finally
            {
                semaphore.Release();
            }
        }

        internal async Task DeleteSingle(short bid, int id)
        {
            await _logger.Information(bid, $"Search delete called for {typeof(S)} with ID {id}.");

            SemaphoreSlim semaphore = GetSemaphore(bid);
            await semaphore.WaitAsync();

            try
            {
                var engine = await GetEngine<S>(bid, _cache);
                int ctid = _ctid;

                engine.DeleteIfExists(ctid, id);

                await PostRefreshFromDelete(bid, id);
            }
            catch (Exception e)
            {
                await _logger.Error(bid, $"Error in deletesingle for {typeof(S)} with ID {id}.", e);
                throw;
            }
            finally
            {
                semaphore.Release();
            }
        }

#endregion Logic

#region Helpers

        private async Task GetMessageBodyFromBlob(MessageHeaderSearchModel header, string storageConnectionString)
        {
            try
            {
                var storageClient = new EntityStorageClient(storageConnectionString, header.BID);
                var msgHeaderDMID = new DMID()
                {
                    id = header.BodyID,
                    ctid = ClassTypeIDMap.MapForClassType(typeof(MessageBody)).ClassTypeID
                };

                if (await storageClient.Exists("body.html", StorageBin.Permanent, Bucket.Data, msgHeaderDMID))
                {
                    var blobStream = await storageClient.GetStream(
                            StorageBin.Permanent,
                            Bucket.Data,
                            msgHeaderDMID,
                            "body.html");
                    blobStream.Position = 0;

                    using (var sr = new StreamReader(blobStream))
                    {
                        header._messageBodyString = sr.ReadToEnd();
                    }
                }
            }
            catch (Exception e)
            {
                await _logger.Error(header.BID, $"Error in GetMessageBodyFromBlob of {typeof(S)}.", e);
                header._messageBodyString = "";
            }
        }
        private async Task GetObjectJSONFromBlob(UserDraftSearchModel draft, string storageConnectionString)
        {
            try
            {
                var storageClient = new EntityStorageClient(storageConnectionString, draft.BID);
                var draftDMID = new DMID()
                {
                    id = draft.ID,
                    ctid = ClassTypeIDMap.MapForClassType(typeof(UserDraftSearchModel)).ClassTypeID
                };

                if (await storageClient.Exists("draft.json", StorageBin.Permanent, Bucket.Data, draftDMID))
                {
                    var blobStream = await storageClient.GetStream(
                        StorageBin.Permanent,
                        Bucket.Data,
                        draftDMID,
                        "draft.json");
                    blobStream.Position = 0;

                    using (var sr = new StreamReader(blobStream))
                    {
                        draft.ObjectJSON = sr.ReadToEnd();
                    }
                }
            }
            catch (Exception e)
            {
                await _logger.Error(draft.BID, $"Error in GetObjectJSONFromBlob of {typeof(S)}.", e);
                draft.ObjectJSON = null;
            }
        }

        private async Task PostRefresh(short bid, Engine<S> engine, S model)
        {
            List<RefreshEntity> refreshEntities = new List<RefreshEntity>()
                    {
                        new RefreshEntity()
                        {
                            BID = (short)bid,
                            ClasstypeID = _ctid,
                            ID = Convert.ToInt32(model.ID),
                            DateTime = DateTime.UtcNow,
                            RefreshMessageType = RefreshMessageType.Change,
                            Data = "index",
                            HeadingHash = GenerateSimpleHeaderHash(model), // header-text" + "sub-header-text
                        },
                        new RefreshEntity()
                        {
                            BID = (short)bid,
                            ClasstypeID = model.ClassTypeID,
                            DateTime = DateTime.UtcNow,
                            RefreshMessageType = RefreshMessageType.Change,
                            Data = "index",
                        }
                    };

            await _rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshEntities });
            if (typeof(S) == typeof(MessageHeaderSearchModel))
            {
                await this.SendMessageHeaderIndexRefresh(model as MessageHeaderSearchModel);
            }

        }

        internal async Task PostRefreshFromDelete(short bid, int id)
        {
            List<RefreshEntity> refreshEntities = new List<RefreshEntity>()
                    {
                        new RefreshEntity()
                        {
                            BID = (short)bid,
                            ClasstypeID = _ctid,
                            ID = id,
                            DateTime = DateTime.UtcNow,
                            RefreshMessageType = RefreshMessageType.Delete,
                        },
                        new RefreshEntity()
                        {
                            BID = (short)bid,
                            ClasstypeID = _ctid,
                            DateTime = DateTime.UtcNow,
                            RefreshMessageType = RefreshMessageType.Delete,
                        }
                    };

            await _rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshEntities });
        }

        private string GenerateSimpleHeaderHash(S m)
        {
            string simpleHeader = (m as BaseSearchModel).Header;
            string simpleSubHeader = (m as BaseSearchModel).Subheader;

            return _toHashCode(simpleHeader + simpleSubHeader);
        }

        private async Task SendMessageHeaderIndexRefresh(MessageHeaderSearchModel msgHeader, string refreshMsg = null, string refreshData = null)
        {
            await _rtmClient.SendEmployeeMessage(new EmployeeMessage()
            {
                BID = (short)msgHeader.BID,
                EmployeeID = msgHeader.EmployeeID,
                Message = refreshMsg ?? $"index: a message({msgHeader.ID}) has been reindexed",
                Data = refreshData ?? msgHeader.ID.ToString()
            });
        }

        private static string _toHashCode(string str)
        {
            var hash = 0;
            if (str.Length > 0)
            {
                for (var i = 0; i < str.Length; i++)
                {
                    var charcode = (int)str[i];
                    hash = ((hash << 5) - hash) + charcode;
                    hash = hash & hash; // Convert to 32bit integer
                }
            }

            return hash.ToString();
        }

        private string CTIDQueryClause
        {
            get
            {
                return $"ClassTypeID:{ _ctid.ToString() }";
            }
        }

        private string GetEffectiveSearch(string search)
        {
            if (!String.IsNullOrWhiteSpace(search))
                //remove spaces before a *
                search = new System.Text.RegularExpressions.Regex("\\s+\\*").Replace(search, "\\*");

            if (String.IsNullOrWhiteSpace(search))
                search = CTIDQueryClause;
            else
            {
                search = $"{CTIDQueryClause} AND {search}";
            }
            return search;
        }

#endregion Helpers
    }
}
