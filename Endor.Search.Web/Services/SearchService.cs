﻿using Endor.Search.Core;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace Endor.Search.web.Services
{
    public class SearchService : SearchableService
    {
        private static Dictionary<string, MethodInfo> MethodCache = new Dictionary<string, MethodInfo>();
        private static ConcurrentDictionary<int, Type> ControllerTypeCache = new ConcurrentDictionary<int, Type>();

        /// <summary>
        /// Logger
        /// </summary>
        protected readonly RemoteLogger _logger;
        /// <summary>
        /// Logger
        /// </summary>
        protected readonly ITenantDataCache _cache;
        /// <summary>
        /// HTTPContext
        /// </summary>
        protected IHttpContextAccessor _httpCtx;

        public SearchService(RemoteLogger logger, ITenantDataCache cache, IHttpContextAccessor httpCtx)
        {
            this._logger = logger;
            this._cache = cache;
            this._httpCtx = httpCtx;
        }
        
        #region logic
        internal async Task<object> InvokeOnControllerByCTIDAsync(short bid, string _namespace, int ctid, string methodName, object[] parameters)
        {
            try
            {
                Type controllerType = GetControllerType(ctid, _namespace);
                if (controllerType != null)
                {
                    var controller = Activator.CreateInstance(controllerType, _httpCtx);
                    var method = GetMethod(controllerType, methodName);
                    object result = await (Task<object>)method.Invoke(controller, parameters);

                    return result;
                }
                else
                    throw new Exception($"Could not find controller type for CTID {ctid}");
            }
            catch (Exception e)
            {
                await _logger.Error(bid, $"Error in InvokeOnControllerByCTIDAsync for ctid of {ctid} and method name of \"{methodName}\"", e);
                throw new Exception(e.Message);
            }
        }
        #endregion logic

        #region helpers
        private static Type GetControllerType(int ctid, string _namespace)
        {
            if (ControllerTypeCache.ContainsKey(ctid))
                return ControllerTypeCache[ctid];

            Type entityType = ClassTypeIDMap.ClassTypeForCTID(ctid);

            if (entityType != null)
            {
                Type controllerType = Type.GetType($"{_namespace}.{entityType.Name.Replace("SearchModel","")}Controller", false, true);

                if (controllerType != null)
                {
                    ControllerTypeCache.TryAdd(ctid,controllerType);
                    return controllerType;
                }
                else
                {
                    throw new ArgumentException($"Class {entityType.FullName} has no corresponding Controller");
                }
            }
            else
            {
                throw new ArgumentException($"CTID {ctid} has no corresponding Model");
            }
        }

        private static MethodInfo GetMethod(Type controllerType, string methodName)
        {
            string cacheKey = $"{controllerType.FullName}|{methodName}";
            if (MethodCache.ContainsKey(cacheKey))
                return MethodCache[cacheKey];
            else
            {
                var method = controllerType.GetMethod(methodName, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public);
                MethodCache[cacheKey] = method;
                return method;
            }
        }
        #endregion helpers
    }
}
