﻿using Endor.Search.Core.Models;
using Endor.Search.infrastructure.Services;
using Endor.Search.web.Common;
using Endor.Tenant;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Endor.Search.web.Services
{
    public class SearchableWildcardService : SearchableService
    {
        protected ITenantDataCache _cache;

        public SearchableWildcardService(ITenantDataCache cache)
        {
            this._cache = cache;
        }
            #region Logic
        internal async Task<List<WildcardSearchModel>> GetAsync(short bid, string search, int top = EndorLuceneConstants.DefaultHits)
        {
            return (await GetEngine<WildcardSearchModel>(bid, _cache)).Query(search, top);
        }

        internal async Task<PagedSearchResult<WildcardSearchModel>> GetPageAsync(short bid, int skip, int? perPage, string search)
        {
            if (!perPage.HasValue)
                perPage = EndorLuceneConstants.DefaultPerPage;

            return (await GetEngine<WildcardSearchModel>(bid, _cache)).Query(search, skip, perPage.Value);
        }
        #endregion Logic
    }
}
