﻿using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.web.Common
{
    /// <summary>
    /// Endor Options
    /// </summary>
    public class EndorOptions : IEnvironmentOptions
    {
        /// <summary>
        /// Origin of the Auth
        /// </summary>
        public string AuthOrigin { get; set; }
        /// <summary>
        /// Tenant Secret
        /// </summary>
        public string TenantSecret { get; set; }
        /// <summary>
        /// Messaging Server URL
        /// </summary>
        public string MessagingServerURL { get; set; }
        /// <summary>
        /// Storage Connection String
        /// </summary>
        public string StorageConnectionString { get; set; }

    }
}
