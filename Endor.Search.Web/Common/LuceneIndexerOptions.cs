﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.web.Common
{
    public interface ILuceneIndexerOptions
    {
        int EntitiesPerBatch { get; set; }
        int SerializedEntityCount { get; set; }
    }
    public class LuceneIndexerOptions : ILuceneIndexerOptions
    {
        public int EntitiesPerBatch { get; set; }
        public int SerializedEntityCount { get; set; }
    }
}
