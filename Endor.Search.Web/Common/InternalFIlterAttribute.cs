﻿using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Endor.Search.web.Common
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class InternalFilterAttribute : AuthorizeAttribute, IAuthorizationFilter 
    {
        public InternalFilterAttribute()
        {
        }
        /// <summary>
        /// This method authenticates the request being sent with the following conditions
        /// An Authorization header with Internal as the auth scheme is checked to see if the first segment (left of the first colon)
        /// of the authorization parameter matches the TenantSecretKey.
        /// If it is a match the BID and AID are extracted from the 2nd and 3rd segments of the authorization parameter
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            HttpRequest request = context.HttpContext.Request;
            StringValues stringValues;

            //If user is authenticated - end
            if (context.HttpContext.User.Identities.Any(x => x.IsAuthenticated)) return;

            if (!request.Headers.TryGetValue("Authorization",out stringValues))
            {
                context.Result = new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                return;
            }

            // 4. If there are credentials that the filter understands, try to validate them.
            //    If the credentials are bad, set the error result.
            // 5. If the credentials are good, try to generate a Principal.
            //    If the principal is null, set the error result.
            var serviceProvider = context.HttpContext.RequestServices;
            var endorOptions = (IEnvironmentOptions)serviceProvider.GetService(typeof(IEnvironmentOptions));
            var authHeader = request.Headers["Authorization"].ToString();
            if (!authHeader.StartsWith("internal", StringComparison.OrdinalIgnoreCase))
            {
                context.Result = new ForbidResult("Internal credentials are incorrect");
                return;
            }
            var token = authHeader.Substring("Internal ".Length).Trim();

            var identity = CreateIdentity(token, endorOptions.TenantSecret);
            if (identity == null)
            {
                context.Result = new ForbidResult("Internal credentials are incorrect");
                return;
            }

            // 6. If the credentials are valid, set principal.
            else
            {
                context.HttpContext.User.AddIdentity(identity);
            }

            return;
        }

        private ClaimsIdentity CreateIdentity(string parameter, string SecretKey)
        {
            string[] values = parameter.Split(':');
            string tenantKey = values.Length > 0 ? values[0] : null;

            if (String.IsNullOrWhiteSpace(tenantKey) || tenantKey != SecretKey)
                return null;

            ClaimsIdentity identity = new ClaimsIdentity("Internal") { };

            if (values.Length > 1)
            {
                short.TryParse(values[1], out short myBID);
                identity.AddClaim(new Claim(Endor.Common.ClaimNameConstants.BID, myBID.ToString(), ClaimValueTypes.Integer));
            }

            if (values.Length > 2)
            {
                byte.TryParse(values[2], out byte myAID);
                identity.AddClaim(new Claim(Endor.Common.ClaimNameConstants.AID, myAID.ToString(), ClaimValueTypes.Integer));
            }

            return identity;
        }
    }

}
