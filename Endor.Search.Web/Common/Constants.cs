﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.web.Common
{
    public static class EndorLuceneConstants
    {
        public const int DefaultHits = 100;
        public const int DefaultPerPage = 20;
    }
}
