﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Search.web.Common
{
    public class PageQueryParameters
    {
        public int? skip { get; set; }
        public int? perPage { get; set; }
        public string search { get; set; }
        public string sortBy { get; set; } = null;
    }
}
