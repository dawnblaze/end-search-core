using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Endor.Search.infrastructure.Services;
using Endor.Search.web.Controllers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Endor.Search.web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "Endor.Search";
            IConfigurationBuilder hostingBuilder = new ConfigurationBuilder()
                .AddEnvironmentVariables();

            var host = new WebHostBuilder();
            var environment = host.GetSetting("environment");
            hostingBuilder.SetBasePath(Directory.GetCurrentDirectory());
            hostingBuilder.AddJsonFile("appsettings.json");
            hostingBuilder.AddJsonFile($"appsettings.{environment}.json", optional: true);

            if (environment == "Development")
            {
                hostingBuilder.AddUserSecrets<Startup>();
            }

            var envConfiguration = hostingBuilder.Build();

            ValidateLuceneControllerNamesAgainstModelNames();

            host.UseKestrel(options =>
            {
                if (environment == "Development")
                {
                    AddHttpsWithCert(options, envConfiguration);
                }
            })
                .UseIISIntegration()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                .Build()
                .Run();

            
        }

        private static void ValidateLuceneControllerNamesAgainstModelNames()
        {
            try
            {
                Assembly asm = Assembly.GetExecutingAssembly();

                asm.GetTypes()
                    .Where(type => !type.IsGenericType && type.IsSubclassOfRawGeneric(typeof(SearchableEntityController<>))) //filter controllers
                    .OrderBy(ct => ct.Name).ToList().ForEach((controllerType) =>
                    {
                        Type modelType = controllerType.BaseType.GetGenericArguments().FirstOrDefault();

                        if (controllerType.Name.ToLowerInvariant() != $"{modelType.Name.Replace("SearchModel","")}controller".ToLowerInvariant())
                        {
                            SearchController.MisnamedSearchableEntityControllers.Add($"A SearchableEntityController {controllerType.Name} is not named after the model that it uses! Please name it {modelType.Name}Controller, where type is an ISearchable. The BGEngineClient uses the model names to target each endpoint.");
                        }
                        else
                        {
                            SearchController.AvailableSearchEntityRoutes.Add(modelType.Name);
                        }
                    });

            }
            catch (Exception)
            {
                //noop, debug type thing
            }
        }

        private static void AddHttpsWithCert(KestrelServerOptions options, IConfigurationRoot envConfiguration)
        {
            Uri hostUri = new Uri(envConfiguration["ASPNETCORE_URLS"]);
            if (Environment.OSVersion.ToString().Contains("Unix"))
            {
                string wildCardCertPath = "/usr/local/share/ca-certificates/wildcard.localcyriousdevelopment.com.v3.GOES_IN_PERSONAL.pfx";
                string wildCardCertPassword = envConfiguration["wildCardCertPassword"];
                options.Listen(IPAddress.Any, hostUri.Port, (a => a.UseHttps(wildCardCertPath, wildCardCertPassword)));
            }
            else
            {
                X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection certs = store.Certificates.Find(X509FindType.FindByThumbprint, envConfiguration["Endor:certThumbprint"], true);
                if (certs != null && certs.Count > 0)
                {
                    X509Certificate2 cert = certs[0];

                    options.Listen(IPAddress.Any, hostUri.Port, (a => a.UseHttps(cert))); //options.UseHttps(cert);
                }
            }
        }
    }
}
