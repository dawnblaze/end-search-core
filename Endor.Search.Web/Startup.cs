using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tenant;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Endor.Search.web.Common;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using System.Reflection;

namespace Endor.Search.web
{
    public abstract class BaseStartup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public BaseStartup(IWebHostEnvironment env)
        {
            IConfigurationBuilder builder = GetConfigurationBuilder(env);

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            Configuration = builder.Build();
        }

        /// <summary>
        /// Returns a configuration builder
        /// </summary>
        /// <param name="env"></param>
        /// <returns></returns>
        public virtual IConfigurationBuilder GetConfigurationBuilder(IWebHostEnvironment env)
        {
            return new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
        }

        public IConfigurationRoot Configuration { get; }

        /// <summary>
        /// This method configures the authentication service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public abstract void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration);


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddConfiguration(Configuration.GetSection("Logging"));
                loggingBuilder.AddConsole();
                loggingBuilder.AddDebug();
            }); 
            
            services.Configure<EndorOptions>(Configuration.GetSection("Endor"));
            services.AddHttpContextAccessor();
            services.AddMemoryCache();
            EndorOptions endorOptions = Configuration.GetSection("Endor").Get<EndorOptions>();
            services.AddSingleton<Endor.Tenant.IEnvironmentOptions>(endorOptions);
            services.AddScoped<InternalFilterAttribute>();
            ConfigureExternalEndorServices(services);
            this.AddAuthentication(services, Configuration);
            services.AddCors();
            services.AddMvc()
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
                //.AddApplicationPart(typeof(LogLevelController).GetTypeInfo().Assembly);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Endor Search",
                    Version = "v1"
                });
#pragma warning disable CS0618
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteActions();
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                c.OperationFilter<SwaggerSecurityRightsDocumentFilter>();
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                string commentsFileName = Assembly.GetExecutingAssembly().GetName().Name + ".XML";
                string commentsFile = System.IO.Path.Combine(baseDirectory, commentsFileName);

                if (System.IO.File.Exists(commentsFile))
                {
                    c.IncludeXmlComments(commentsFile);
                }
                //c.SchemaFilter<SwaggerAddMissingEnums>();
            });
            
            services.AddControllers();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime)
        {
            RemoteLoggingExtensions.ConfigureSystemRemoteLogging(Configuration, loggerFactory, appLifetime); 
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseSwagger(o =>
            {
                o.PreSerializeFilters.Add((swagger, httpReq) =>
                {
                    swagger.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}" } };
                });
            })
            .UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Endor Search v1");
                c.DocExpansion(DocExpansion.None);
                c.RoutePrefix = "swagger";
            })
            .UseCors(t => t.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod())
            .UseRouting()
            .UseAuthorization()
            .UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Configures External Endor Services
        /// </summary>
        /// <param name="services"></param>
        public virtual void ConfigureExternalEndorServices(IServiceCollection services)
        {
            services.AddSingleton<ITenantDataCache, NetworkTenantDataCache>();
            services.AddSingleton<RemoteLogger>();
            services.AddTransient<IRTMPushClient>((x) => new RealtimeMessagingPushClient(Configuration["Endor:MessagingServerURL"]));
            ILuceneIndexerOptions luceneOptions = Configuration.GetSection("LuceneIndexer").Get<LuceneIndexerOptions>();
            services.AddSingleton<ILuceneIndexerOptions>(luceneOptions ?? new LuceneIndexerOptions() { SerializedEntityCount = 3, EntitiesPerBatch = 9 });

        }
    }

    /// <summary>
    /// Startup Class
    /// </summary>
    public class Startup : BaseStartup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public Startup(IWebHostEnvironment env) : base(env)
        {
        }

        /// <summary>
        /// This method configures the authentication service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public override void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(option =>
            {
                option.Audience = Configuration["Auth:ValidAudience"];
                option.TokenValidationParameters = new TokenValidationParameters()
                {
                    NameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier",
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["Auth:ValidIssuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(WebEncoders.Base64UrlDecode(Configuration["Auth:SymmetricKey"])),
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMilliseconds(2)
                };
            });
            services.AddAuthorization();
        }
    }
}
