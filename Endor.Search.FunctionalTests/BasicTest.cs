﻿using Endor.Search.Core;
using Endor.Search.Core.Models;
using Endor.Search.FunctionalTests.Common;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Search.FunctionalTests
{
    [TestClass]
    public class BasicTest : CommonControllerTestClass
    {
        [TestMethod]
        public async Task TestEmployeeDataThroughAPI()
        {
            var IDToUse = -100;
            var BID = 1;
            var repo = ((TestDapperRepository)GetDapperRepository());

            //Arrange
            var locationID = GetValidLocationID(repo);

            object defaultParam = new
            {
                BID = BID,
                ID = IDToUse,
            };

            await DeleteSqlExec(repo, ChildObjectTableInfo.UserLinkTableName, "EmployeeID", defaultParam);
            await DeleteSqlExec(repo, ChildObjectTableInfo.EmployeeDataTableName, ChildObjectTableInfo.IDColumn, defaultParam);


            string insertQuery = @"INSERT INTO [dbo].[Employee.Data]([BID], [ID], [First], [Last]) VALUES (@BID, @ID, @First, @Last)";

            var expected = new
            {
                BID = BID,
                ID = IDToUse,
                First = "foos",
                Last = "barino",
                LocationID = locationID
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.EmployeeDataTableName, expected);

            var expected2 = new
            {
                BID = BID,
                AuthUserID = IDToUse,
                EmployeeID = IDToUse,
                DisplayName = "Test",
                UserAccessType = 49,
                UserName = "TestMe"
            };
            await InsertSqlExec(repo, ChildObjectTableInfo.UserLinkTableName, expected2);

            //Act
            await AssertGetStatusCode(client, $"{apiUrl}/index/{expected.BID}/{ClassTypeIDMap.CTIDForClassType(typeof(EmployeeDataSearchModel))}/{expected.ID}", HttpStatusCode.OK);
            var allResults = await AssertGetStatusCode<List<EmployeeDataSearchModel>>(client, $"{apiUrl}/employeedata?search=", HttpStatusCode.OK);
            var singleResults = await AssertGetStatusCode<List<EmployeeDataSearchModel>>(client, $"{apiUrl}/employeedata?search=BID:1", HttpStatusCode.OK);
            var singleFilterResults = await AssertGetStatusCode<List<EmployeeDataSearchModel>>(client, $"{apiUrl}/employeedata?search=BID:1 AND foos", HttpStatusCode.OK);
            var NoResults = await AssertGetStatusCode<List<EmployeeDataSearchModel>>(client, $"{apiUrl}/employeedata?search=BID:0 AND foos", HttpStatusCode.OK);
            var NoFilterResults = await AssertGetStatusCode<List<EmployeeDataSearchModel>>(client, $"{apiUrl}/employeedata?search=BID:1 AND NotFound", HttpStatusCode.OK);

            //Assert
            Assert.IsTrue(allResults.Count >= 1);

            Assert.IsTrue(singleResults.Count >= 1);

            Assert.IsTrue(singleFilterResults.Count == 1);

            var actual = singleFilterResults.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.BID);
            Assert.AreEqual(expected.ID, actual.ID);
            Assert.AreEqual(expected2.UserName, actual.UserName);

            Assert.IsTrue(NoResults.Count == 0);

            Assert.IsTrue(NoFilterResults.Count == 0);
        }
    }
}
