﻿using Endor.Search.Core;
using Endor.Search.Core.Models;
using Endor.Search.FunctionalTests.Common;
using Endor.Search.IntegrationTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Search.FunctionalTests
{
    [TestClass]
    public class IndexTests : CommonControllerTestClass
    {
        /// <summary>
        /// Test each classtype, 1k to 9.9k
        /// </summary>
        /// <param name="classType"></param>
        /// <returns></returns>
        [Ignore]
        [DataTestMethod]
        [DataRow(1005)] 
        [DataRow(1021)] 
        [DataRow(1022)] 
        [DataRow(1023)] 
        [DataRow(1050)] 
        [DataRow(1100)] 
        [DataRow(2000)]
        [DataRow(2011)]  
        [DataRow(3000)] 
        [DataRow(5000)] 
        [DataRow(5020)] 
        [DataRow(8005)] 
        [DataRow(8010)]
        [DataRow(8011)] 
        [DataRow(8015)] 
        public async Task CTID1kIndex(int classType)
        {
            var BID = 1;

            await AssertGetStatusCode(client, $"{apiUrl}/index/{BID}/{classType}", HttpStatusCode.OK);
        }

        /// <summary>
        /// test order and estimate classtypes
        /// </summary>
        /// <param name="classType"></param>
        /// <returns></returns>
        [Ignore]
        [DataTestMethod]
        [DataRow(10000)]
        [DataRow(10200)]
        [DataRow(10300)]
        public async Task CTIDOrderIndex(int classType)
        {
            var BID = 1;

            await AssertGetStatusCode(client, $"{apiUrl}/index/{BID}/{classType}", HttpStatusCode.OK);
        }

        /// <summary>
        /// test 10k-11.9k classtypes
        /// </summary>
        /// <param name="classType"></param>
        /// <returns></returns>
        [Ignore]
        [DataTestMethod]
        [DataRow(10020)]
        [DataRow(10040)]
        public async Task CTID10kIndex(int classType)
        {
            var BID = 1;

            await AssertGetStatusCode(client, $"{apiUrl}/index/{BID}/{classType}", HttpStatusCode.OK);
        }

        /// <summary>
        /// test 12k-13.9k classtypes
        /// </summary>
        /// <param name="classType"></param>
        /// <returns></returns>
        [Ignore]
        [DataTestMethod]
        [DataRow(12000)]
        [DataRow(12002)]
        [DataRow(12020)]
        [DataRow(12022)]
        [DataRow(12030)]
        [DataRow(12032)]
        [DataRow(12040)]
        [DataRow(12042)]
        [DataRow(12060)]
        [DataRow(12070)]
        public async Task CTID12kIndex(int classType)
        {
            var BID = 1;

            await AssertGetStatusCode(client, $"{apiUrl}/index/{BID}/{classType}", HttpStatusCode.OK);
        }

        /// <summary>
        /// test 14k+ classtypes
        /// </summary>
        /// <param name="classType"></param>
        /// <returns></returns>
        [Ignore]
        [DataTestMethod]
        [DataRow(14000)]
        [DataRow(14100)]
        [DataRow(14111)]
        [DataRow(14220)]
        [DataRow(14500)]
        [DataRow(14503)]
        [DataRow(15025)]
        [DataRow(15027)]
        public async Task CTID14kIndex(int classType)
        {
            var BID = 1;

            await AssertGetStatusCode(client, $"{apiUrl}/index/{BID}/{classType}", HttpStatusCode.OK);
        }

        /// <summary>
        /// test 14k+ classtypes
        /// </summary>
        /// <param name="classType"></param>
        /// <returns></returns>
        [DataTestMethod]
        [DataRow(11105)]
        [DataRow(10016)]
        [DataRow(10013)]
        public async Task CTIDMissingIndex(int classType)
        {
            var BID = 1;

            await AssertGetStatusCode(client, $"{apiUrl}/index/{BID}/{classType}", HttpStatusCode.OK);
        }
    }
}
