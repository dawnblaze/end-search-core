﻿using Acheve.AspNetCore.TestHost.Security;
using Acheve.TestHost;
using Endor.Search.IntegrationTests.Common;
using Endor.Search.web;
using Endor.Search.web.Common;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tenant;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.FunctionalTests.Common
{
    public class TestStartup : BaseStartup
    {
        public TestStartup(IWebHostEnvironment env) : base(env)
        {
        }

        public override IConfigurationBuilder GetConfigurationBuilder(IWebHostEnvironment env)
        {
            var configBuilder = base.GetConfigurationBuilder(env);
            configBuilder.AddInMemoryCollection(MockTenantDataCache.GetLocalSettings());
            return configBuilder;
        }

        public override void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = TestServerDefaults.AuthenticationScheme;
            })
            .AddTestServer();
        }

        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime)
        {
            app.UseAuthentication();
            base.Configure(app, env, loggerFactory, appLifetime);
        }

        public override void ConfigureExternalEndorServices(IServiceCollection services)
        {
            var tdc = GetMockTenantDataCache();
            services.AddSingleton<ITenantDataCache>(tdc);
            services.AddSingleton<RemoteLogger>((x) => new RemoteLogger(tdc)); //new RemoteLogger(tdc));
            services.AddTransient<IRTMPushClient>((x) => new MockRealTimeMessagingPushClient());
            ILuceneIndexerOptions luceneOptions = Configuration.GetSection("LuceneIndexer").Get<LuceneIndexerOptions>();
            services.AddSingleton<ILuceneIndexerOptions>(luceneOptions ?? new LuceneIndexerOptions() { SerializedEntityCount = 3, EntitiesPerBatch = 9 });
        }

        private MockTenantDataCache GetMockTenantDataCache()
        {
            var tdc = new MockTenantDataCache();
            return tdc;
        }
    }
}
