﻿using Endor.Search.Core.Models;
using Endor.Search.Core.SharedKernal;
using System;
using System.Linq.Expressions;

namespace Endor.Search.FunctionalTests.Common
{
    public class GetFirstValidAuthUserIDSpecification : BaseSpecification<UserLink>
    {
        public GetFirstValidAuthUserIDSpecification(short bid)
            : base()
        {
            AddCriteria(new GetFirstValidAuthUserIDCriteria(bid));
            ApplyPaging(0, 1);
        }
    }

    public class GetFirstValidAuthUserIDCriteria : Criteria<UserLink>
    {
        private readonly short _bid;
        public GetFirstValidAuthUserIDCriteria(short bid)
        {
            _bid = bid;
        }
        public override Expression<Func<UserLink, bool>> ToExpression()
        {
            return ul => ul.BID == _bid && ul.EmployeeID != null && ul.AuthUserID != null;
        }
    }
}
