﻿using Endor.RTM;
using Endor.RTM.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Search.FunctionalTests.Common
{
    public class MockRealTimeMessagingPushClient : IRTMPushClient
    {
        public MockRealTimeMessagingPushClient()
        {
        }

        public Task SendRefreshMessage(RefreshMessage refreshMsg, string exclude)
        {
            return Task.CompletedTask;
        }

        public Task SendSystemMessage(SystemMessage systemMsg)
        {
            return Task.CompletedTask;
        }

        public Task SendUserMessage(UserMessage userMsg)
        {
            return Task.CompletedTask;
        }

        public Task SendEmployeeMessage(EmployeeMessage employeeMessage)
        {
            return Task.CompletedTask;
        }

        public Task SendReportTemplateRefreshMessage(ReportTemplateRefreshMessage refreshMsg)
        {
            return Task.CompletedTask;
        }
    }
}
