﻿using Endor.Search.Core;
using Endor.Search.Core.Models;
using Endor.Search.Core.SharedKernal;
using Endor.Search.IntegrationTests.Common;
using Endor.Common;
using Endor.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static Endor.Search.IntegrationTests.Common.UnitTestHelpers;

namespace Endor.Search.FunctionalTests.Common
{
    public class CommonControllerTestClass : BaseTestFixture
    {
        public const string apiUrl = "/api";
        private static Task completedTask = Task.FromResult(false);

        private HttpContextAccessor CreateDefaultHttpContext()
        {
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                 new Claim(ClaimNameConstants.UserID, UnitTestHelpers.AuthUserID.ToString()),
                 new Claim(ClaimNameConstants.BID, UnitTestHelpers.BID.ToString()),
                 new Claim(ClaimNameConstants.AID, "1")
                 //new Claim(ClaimNameConstants.UserLinkID, UserLinkID.ToString()),
                 //new Claim(ClaimNameConstants.EmployeeID, TestConstants.AuthEmployeeID.ToString())
            }));
            var httpContext = new DefaultHttpContext() { User = user };
            var httpContextAccessor = new HttpContextAccessor();
            httpContextAccessor.HttpContext = httpContext;
            return httpContextAccessor;
        }
        protected const string SQLConfigName = "SQL";

        protected IConfigurationRoot _config;

        protected System.Net.Http.HttpClient client;

        [TestInitialize]
        public virtual void Init()
        {
            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();
            _cache = serviceProvider.GetService<IMemoryCache>(); 
            var dapperRepo = GetDapperRepository(_cache);
            dapperRepo.SetTableName(ClassTypeIDMap.MapForClassType(typeof(UserLink)).TableName);
            var authUserID = (dapperRepo.List<UserLink>(new GetFirstValidAuthUserIDSpecification(BID))).First().AuthUserID;
            dapperRepo.SetTableName(ClassTypeIDMap.MapForClassType(typeof(EmployeeDataSearchModel)).TableName);
            var employeeID = (dapperRepo.List<EmployeeData>(new GetValidIDByBIDSpecification<EmployeeData>(BID))).First().ID;

            client = GetHttpClient((short)authUserID, (short)employeeID);

        }

        /// <summary>
        /// Gets an HttpClient for use with the other EndpointTests methods
        /// </summary>
        /// <param name="AuthUserID"></param>
        /// <param name="rights">Array of SecurityRights that the User will have (optional)</param>
        /// <returns></returns>
        public static HttpClient GetHttpClient(short AuthUserID, short employeeID, SecurityRight[] rights = null)
        {
            string contentRoot = Environment.CurrentDirectory;
            contentRoot = contentRoot.Substring(0, contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\') - 1) - 1) - 1)) + "\\Endor.Search.Web\\";
            var builder = new WebHostBuilder()
              .UseContentRoot(contentRoot)
              .UseEnvironment("Development")
              .UseStartup<TestStartup>();

            var testServer = new TestServer(builder);

            rights = rights ?? new SecurityRight[]
            {
            };

            byte[] bytes = new byte[128];
            for (int i = 0; i < 128; i++)
            {
                bytes[i] = 0x0;
            }

            foreach (var right in rights)
            {
                int bytePos = (int)right / 8;
                int bitPos = (int)right % 8;
                byte byteAtBytePos = bytes[bytePos];
                int resultByte = byteAtBytePos | ((byte)(1 << bitPos));

                bytes[bytePos] = Convert.ToByte(resultByte);
            }


            var client = testServer.CreateClient().WithDefaultIdentity(new[] {
                new Claim(ClaimNameConstants.BID,        BID.ToString()),
                new Claim(ClaimNameConstants.AID,        AID.ToString()),
                new Claim(ClaimNameConstants.UserID,     AuthUserID.ToString()),
                new Claim(ClaimNameConstants.UserName,   AuthUserName.ToString()),
                new Claim(ClaimNameConstants.EmployeeID, employeeID.ToString()),
                new Claim(ClaimNameConstants.UserLinkID, AuthUserID.ToString()),
                new Claim(ClaimNameConstants.AccessType, AccessType.ToString()),
                new Claim(ClaimNameConstants.Rights, Convert.ToBase64String(bytes))
            });
            client.Timeout = TimeSpan.FromMinutes(30);
            return client;

        }

        /// <summary>
        /// Tests against POST response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> AssertPostStatusCode(HttpClient client, string url, string content, HttpStatusCode statusCode)
        {
            StringContent strContent = null;

            if (!string.IsNullOrWhiteSpace(content))
                strContent = new StringContent(content, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(url, strContent);
            var strResponseTask = response.Content.ReadAsStringAsync();
            strResponseTask.Wait();
            string strResponse = strResponseTask.Result;

            Assert.AreEqual(statusCode, response.StatusCode, $"POST {url} did not match status code {statusCode}. {strResponse}");
            return response;
        }

        /// <summary>
        /// Tests against POST response's Status Code and returns an object
        /// </summary>
        /// <typeparam name="T">Class to test against</typeparam>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Object to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<T> AssertPostStatusCode<T>(HttpClient client, string url, object content, HttpStatusCode statusCode)
        {
            return await AssertPostStatusCode<T>(client, url, JsonConvert.SerializeObject(content), statusCode);
        }

        /// <summary>
        /// Tests against POST response's Status Code and returns an object
        /// </summary>
        /// <typeparam name="T">Class to test against</typeparam>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<T> AssertPostStatusCode<T>(HttpClient client, string url, string content, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await AssertPostStatusCode(client, url, content, statusCode);

            string responseString = await response.Content.ReadAsStringAsync();

            // skip deserializing response if we expect a bad request
            if (string.IsNullOrWhiteSpace(responseString) && statusCode == HttpStatusCode.BadRequest)
            {
                return default(T);
            }

            Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString), $"POST {url} content was null");

            var result = JsonConvert.DeserializeObject<T>(responseString);
            Assert.IsNotNull(result, $"POST {url} content did not deserialize into {typeof(T).Name}");

            return result;
        }

        /// <summary>
        /// Tests against GET response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task AssertGetStatusCode(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.AreEqual(statusCode, response.StatusCode, $"GET {url} did not match status code {statusCode}");
        }

        /// <summary>
        /// Tests against GET response's Status Code and returns an object
        /// </summary>
        /// <typeparam name="T">Class to test against</typeparam>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<T> AssertGetStatusCode<T>(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.AreEqual(statusCode, response.StatusCode, $"GET {url} did not match expected status code");

            // skip checking when bad request is expected
            if (statusCode.Equals(HttpStatusCode.NotFound) || statusCode.Equals(HttpStatusCode.BadRequest))
            {
                return default(T);
            }

            string responseString = await response.Content.ReadAsStringAsync();

            Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString), $"GET {url} had null content");

            var result = JsonConvert.DeserializeObject<T>(responseString);
            Assert.IsNotNull(result, $"GET {url} content did deserialize into a {typeof(T).Name}");

            return result;
        }

        /// <summary>
        /// Tests against DELETE response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> AssertDeleteStatusCode(HttpClient client, string url, params HttpStatusCode[] statusCodes)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync(url);
                //Assert.AreEqual(statusCode, response.StatusCode);
                AssertAtLeastOneMatch(response, statusCodes);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void AssertAtLeastOneMatch(HttpResponseMessage response, HttpStatusCode[] statusCodes)
        {
            Assert.IsNotNull(response);
            if (statusCodes == null)
                throw new ArgumentNullException("statusCodes");
            else if (statusCodes.Length == 0)
                throw new InvalidOperationException("Must pass at least one expected status code");
            else if (statusCodes.Length == 1)
                Assert.AreEqual(statusCodes[0], response.StatusCode);
            else
            {
                if (!statusCodes.Any(x => response.StatusCode == x))
                {
                    string expected = String.Join(" or ", statusCodes);
                    Assert.Fail($"Expected {expected}, instead got {response.StatusCode}");
                }
            }
        }

        /// <summary>
        /// Tests against PUT response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> AssertPutStatusCode(HttpClient client, string url, string content, HttpStatusCode statusCode, HttpStatusCode? statusCodeAlternative = null)
        {
            StringContent strContent = null;

            if (!string.IsNullOrWhiteSpace(content))
                strContent = new StringContent(content, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PutAsync(url, strContent);
            Assert.IsTrue(response.StatusCode == statusCode || (statusCodeAlternative.HasValue && response.StatusCode == statusCodeAlternative.Value),
                $"PUT {url} expected {statusCode}{(statusCodeAlternative == null ? "" : " or " + statusCodeAlternative)} instead, got {response.StatusCode}");

            return response;
        }

        public static async Task<T> AssertPutStatusCode<T>(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.PutAsync(url, null);
            T result = await HandleResponseMessage<T>(response);
            return result;
        }

        /// <summary>
        /// Tests against PUT response's Status Code and returns an object
        /// </summary>
        /// <typeparam name="T">Class to test against</typeparam>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<T> AssertPutStatusCode<T>(HttpClient client, string url, string content, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await AssertPutStatusCode(client, url, content, statusCode);
            T result = await HandleResponseMessage<T>(response);

            return result;
        }

        /// <summary>
        /// put object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        /// <param name="url"></param>
        /// <param name="content"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public static async Task<T> AssertPutStatusCode<T>(HttpClient client, string url, object content, HttpStatusCode statusCode)
        {
            return await AssertPutStatusCode<T>(client, url, JsonConvert.SerializeObject(content), statusCode);
        }

        /// <summary>
        /// Deserializes response from endpoint into type of T. Optionally allows for NoContentResponse e.g. from DELETE endpoints
        /// </summary>
        /// <typeparam name="T">Type of object to be used for deserialization</typeparam>
        /// <param name="response">HttpResponseMessage received from API</param>
        /// <param name="allowNoContentResponse">Set this to 'true' when expecting NoContentResponse or empty response body. Defaults to false</param>
        /// <returns></returns>
        private static async Task<T> HandleResponseMessage<T>(HttpResponseMessage response, bool allowNoContentResponse = false)
        {
            T result;
            string verb = response.RequestMessage.Method.Method;
            string responseString = await response.Content.ReadAsStringAsync();

            //Special handling of Delete endpoints that return NoContentResult
            if (!allowNoContentResponse)
            {
                Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString), $"{verb} {response.RequestMessage.RequestUri.AbsoluteUri} content was null");
                result = JsonConvert.DeserializeObject<T>(responseString);
                Assert.IsNotNull(result, $"{verb} {response.RequestMessage.RequestUri.AbsoluteUri} content did not deserialize into {typeof(T).Name}");
            }
            else
            {//Create instance of NoContentResult or expected return type of T
                result = Activator.CreateInstance<T>();
            }

            return result;
        }

        protected static async Task<Task> DeleteSqlExec(TestDapperRepository repo, string tableName, string IDColumnName, object param)
        {
            string deleteQuery = $"Delete From {tableName} where BID = @BID and {IDColumnName}=@ID";
            var deleteResult = await repo.ExecuteSqlRawAsync(deleteQuery, param);
            return completedTask;
        }
        protected static async Task<Task> InsertSqlExec(TestDapperRepository repo, string tableName, object param)
        {

            PropertyInfo[] properties = param.GetType().GetProperties();
            string fields = "";
            string values = "";
            foreach (var property in properties)
            {
                fields += "[" + property.Name + "],";
                values += "@" + property.Name + ",";
            }
            fields = fields.TrimEnd(',');
            values = values.TrimEnd(',');
            string insertQuery = $"INSERT INTO {tableName}({fields}) VALUES ({values})";
            var insertResult = await repo.ExecuteSqlRawAsync(insertQuery, param);
            return completedTask;
        }
    }

    public class EmployeeData : BaseIdentityEntity
    {
        public override int ClassTypeID { get => throw new NotImplementedException(); set { } }
    }
}
