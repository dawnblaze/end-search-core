﻿using Endor.Search.Core.Interfaces;

namespace Endor.Search.Core.Models
{
    public class BaseAutomappableModel<T> : BaseSearchModel, IAutomappableSearchModel<T>
    {
        public virtual void MapAdditional()
        {

        }

        public void Map()
        {
            this.MapAdditional();
            this.FillExtendedProperties();
            SearchKey = GetSearchKey();
        }
    }
}
