﻿using Endor.Search.Core.Enums;

namespace Endor.Search.Core.Models
{
    public class BaseLocator
    {
        public short ParentID { get; set; }
        public short SortIndex { get; set; }
        public LocatorType LocatorType { get; set; }
        public string Locator { get; set; }
        public object MetaData { get; set; }
    }
}
