﻿using Endor.Search.Core.Interfaces;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using Lucene.Net.Index;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Endor.Search.Core.Models
{
    /// <summary>
    /// An abstract class that implements ISearchModel so that we have a common base for all search models
    /// </summary>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="I"></typeparam>
    [LukeMapper(DefaultIndex = Core.SharedKernal.LukeMapper.Attributes.Index.NO, DefaultStore = Store.NO)]
    public abstract class BaseSearchModel : ISearchModel, ISimpleSearchModel
    {
        #region Static Helper Methods
        protected static string GetSelectedSegment(IEnumerable<string> list)  => (list == null) ? "" : String.Join(" ", list);

        protected static string GetCommaSegment(IEnumerable<string> list) => (list == null) ? "" : String.Join(", ", list);
        #endregion
        
        #region IAtom Implementation
        [JsonIgnore]
        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NOT_ANALYZED)]
        //This has to be pulled from the db to make sure it get's associate with the correct table
        public virtual short BID { get; set; }

        [Luke(Store = Store.YES, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NOT_ANALYZED)]
        public int ID { get; set; }

        [Luke(Store = Store.YES, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NOT_ANALYZED)]
        public int ClassTypeID { get; private set; }

        [JsonIgnore]
        public DateTime ModifiedDT { get; set; }
        #endregion

        #region ISearchModel Methods
        private const string SearchKeyFieldName = "SearchKey";
        
        // appears to not be used
        //[JsonIgnore]
        //[Luke(Store = Store.NO, Index = LukeMapper.Attributes.Index.NOT_ANALYZED)]
        //public string Key { get; set; } 

        //[LukeSerializer("SearchSummary")]  -- Not needed now that it's a property?  Just set Store = Store.YES
        //[LukeDeserializer("SearchSummary")]
        [Luke(Store = Store.YES, Index = Core.SharedKernal.LukeMapper.Attributes.Index.ANALYZED)]
        [DBIgnore]
        public string Summary { get; set; }

        //[LukeSerializer("SearchDetail")] -- Not needed now that it's a property?  Just set Store = Store.YES
        //[LukeDeserializer("SearchDetail")]
        [Luke(Store = Store.YES, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public string Detail { get; set; }

        [Luke(Store = Store.YES, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NOT_ANALYZED)]
        [DBIgnore]
        [JsonIgnore]
        public string SearchKey { get; set; }

        public Term GetSearchKeyTerm() => new Term(SearchKeyFieldName, SearchKey);

        public string GetSearchKey()
        {
            return $"{ClassTypeID}:{ID}";
        }
        

        // this method is used to fill in the Summary, Detail, etc. based on other information in the data
        // it should be OV in most cases
        public virtual void FillExtendedProperties()
        {
            Summary = "Override FillExtendedProperties() to Set the Search Summary Value";
            Detail = "Override FillExtendedProperties() to Set the Search Detail Value";
        }
        #endregion

        #region ISimpleSearchModel Methods
        [Luke(Store = Store.YES, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public virtual string Path { get => $"/{ClassTypeID}/{ID}"; }

        [DBIgnore]
        public virtual string Header { get => Summary; }

        [DBIgnore]
        public virtual string Subheader { get => null; }

        public ISimpleSearchModel ToSimpleSearchModelDTO() => new SimpleSearchModelDTO(this); 
        #endregion

        #region Other Common Properties
        [Luke(Store = Store.YES, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        public virtual bool HasImage { get; set; }

        #endregion

    }
}
