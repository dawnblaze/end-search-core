﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class EmployeeTeamSearchModel : BaseAutomappableModel<EmployeeTeamSearchModel>
    {
        #region sorting fields
        //name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string TeamNameSort { get; set; }

        //teamMembers
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string TeamMembersSort { get; set; }

        //locations
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string LocationsSort { get; set; }
        #endregion sorting fields

        #region dates
        public DateTime CreatedDate { get; set; }
        #endregion dates

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [DBIgnore]
        public string EmployeeIDs { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [DBIgnore]
        public string LocationIDs { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string EmployeeNames { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string LocationNames { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public bool IsAdHocTeam { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.EmployeeTeamLocationLinkTableName, "TeamID", "LocationID", ChildObjectTableInfo.LocationTableName, ChildObjectTableInfo.IDColumn)]
        public ICollection<EmployeeTeamLocationLink> EmployeeTeamLocationLinks { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.EmployeeTeamEmployeeLinkTableName, "TeamID", "EmployeeID", ChildObjectTableInfo.EmployeeDataTableName, ChildObjectTableInfo.IDColumn)]
        public ICollection<EmployeeTeamEmployeeLink> EmployeeTeamLinks { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{this.Name} {this.EmployeeNames} {this.LocationNames}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => string.Empty;
        }

        public override void MapAdditional()
        {
            this.EmployeeNames = GetCommaSegment(this.EmployeeTeamLinks?.Select(el => el?.LongName));
            this.LocationNames = GetCommaSegment(this.EmployeeTeamLocationLinks?.Select(l => l?.Name));
            this.EmployeeIDs = GetSelectedSegment(this.EmployeeTeamLinks?.Select(e => e?.ID.ToString()));
            this.LocationIDs = GetSelectedSegment(this.EmployeeTeamLocationLinks?.Select(e => e?.ID.ToString()));

            this.TeamNameSort = (this.Name ?? "").Trim().ToLower();
            this.TeamMembersSort = (this.EmployeeNames ?? "").ToLower();
            this.LocationsSort = (this.LocationNames ?? "").ToLower();
        }
    }
}
