﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class EmployeeDataSearchModel : BaseAutomappableModel<EmployeeDataSearchModel>
    {

        #region sorting fields
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index.NOT_ANALYZED)]
        public string LongNameSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index.NOT_ANALYZED)]
        public string EmailAddressSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index.NOT_ANALYZED)]
        public string PhoneNumberSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index.NOT_ANALYZED)]
        public string LocationSort { get; set; }

        // LastConnectionDT
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public long LastConnectionDTSort { get; set; }

        #endregion

        #region dates
        public DateTime CreatedDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? HireDate { get; set; }
        public DateTime? ReleaseDate { get; set; }
        #endregion

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        #region names
        [Luke(Index = Index.ANALYZED)]
        public string LongName { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        public string First { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        public string Middle { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        public string Last { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        public string Suffix { get; set; }
        #endregion

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Prefix { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Position { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string LocationID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.LocationTableName, "Name", "LocationID", ChildObjectTableInfo.IDColumn, "location")]
        public string Location { get; set; }

        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        [DBIgnore]
        public string EmailAddress { get; set; }

        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        [DBIgnore]
        public string PhoneNumber { get; set; }

        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        [DBIgnore]
        public string UserName { get; set; }

        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        [DBIgnore]
        public DateTime? LastConnectionDT { get; set; }

        [Luke(Store = Store.YES, Index = Index.NO)]
        [DBIgnore]
        public string UserIDs { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.EmployeeLocatorTableName, "ParentID")]
        public ICollection<EmployeeLocator> EmployeeLocators { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.UserLinkTableName,"EmployeeID")]
        public ICollection<UserLink> UserLinks { get; set; }


        private string PositionSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.Position) ? "" : $": {this.Position},";
            }
        }

        private string PhoneNumberSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.PhoneNumber) ? "" : "Ph:" + this.PhoneNumber;
            }
        }

        private string EmailAddressSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.EmailAddress) ? "" : "Email:" + this.EmailAddress;
            }
        }

        private string UserNameSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.UserName) ? "" : "UserName:" + this.UserName;
            }
        }

        private string LocationSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.Location) ? "" : "Location:" + this.Location;
            }
        }

        public override void FillExtendedProperties()
        {
            Summary = $"{LongName} {PositionSearchFragment} {PhoneNumberSearchFragment} {EmailAddressSearchFragment} {UserNameSearchFragment} {LocationSearchFragment}";
            Detail = Summary;
        }

        public override string Header { get => LongName; }

        public override string Path => $"/manage/employee/{ID}";

        public override void MapAdditional()
        {
            this.EmailAddress = this.EmployeeLocators?.OrderBy(x => x.SortIndex)?.FirstOrDefault(x => x.LocatorType == LocatorType.Email)?.Locator;
            this.PhoneNumber = this.EmployeeLocators?.OrderBy(x => x.SortIndex)?.FirstOrDefault(x => x.LocatorType == LocatorType.Phone)?.Locator;
            
            if (this.UserLinks != null)
            {
                UserIDs = String.Join(",", this.UserLinks.Select(x => x.AuthUserID.ToString()).ToArray());
                var defaultUserLink = this.UserLinks.FirstOrDefault();
                this.UserName = defaultUserLink?.UserName ?? "";
                this.LastConnectionDT = defaultUserLink?.LastConnectionDT ?? null;
            }

            //do not map username if equal to phone or email locator
            if (this.UserName == this.EmailAddress || this.UserName == this.PhoneNumber)
            {
                this.UserName = "";
            }

            //these fields are converted to lowercase because lucene gives higher priority to uppercase characters
            this.LongNameSort = (this.LongName ?? "").ToLower();
            this.EmailAddressSort = (this.EmailAddress ?? "").ToLower();
            this.PhoneNumberSort = (this.PhoneNumber ?? "").ToLower();
            this.LocationSort = (this.Location ?? "").ToLower();
            this.LastConnectionDTSort = (this.LastConnectionDT?.Ticks ?? 0);
        }
    }
}
