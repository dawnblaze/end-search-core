﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class LaborDataSearchModel : BaseAutomappableModel<LaborDataSearchModel>
    {
        #region sorting fields
        //name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        //description
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string DescriptionSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string LaborCategoriesSort { get; set; }
        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string SKU { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string InvoiceText { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public int IncomeAccountID { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public int ExpenseAccountID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string LaborCategoryNames { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.PartLaborCategoryLinkTableName, "PartID", "CategoryID", ChildObjectTableInfo.LaborCategoryTableName, ChildObjectTableInfo.IDColumn)]
        public ICollection<LaborCategoryLink> LaborCategoryLinks { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{Name} {SKU} {InvoiceText} {Description} {LaborCategoryNames}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            if (this.LaborCategoryLinks!=null)
                this.LaborCategoryNames = String.Join(", ", this.LaborCategoryLinks?.Select(t => t.Name));

            this.NameSort = (this.Name ?? "").Trim().ToLower();
            this.DescriptionSort = (this.Description ?? "").Trim().ToLower();
            this.LaborCategoriesSort = (this.LaborCategoryNames ?? "").ToLower();
        }
    }
}
