﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class DashboardDataSearchModel : BaseAutomappableModel<DashboardDataSearchModel>
    {
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string ModuleNames { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        [DBIgnore]
        public byte Module { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{Name} {Description} {ModuleNames}";
            Detail = Summary;
        }

        public override string Header
        {
            get => Name;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            var modules = new List<string>();
            var enumVal = (Module)Module;
            if (enumVal.HasFlag(Models.Module.Accounting))
                modules.Add("Accounting");
            if (enumVal.HasFlag(Models.Module.Ecommerce))
                modules.Add("E-Commerce");
            if (enumVal.HasFlag(Models.Module.Management))
                modules.Add("Management");
            if (enumVal.HasFlag(Models.Module.Production))
                modules.Add("Production");
            if (enumVal.HasFlag(Models.Module.Purchasing))
                modules.Add("Purchasing");
            if (enumVal.HasFlag(Models.Module.Sales))
                modules.Add("Sales");
            if (enumVal.HasFlag(Models.Module.User))
                modules.Add("User");

            ModuleNames = String.Join(", ", modules.ToArray());
        }
    }
    public enum Module : short
    {
        Management = 1,
        Accounting = 2,
        Sales = 4,
        Production = 8,
        Purchasing = 16,
        Ecommerce = 32,
        User = 128,
    }
}
