﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public partial class CustomFieldDefinitionSearchModel : BaseAutomappableModel<CustomFieldDefinitionSearchModel>
    {
        #region sorting fields
        //name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        //label
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string LabelSort { get; set; }

        //appliesto
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string AppliesToSort { get; set; }

        //inputyype
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string InputTypeSort { get; set; }
        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Label { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [MapDBField("AppliesToClass")]
        public string AppliesTo { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EnumDataTypeTableName, "Name", "DataType", ChildObjectTableInfo.IDColumn, "datatype", true)]
        public string InputType { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public string Description { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{Name} {Label} {AppliesTo} {InputType}";
            Detail = $"{Summary} {Description}";
        }
            
        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => string.Empty;
        }

        public override string Path => $"/manage/global-settings/custom-field/{this.ID}";

        public override void MapAdditional()
        {
            this.Label = string.IsNullOrWhiteSpace(this.Label) ? "" : this.Label;
            
            this.NameSort = (this.Name ?? "").Trim().ToLower();
            this.LabelSort = (this.Label ?? "").Trim().ToLower();
            this.AppliesToSort = (this.AppliesTo ?? "").Trim().ToLower();
            this.InputTypeSort = (this.InputType ?? "").Trim().ToLower();
        }
    }
}
