﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public partial class AlertDefinitionSearchModel : BaseAutomappableModel<AlertDefinitionSearchModel>
    {
        #region sorting fields

        //name
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string NameSort { get; set; }

        //EmployeeName
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string EmployeeNameSort { get; set; }

        //Event
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string EventSort { get; set; }

        //ConditionReadable
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string ConditionReadableSort { get; set; }

        //Alerts
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string AlertsSort { get; set; }

        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EmployeeDataTableName, "ShortName", "EmployeeID", ChildObjectTableInfo.IDColumn, "employee")]
        public string EmployeeName { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public short EmployeeID { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.SystemAutomationTriggerDefinitionTableName, "Name", "TriggerID", ChildObjectTableInfo.IDColumn, "systemtriggerdef", true)]
        public string Event { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public string TriggerReadable { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public string ConditionReadable { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        [DBIgnore]
        public string Alerts { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        [ParentFieldMap(ChildObjectTableInfo.EnumDataTypeTableName, "Name", "DataType", ChildObjectTableInfo.IDColumn, "datatype", true)]
        public string DataType { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        [DBIgnore]
        public string AlertCategoryType { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.AlertDefinitionActionTableName, "AlertDefinitionID")]
        public ICollection<AlertDefinitionAction> Actions { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        public int TriggerID { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [AdditionalParent(ChildObjectTableInfo.SystemAutomationTriggerDefinitionTableName, "TriggerID", ChildObjectTableInfo.IDColumn, true)]
        public SystemAutomationTriggerDefinition Trigger { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{Name} {this.EmployeeName} {Event} {DataType} {AlertCategoryType}";
            Detail = "";
        }     

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            this.Event = this.Trigger?.Name;
            this.AlertCategoryType = this.Trigger?.TriggerCategoryType;
            if (this.Actions!=null)
                this.Alerts = String.Join(", ", this.Actions?.Select(a => a.DefinitionName ?? ""));
            this.NameSort = (this?.Name ?? "").Trim().ToLower();
            this.EmployeeNameSort = (this.EmployeeName ?? "").Trim().ToLower();
            this.EventSort = (this.Event ?? "").Trim().ToLower();
            this.ConditionReadableSort = (this.ConditionReadable ?? "").Trim().ToLower();
            this.AlertsSort = (this.Alerts ?? "").Trim().ToLower();
        }
    }
}
