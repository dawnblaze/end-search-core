﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;
namespace Endor.Search.Core.Models
{
    public class DestinationDataSearchModel : BaseAutomappableModel<DestinationDataSearchModel>
    {
        #region sorting fields
        //name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        //description
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string DescriptionSort { get; set; }
        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Profiles { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.DestinationProfileTableName, "DestinationID")]
        public ICollection<DestinationProfile> DestinationProfiles { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }
        public override void FillExtendedProperties()
        {
            Summary = $"{Name} {Description}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            this.NameSort = (this.Name ?? "").Trim().ToLower();
            this.DescriptionSort = (this.Description ?? "").Trim().ToLower();
            string result = String.Empty;
            if (Profiles != null)
            {
                result = string.Join(",", DestinationProfiles.Select(i => i.Name));
            }
            this.Profiles = result;
        }
    }
}
