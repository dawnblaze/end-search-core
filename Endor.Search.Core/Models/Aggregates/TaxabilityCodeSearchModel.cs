﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class TaxabilityCodeSearchModel : BaseAutomappableModel<TaxabilityCodeSearchModel>
    {
        #region sorting fields

        //name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        //TaxTreatment
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string TaxTreatmentSort { get; set; }

        //TaxCode
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string TaxCodeSort { get; set; }

        //ExemptedTaxItems
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string ExemptedTaxItemsSort { get; set; }

        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string TaxTreatment { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string TaxCode { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsSystem { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string ExemptedTaxItems { get; set; }
        
        [Luke(Index = Index.NO, Store = Store.NO)]
        [JsonIgnore]
        public bool IsTaxExempt { get; set; }
        
        [Luke(Index = Index.NO, Store = Store.NO)]
        [JsonIgnore]
        public bool HasExcludedTaxItems { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.AccountingTaxCodeItemExemptionLinkTableName, "TaxCodeID", "TaxItemID", ChildObjectTableInfo.AccountingTaxItemTableName, ChildObjectTableInfo.IDColumn)]
        public ICollection<AccountingTaxCodeItemExemptionLInk> TaxabilityCodeItemExemptionLinks { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{this.Name} {this.TaxCode} {this.TaxTreatment}";
            Detail = $"{Summary} {this.ExemptedTaxItems}";
        }

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => string.Empty;
        }

        public override void MapAdditional()
        {
            var exemptedTaxItems = this.TaxabilityCodeItemExemptionLinks?.ToList().Select(l => l.Name).ToList();
            this.ExemptedTaxItems = (exemptedTaxItems != null && exemptedTaxItems.Count > 0) ? string.Join("; ", exemptedTaxItems) : "";

            if (this.IsTaxExempt)
                this.TaxTreatment = "Tax Exempt";
            else if (!this.IsTaxExempt && this.HasExcludedTaxItems)
                this.TaxTreatment = "Taxable with Exclusions";
            else
                this.TaxTreatment = "Taxable";

            this.NameSort = (this?.Name ?? "").Trim().ToLower();
            this.TaxTreatmentSort = (this.TaxTreatment ?? "").Trim().ToLower();
            this.TaxCodeSort = (this.TaxCode ?? "").Trim().ToLower();
            this.ExemptedTaxItemsSort = (this.ExemptedTaxItems ?? "").Trim().ToLower();
        }
    }
}
