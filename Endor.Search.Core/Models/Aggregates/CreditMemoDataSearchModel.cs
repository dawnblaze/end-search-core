﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class CreditMemoDataSearchModel : TransactionHeaderDataSearchModel<CreditMemoDataSearchModel>
    {
        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [DBIgnore]
        public static string AdditionalSqlFilter => "CreditMemoData.TransactionType = 8";

    }
}
