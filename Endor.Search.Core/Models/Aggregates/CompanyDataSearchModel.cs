﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public partial class CompanyDataSearchModel : BaseAutomappableModel<CompanyDataSearchModel>
    {
        #region sorting fields
        //name
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string NameSort { get; set; }

        //primary contact
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string PrimaryContactNameSort { get; set; }

        //primary contact email
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string PrimaryContactEmailAddressSort { get; set; }

        //company phone
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string PhoneNumberSort { get; set; }

        //location
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string LocationSort { get; set; }

        //sales person
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string SalesPersonNameSort { get; set; }
        
        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        //name
        [Luke(Index = Index.ANALYZED)]
        public string Name { get; set; }

        //primary contact
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string PrimaryContactName { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.NO)]
        [DBIgnore]
        public int PrimaryContactID { get; set; }

        //primary email
        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        [DBIgnore]
        public string PrimaryContactEmailAddress { get; set; }

        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        [DBIgnore]
        public string EmailAddress { get; set; }

        //company phone
        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        [DBIgnore]
        public string PhoneNumber { get; set; }

        //location
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.NO)]
        public byte LocationID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.LocationTableName, "Name", "LocationID", ChildObjectTableInfo.IDColumn, "location")]
        public string Location { get; set; }

        
        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.CompanyLocatorTableName, "ParentID")]
        public ICollection<CompanyLocator> CompanyLocators { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.CompanyContactLinkTableName, "CompanyID", "ContactID", ChildObjectTableInfo.ContactDataTableName, ChildObjectTableInfo.IDColumn, " IsPrimary = 1 ")]
        public ICollection<CompanyContactLink> CompanyContactLinks { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [CustomSql(" Select Top 1 cl.ParentID as ID, cl.Locator as Value " +
            "from [dbo].[Contact.Locator] cl where cl.BID = @BID and cl.LocatorType = 3 and cl.ParentID IN " +
            "(select ContactID FROM [dbo].[Company.Contact.Link] Where BID = @BID and CompanyID IN @IDs and IsPrimary=1)"
            )]
        public IDStringValueObject emailAddressInfo { get; set; }

        //salesperson
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        [CustomSql("Select Top 1 emp.ShortName " +
                "from [dbo].[Employee.data] emp where emp.BID = 1 and emp.ID IN " +
                "(Select * from[dbo].[Employee.TeamLink] where RoleID = 1 And TeamID IN " +
                "(select TeamID FROM[dbo].[Company.Data] Where BID = 1 and ID IN @IDs))")]
        public string SalesPersonName { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.NO)]
        [DBIgnore]
        [CustomSql("Select Top 1 emp.ID " +
                "from [dbo].[Employee.data] emp where emp.BID = 1 and emp.ID IN " +
                "(Select * from[dbo].[Employee.TeamLink] where RoleID = 1 And TeamID IN " +
                "(select TeamID FROM[dbo].[Company.Data] Where BID = 1 and ID IN @IDs))")]
        public int SalesPersonID { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [DBIgnore]
        public static string AdditionalSqlFilter => "CompanyData.IsAdHoc = 0";
        
        private string PhoneNumberSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.PhoneNumber) ? "" : "Ph:" + this.PhoneNumber;
            }
        }

        private string EmailAddressSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.EmailAddress) ? "" : "Email:" + this.EmailAddress;
            }
        }

        public override void FillExtendedProperties()
        {
            Summary = $"{Name} {PrimaryContactName} {PrimaryContactEmailAddress} {PhoneNumberSearchFragment} {EmailAddressSearchFragment} {Location} {SalesPersonName}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            this.EmailAddress = this.CompanyLocators?.OrderBy(x => x.SortIndex)?.FirstOrDefault(x => x.LocatorType == LocatorType.Email)?.Locator;
            this.PhoneNumber = this.CompanyLocators?.OrderBy(x => x.SortIndex)?.FirstOrDefault(x => x.LocatorType == LocatorType.Phone)?.Locator;

            var primary = this.CompanyContactLinks?.FirstOrDefault();

            if (primary != null)
            {
                this.PrimaryContactID = primary.ID;
                this.PrimaryContactName = primary.LongName;
                this.PrimaryContactEmailAddress = emailAddressInfo?.Value;
            }
            else
            {
                this.PrimaryContactID = -1;
                this.PrimaryContactName = "";
                this.PrimaryContactEmailAddress = "";
            }

            this.NameSort = (this?.Name ?? "").Trim().ToLower();
            this.PrimaryContactNameSort = (this.PrimaryContactName ?? "").Trim().ToLower();
            this.PrimaryContactEmailAddressSort = (this.PrimaryContactEmailAddress ?? "").Trim().ToLower();
            this.PhoneNumberSort = (this.PhoneNumber ?? "").Trim().ToLower();
            this.LocationSort = (this.Location ?? "").Trim().ToLower();
            this.SalesPersonNameSort = (this.SalesPersonName ?? "").Trim().ToLower();
        }
    }
}
