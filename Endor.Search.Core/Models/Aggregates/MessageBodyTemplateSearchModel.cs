﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public partial class MessageBodyTemplateSearchModel : BaseAutomappableModel<MessageBodyTemplateSearchModel>
    {
        #region sorting fields

        //Name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        //Description
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string DescriptionSort { get; set; }

        //EmployeeName
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string EmployeeNameSort { get; set; }

        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Subject { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Body { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int AppliesToClassTypeID { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int MessageTemplateType { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.SystemMessageTemplateTypeTableName, "Name", "MessageTemplateType",  ChildObjectTableInfo.IDColumn, "systemmessageemplate", true)]
        public string MessageTemplateTypeText { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int ChannelType { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EnumMessageChannelTypeTableName, "Name", "ChannelType",  ChildObjectTableInfo.IDColumn, "messagechanneltype", true)]
        public string ChannelTypeText { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int LocationID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.LocationTableName, "Name", "LocationID", ChildObjectTableInfo.IDColumn, "location")]
        public string LocationName { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int CompanyID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.CompanyDataTableName, "Name", "CompanyID", ChildObjectTableInfo.IDColumn, "company")]
        public string CompanyName { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int EmployeeID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EmployeeDataTableName, "ShortName", "EmployeeID",  ChildObjectTableInfo.IDColumn,"employee")]
        public string EmployeeName { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int SortIndex { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int SizeInKB { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public bool HasAttachment { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Firt100Chars { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = String.Join(" ", new string[] {
                this.Name,
                this.Description,
                this.LocationName ?? "",
                this.CompanyName ?? "",
                this.EmployeeName ?? "",
                this.Subject,
                this.Firt100Chars
            });
            Detail = String.Join(" ", new string[] {
                this.Name,
                this.Description,
                this.LocationName ?? "",
                this.CompanyName ?? "",
                this.EmployeeName ?? "",
                this.Subject,
                this.Body
            });
        }

        public override string Header => this.Name;

        public override string Subheader => "";

        public override void MapAdditional()
        {
            this.Firt100Chars = !String.IsNullOrWhiteSpace(this.Body) && this.Body.Length >= 100 ? Regex.Replace(this.Body.Substring(0, 5), "<.*?>", String.Empty) : Regex.Replace(this.Body, "<.*?>", String.Empty);
            this.NameSort = (this.Name ?? "").Trim().ToLower();
            this.DescriptionSort = (this.Description ?? "").Trim().ToLower();
            this.EmployeeNameSort = (this.EmployeeName ?? "").Trim().ToLower();

        }
    }
}
