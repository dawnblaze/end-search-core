﻿using Endor.Search.Core.SharedKernal;
using System;

namespace Endor.Search.Core.Models
{
    /// <summary>
    /// A class for materializing search results when you don't know what type you are serializing
    /// </summary>
    public class WildcardAtomParentModel : BaseIdentityEntity
    {
        public override int ClassTypeID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
