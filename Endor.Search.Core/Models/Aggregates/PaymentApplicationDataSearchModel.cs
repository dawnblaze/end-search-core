﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class PaymentApplicationDataSearchModel : BaseAutomappableModel<PaymentApplicationDataSearchModel>
    {
        #region sorting fields
        // ID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string IDSort { get; set; }

        // Location ID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string LocationNameSort { get; set; }

        // Company ID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string CompanyNameSort { get; set; }

        // EnteredByEmployee ID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string EnteredByEmployeeNameSort { get; set; }

        // EnteredByContact ID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string EnteredByContactNameSort { get; set; }

        // GLActivityID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string GLActivityNameSort { get; set; }

        // AccountingDT
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string AccountingDTTextSort { get; set; }

        // PaymenTransactionType
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string PaymentTransactionTypeTextSort { get; set; }

        // PaymentType
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string PaymentTypeTextSort { get; set; }

        // Amount
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string AmountSort { get; set; }

        // Display Number
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string DisplayNumberSort { get; set; }

        // Notes
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NotesSort { get; set; }

        // MasterID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string MasterIDSort { get; set; }

        // OrderID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string OrderIDSort { get; set; }
        #endregion sorting fields

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public DateTime AccountingDT { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string AccountingDTText { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public byte PaymentTransactionType { get; set; }
        
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EnumPaymentTransactionTypeTableName, "Name", "PaymentTransactionType", ChildObjectTableInfo.IDColumn, "transactiontype", true)]
        public string PaymentTransactionTypeText { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.PaymentMethodTypeTableName, "Name", "PaymentMethodID", ChildObjectTableInfo.IDColumn, "paymentmethod")]
        public string PaymentTypeText { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public decimal Amount { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string DisplayNumber { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Notes { get; set; }

        // Location
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public byte LocationID { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.LocationTableName, "Name", "LocationID", ChildObjectTableInfo.IDColumn, "location")]
        public string LocationName { get; set; }

        // Company
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int CompanyID { get; set; }
        
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.CompanyDataTableName, "Name", "CompanyID", ChildObjectTableInfo.IDColumn, "company")]
        public string CompanyName { get; set; }

        // Employee
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public short EnteredByEmployeeID { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EmployeeDataTableName, "LongName", "EnteredByEmployeeID", ChildObjectTableInfo.IDColumn, "employee")]
        public string EnteredByEmployeeName { get; set; }

        // Contact
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int EnteredByContactID { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EmployeeDataTableName, "LongName", "EnteredByContactID", ChildObjectTableInfo.IDColumn, "employee")]
        public string EnteredByContactName { get; set; }

        // GLActivity
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int GLActivityID { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.GLActivityTableName, "Name", "GLActivityID", ChildObjectTableInfo.IDColumn, "glactivity")]
        public string GLActivityName { get; set; }

        // Master
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int MasterID { get; set; }

        // Order
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int OrderID { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = this.GenerateSearchSummary();
            Detail = Summary;
        }

        public string GenerateSearchSummary()
        {
            return String.Join(" ", new string[] {
                this.LocationName,
                this.CompanyName,
                this.EnteredByEmployeeName,
                this.EnteredByContactName,
                this.AccountingDTText,
                this.PaymentTransactionTypeText,
                this.Amount.ToString(),
                this.DisplayNumber,
                this.Notes,
                this.GLActivityName
            });
        }

        public override string Header
        {
            get => $"{ID}";
        }

        public override string Subheader
        {
            get => $"{CompanyName}";
        }

        public override void MapAdditional()
        {
            AccountingDTText = this.AccountingDT.ToString("o");

            PaymentTransactionTypeText = this.PaymentTransactionType.ToString()?.ToLower() ?? "";

            // sorting fields
            this.IDSort = (this.ID.ToString() ?? "").ToLower();
            this.LocationNameSort = (this.LocationName ?? "").Trim().ToLower();
            this.CompanyNameSort = (this.CompanyName ?? "").Trim().ToLower();
            this.EnteredByEmployeeNameSort = (this.EnteredByEmployeeName ?? "").Trim().ToLower();
            this.EnteredByContactNameSort = (this.EnteredByContactName ?? "").Trim().ToLower();
            this.AccountingDTTextSort = (this.AccountingDTText ?? "").Trim().ToLower();
            this.PaymentTransactionTypeTextSort = (this.PaymentTransactionTypeText ?? "").Trim().ToLower();
            this.PaymentTypeTextSort = (this.PaymentTypeText ?? "").Trim().ToLower();
            this.DisplayNumberSort = (this.DisplayNumber ?? "").Trim().ToLower();
            this.AmountSort = (this.Amount.ToString() ?? "").Trim().ToLower();
            this.NotesSort = (this.Notes ?? "").Trim().ToLower();
            this.GLActivityNameSort = (this.GLActivityName ?? "").Trim().ToLower();
            this.MasterIDSort = (this.MasterID.ToString() ?? "").ToLower();
            this.OrderIDSort = (this.OrderID.ToString() ?? "").ToLower();
        }
    }
}
