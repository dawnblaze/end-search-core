﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    /// <summary>
    /// https://corebridge.atlassian.net/browse/END-3048
    /// 
    /// Search Fields: Subject, Entire Body (plain text if possible), UserNames, EmployeeID, ContactID, AttachmentNames, ChannelName
    /// </summary>
    public partial class MessageHeaderSearchModel : BaseAutomappableModel<MessageHeaderSearchModel>
    {
        #region sorting fields

        //ID
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string IDSort { get; set; }

        // ReceivedDT
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public long ReceivedDTSort { get; set; }

        #endregion sorting fields

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public DateTime ReceivedDT { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public short EmployeeID { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int ParticipantID { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int BodyID { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsRead { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public DateTime? ReadDT { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsDeleted { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public bool IsExpired { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public DateTime? DeletedOrExpiredDT { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EnumMessageChannelTypeTableName, "Name", "Channels",  ChildObjectTableInfo.IDColumn, "messagechanneltype", true)]
        public string ChannelName { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool InSentFolder { get; set; }

        //--messagebody parts
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.MessageBodyTableName, "Subject", "BodyID", ChildObjectTableInfo.IDColumn, "messagebody")]
        public string Subject { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.MessageBodyTableName, "BodyFirstLine", "BodyID", ChildObjectTableInfo.IDColumn, "messagebody")]
        public string BodyFirstLine { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.MessageBodyTableName, "HasBody", "BodyID", ChildObjectTableInfo.IDColumn, "messagebody")]
        public bool HasBody { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.MessageBodyTableName, "AttachedFileCount", "BodyID", ChildObjectTableInfo.IDColumn, "messagebody")]
        public byte AttachedFileCount { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.MessageBodyTableName, "AttachedFileNames", "BodyID", ChildObjectTableInfo.IDColumn, "messagebody")]
        public string AttachedFileNames { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.MessageBodyTableName, "HasAttachment", "BodyID", ChildObjectTableInfo.IDColumn, "messagebody")]
        public bool HasAttachment { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.MessageBodyTableName, "MetaData", "BodyID", ChildObjectTableInfo.IDColumn, "messagebody")]
        public string MetaData { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.MessageBodyTableName, "WasModified", "BodyID", ChildObjectTableInfo.IDColumn, "messagebody")]
        public bool WasModified { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.MessageBodyTableName, "SizeInKB", "BodyID", ChildObjectTableInfo.IDColumn, "messagebody")]
        public int? SizeInKB { get; set; }

        //unmapped
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string BodyWithTags { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Body { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Usernames { get; set; }
        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }


        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [SiblingCollection("BodyID", ChildObjectTableInfo.MessageParticipantInfoTableName, "BodyID")]
        public ICollection<MessageParticipantInfo> MessageParticipantInfos { get; set; }
        //visible properties - end

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [DBIgnore]
        public string _messageBodyString { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{this.Subject} {this.Body} {this.Usernames} {this.AttachedFileNames} {this.ChannelName}";
            Detail = Summary;
        }

        public override string Header  
        {
            get => this.Subject;
        }

        public override string Subheader
        {
            get => this.BodyFirstLine;
        }

        public override void MapAdditional()
        {           
            var usernames = this.MessageParticipantInfos
                                ?.Select(p => p.UserName)
                                ?.ToList();

            this.Usernames = string.Join(",", usernames);

            this.Body = Regex.Replace(this._messageBodyString ?? "", "<.*?>", String.Empty);
            this.BodyWithTags = this._messageBodyString;
     
            this.ReceivedDTSort = ReceivedDT.Ticks;
            this.IDSort = (this.ID.ToString() ?? "").ToLower();
        }
    }
}
