﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class PaymentMasterDataSearchModel : BaseAutomappableModel<PaymentMasterDataSearchModel>
    {
        #region sorting fields
        // ID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string IDSort { get; set; }

        // Location ID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string LocationNameSort { get; set; }

        // Company ID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string CompanyNameSort { get; set; }

        // AccountingDT
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string AccountingDTTextSort { get; set; }


        // Order IDs
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string FormattedNumbersSort { get; set; }

        // PaymenTransactionType
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string PaymentTransactionTypeTextSort { get; set; }

        // Amount
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string AmountSort { get; set; }

        // Display Number
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string DisplayNumberSort { get; set; }
        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string PaymentIDText { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public DateTime AccountingDT { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string AccountingDTText { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public byte PaymentTransactionType { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string PaymentTransactionTypeText { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        [ParentFieldMap(ChildObjectTableInfo.EnumPaymentTransactionTypeTableName, "Name", "PaymentTransactionType", ChildObjectTableInfo.IDColumn, "transactiontype", true)]
        public string PaymentTransactionTypeTextOriginal { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string PaymentTypeText { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public decimal Amount { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string AmountText { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Method { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string DisplayNumber { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [DBIgnore]
        public string FormattedNumbers { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string FormattedNumberText { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string OrderIDs { get; set; }


        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string AppAmounts { get; set; }

        // Location
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public byte LocationID { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.LocationTableName, "Name", "LocationID", ChildObjectTableInfo.IDColumn, "location")]
        public string LocationName { get; set; }

        // Company
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int CompanyID { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.CompanyDataTableName, "Name", "CompanyID", ChildObjectTableInfo.IDColumn, "company")]
        public string CompanyName { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.PaymentApplicationTableName, "MasterID")]
        public ICollection<PaymentApplication> PaymentApplications { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        private string AccountingDTTextSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.AccountingDTText) ? "" : $"AccountingDate: {this.AccountingDTText}";
            }
        }

        private string MethodSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.Method) ? "" : $"Method: {this.Method}";
            }
        }

        private string AmountSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.AmountText) ? "" : $"Amount: {this.AmountText}";
            }
        }

        private string OrderIDSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.FormattedNumberText) ? "" : $"OrderID: {this.FormattedNumberText}";
            }
        }

        public override void FillExtendedProperties()
        {
            Summary = $"{PaymentIDText} {LocationName} {CompanyName} {AccountingDTTextSearchFragment} {PaymentTransactionTypeText} {MethodSearchFragment} {FormattedNumbers} {AmountSearchFragment} {AppAmounts} {OrderIDSearchFragment}";
            Detail = Summary;
        }

        public override string Header
        {
            get => $"{ID}";
        }

        public override string Subheader
        {
            get => $"{CompanyName}";
        }

        public override void MapAdditional()
        {
            AccountingDTText = this.AccountingDT.ToString("MM/dd/yyyy - hh:mm tt");

            PaymentIDText = this.ID.ToString();

            PaymentTransactionTypeText = PaymentTransactionTypeTextOriginal.Replace("_", " ");

            if (this.PaymentApplications != null)
            {
                //var methodId = (byte)this.PaymentApplications?.Select(x => x.PaymentMethodID).FirstOrDefault();
                this.PaymentTypeText = this.PaymentApplications?.Select(x => x.PaymentMethodText).FirstOrDefault();
            }
            
            string displayNumber = DisplayNumber != null && DisplayNumber != "" ? $" - {DisplayNumber}" : "";
            Method = $"{PaymentTypeText}{displayNumber}";

            FormattedNumbers = GetLinkedPaymentApplicationOrderFormattedNumberSegment();
            OrderIDs = GetLinkedPaymentApplicationOrderIDSegment();
            AppAmounts = GetLinkedPaymentApplicationOrderAmountSegment();

            List<string> orderIDs = this.PaymentApplications?.Select(x => x.FormattedNumber ?? "").ToList();
            if (orderIDs != null && orderIDs.Count > 0)
            {
                FormattedNumberText = orderIDs.Count > 1 ? "Multiple" : orderIDs[0];
            }
            else
            {
                FormattedNumberText = "";
            }

            AmountText = (this.Amount.ToString("C") ?? "").Trim().ToLower();

            // sorting fields
            this.IDSort = (this.ID.ToString() ?? "").ToLower();
            this.LocationNameSort = (this.LocationName ?? "").Trim().ToLower();
            this.CompanyNameSort = (this.CompanyName ?? "").Trim().ToLower();
            this.AccountingDTTextSort = (this.AccountingDTText ?? "").Trim().ToLower();
            this.FormattedNumbersSort = (this.FormattedNumbers ?? "").Trim().ToLower();
            this.PaymentTransactionTypeTextSort = (this.PaymentTransactionTypeText ?? "").Trim().ToLower();
            this.DisplayNumberSort = (this.DisplayNumber ?? "").Trim().ToLower();
            this.AmountSort = (this.Amount.ToString() ?? "").Trim().ToLower();
        }

        private string GetLinkedPaymentApplicationOrderIDSegment()
        {
            return GetSelectedSegment(this.PaymentApplications?.Select(x => x.OrderID + "").ToList());
        }
        private string GetLinkedPaymentApplicationOrderFormattedNumberSegment()
        {
            return GetSelectedSegment(this.PaymentApplications?.Select(x => x.FormattedNumber ?? "").ToList());
        }

        private string GetLinkedPaymentApplicationOrderAmountSegment()
        {
            return GetSelectedSegment(this.PaymentApplications?.Select(x => x.Amount.ToString("C")?.ToLower() ?? "").ToList());
        }
    }
}
