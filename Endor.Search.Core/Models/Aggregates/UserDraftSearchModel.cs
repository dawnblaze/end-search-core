﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class UserDraftSearchModel : BaseAutomappableModel<UserDraftSearchModel>
    {
        #region UserDraft sorting fields
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public long DraftDateSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string DraftBySort { get; set; }
        #endregion UserDraft sorting fields

        #region Order/Estimate sorting fields
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string CompanyNameSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string PrimaryContactNameSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public double PriceTotalSort { get; set; }
        #endregion Order/Estimate sorting fields


        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Name { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string DraftDate { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public DateTime? ExpirationDT { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string DraftBy { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public short UserLinkID { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [MapDBField("MetaData")]
        public string ObjectJSON { get; set; }

        #region Search Only Filters
        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        public int ObjectCTID { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [DBIgnore]
        public string ContactRolesNames { get; set; }
        #endregion

        #region Order/Estimate Properties
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string CompanyName { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string PrimaryContactName { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public decimal? PriceTotal { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        #endregion

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [AdditionalParent(ChildObjectTableInfo.UserLinkTableName, "UserLinkID", ChildObjectTableInfo.IDColumn)]
        public UserLink UserLink { get; set; }

        public string GenerateSearchSummary()
        {
            return String.Join(" ", new string[] {
                this.DraftBy ?? "",
                this.DraftDate ?? "",
                this.CompanyName ?? "",
                this.PrimaryContactName ?? "",
                this.PriceTotal.ToString() ?? ""
            });
        }

        public override void FillExtendedProperties()
        {
            Summary = this.GenerateSearchSummary();
            Detail = Summary;
        }

        public override string Header
        {
            get => this.CompanyName;
        }

        public override string Subheader
        {
            get => this.Name;
        }

        public override void MapAdditional()
        {
            this.DraftDate = this?.ModifiedDT.ToString("MM/dd/yyyy");
            this.Name = $"this ({FormatDate(this.ModifiedDT)})";
            
            if (this.UserLink != null)
            {
                this.DraftBy = this.UserLink.EmployeeName ?? "";
            }

            this.DraftDateSort = this?.ModifiedDT.Ticks ?? 0;
            this.DraftBySort = (this.DraftBy ?? "").Trim().ToLower();
            this.ObjectCTID = this?.ObjectCTID ?? 0;
            
            if (this.ObjectJSON != null)
            {
                if (this?.ObjectCTID == (int)ClassTypeIDMap.CTIDForClassType(typeof(OrderDataSearchModel)) 
                    || this?.ObjectCTID == ClassTypeIDMap.CTIDForClassType(typeof(EstimateDataSearchModel)))
                {
                    dynamic input = JsonConvert.DeserializeObject(this.ObjectJSON);
                    
                    if (input != null)
                    {
                        if (input.Company != null)
                        {
                            this.CompanyName = input.Company.Name;
                            this.CompanyNameSort = (this.CompanyName ?? "").Trim().ToLower();
                        }
                        else
                        {
                            this.CompanyName = "No Company Selected";
                        }

                        if (input.ContactRoles != null)
                        {
                            IEnumerable<dynamic> ContactRoles = input.ContactRoles;
                            this.ContactRolesNames = GetLinkedContactSegment(input);

                            var primary = ContactRoles.FirstOrDefault(x => x.RoleType == "1"); //Primary = 1 
                            if (primary != null)
                            {
                                this.PrimaryContactName = primary?.Contact?.ShortName ?? "";
                                this.PrimaryContactNameSort = (this.PrimaryContactName ?? "").Trim().ToLower();
                            }
                        }

                        this.Description = input.Description ?? "";
                        this.PriceTotal = input.PriceTotal ?? 0;
                        this.PriceTotalSort = Convert.ToDouble(this.PriceTotal);
                    }
                }
            }
        }

        private static string FormatDate(DateTime date)
        {
            string suffix;

            if (new[] { 11, 12, 13 }.Contains(date.Day))
            {
                suffix = "th";
            }
            else if (date.Day % 10 == 1)
            {
                suffix = "st";
            }
            else if (date.Day % 10 == 2)
            {
                suffix = "nd";
            }
            else if (date.Day % 10 == 3)
            {
                suffix = "rd";
            }
            else
            {
                suffix = "th";
            }

            return string.Format("{0:MMMM} {1}{2} {0:h:mm tt}", date, date.Day, suffix);
        }

        private static string GetLinkedContactSegment(IEnumerable<dynamic> ContactRoles)
        {
            return GetSelectedSegment((IEnumerable<string>)(ContactRoles?.Select(x => x.Contact?.ShortName ?? "")));
        }
        
    }
}
