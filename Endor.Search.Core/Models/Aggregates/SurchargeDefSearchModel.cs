﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class SurchargeDefSearchModel : BaseAutomappableModel<SurchargeDefSearchModel>
    {
        #region sorting fields

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string TaxCodeSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string IncomeAccountSort { get; set; }

        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int IncomeAccountID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.AccountingGLAccountTableName, "Name", "IncomeAccountID", ChildObjectTableInfo.IDColumn, "glaccount")]
        public string IncomeAccount { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public short TaxCodeID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.AccountingTaxCodeTableName, "Name", "TaxCodeID", ChildObjectTableInfo.IDColumn, "taxcode")]
        public string TaxCode { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{Name} {TaxCode} {IncomeAccount}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => "";
        }

        public void MapAdditional(SurchargeDef parent)
        {
            this.NameSort = (parent?.Name ?? "").Trim().ToLower();
            this.IncomeAccountSort = (this.IncomeAccount ?? "").Trim().ToLower();
            this.TaxCodeSort = (this.TaxCode ?? "").Trim().ToLower();
        }
    }
}
