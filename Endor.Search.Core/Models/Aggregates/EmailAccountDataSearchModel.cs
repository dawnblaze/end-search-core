﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public partial class EmailAccountDataSearchModel : BaseAutomappableModel<EmailAccountDataSearchModel>
    {
        #region sorting fields

        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string EmailAddressSort { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string TypeSort { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string AssociationSort { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string StatusSort { get; set; }

        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.ANALYZED)]
        public string DisplayName { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EmployeeDataTableName, "ShortName", "EmployeeID", ChildObjectTableInfo.IDColumn, "employee")]
        public string EmployeeName { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public short? EmployeeID { get; set; }

        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        public string EmailAddress { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [MapDBFieldAttribute("StatusType")]
        public int StatusID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EnumEmailAccountStatusTableName, "Name", "StatusType", ChildObjectTableInfo.IDColumn, "enumemail", true)]
        public string StatusTypeText { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Associations { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsPrivate { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [DBIgnore]
        public string TeamIDs { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string TeamNames { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string TypeText { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.EmailAccountTeamLinkTableName, "EmailAccountID", "TeamID", ChildObjectTableInfo.EmployeeTeamTableName,ChildObjectTableInfo.IDColumn)]
        public ICollection<EmployeeTeamLink> EmployeeTeams { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{EmailAddress} {TypeText} {Associations} {StatusTypeText}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.DisplayName;
        }

        public override void MapAdditional()
        {
            if (this.EmployeeTeams != null && this.EmployeeTeams.Count > 0)
            {
                this.TypeText = "Teams";
                this.TeamIDs = GetSelectedSegment(this.EmployeeTeams.Select(l => l.ID.ToString()));
                this.TeamNames = GetCommaSegment(this.EmployeeTeams.Select(l => l.Name ?? ""));
                this.Associations = this.TeamNames;
            }
            else
            {
                this.TypeText = "Personal";
                this.Associations = this.EmployeeName;
            }

            this.EmailAddressSort = (this?.EmailAddress ?? "").Trim().ToLower();
            this.TypeSort = this.TypeText.Trim().ToLower();
            this.AssociationSort = this.Associations.Trim().ToLower();
            this.StatusSort = this.StatusTypeText.Trim().ToLower();
        }
    }
}
