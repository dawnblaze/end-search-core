﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.Helper_Classes;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class BoardDefinitionDataSearchModel : BaseAutomappableModel<BoardDefinitionDataSearchModel>
    {
        #region sorting fields
        //name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        //boardyype
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string BoardTypeSort { get; set; }

        //DetailLevel
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string DetailLevelSort { get; set; }
        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsSystem { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public short DataType { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        public bool LimitToRoles { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public bool IsAlwaysShown { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string ConditionFx { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public DateTime? LastRunDT { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public int? LastRecordCount { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public decimal? LastRunDurationSec { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public DateTime? CummCounterDT { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public int? CummRunCount { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public decimal? CummRunDurationSec { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public decimal? AverageRunDuration { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        [DBIgnore]
        public string Type { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        [DBIgnore]
        public string DetailLevel { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string ModuleNames { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.BoardModuleLinkTableName, "BoardID", "ModuleType", ChildObjectTableInfo.EnumModuleTableName, ChildObjectTableInfo.IDColumn)]
        public ICollection<BoardModuleLink> ModuleLinks { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }


        public override void FillExtendedProperties()
        {
            Summary = this.GenerateSearchSummary();
            Detail = Summary;
        }

        private string GenerateSearchSummary()
        {
            var sb = new StringBuilder(Name);

            var boardDataTypeMappings = Extensions.GetBoardDataTypeMappings(DataType);
            string type = boardDataTypeMappings?.Type;
            string detailLevel = boardDataTypeMappings?.DetailLevel;

            if (!string.IsNullOrWhiteSpace(type))
                sb.Append($" {type}");

            if (!string.IsNullOrWhiteSpace(detailLevel))
                sb.Append($" {detailLevel}");

            return sb.ToString();
        }

        public override string Header { get => this.Name; }

        public override string Subheader { get => ""; }

        public override void MapAdditional()
        {
            var boardDataTypeMappings = Extensions.GetBoardDataTypeMappings(DataType);

            Type = boardDataTypeMappings?.Type;
            DetailLevel = boardDataTypeMappings?.DetailLevel;
            if (this.ModuleLinks != null)
                ModuleNames = String.Join(", ", this.ModuleLinks?.Select(a => a.Name ?? ""));

            this.NameSort = (this?.Name ?? "").Trim().ToLower();
            this.BoardTypeSort = (Type ?? "").Trim().ToLower();
            this.DetailLevelSort = (boardDataTypeMappings?.DetailLevel ?? "").Trim().ToLower();
        }
    }
}
