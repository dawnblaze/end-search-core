﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class DashboardWidgetDefinitionSearchModel : BaseAutomappableModel<DashboardWidgetDefinitionSearchModel>
    {

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string DefaultName { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string ModuleNames { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        public short? Modules { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        [DBIgnore]
        public override short BID { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{DefaultName} {Description} {ModuleNames}";
            Detail = Summary;
        }

        public override string Header
        {
            get => DefaultName;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            if (this.Modules.HasValue)
            {
                var modules = new List<string>();
                var module = (Module)this.Modules;
                if (module.HasFlag(Module.Accounting))
                    modules.Add("Accounting");
                if (module.HasFlag(Module.Ecommerce))
                    modules.Add("E-Commerce");
                if (module.HasFlag(Module.Management))
                    modules.Add("Management");
                if (module.HasFlag(Module.Production))
                    modules.Add("Production");
                if (module.HasFlag(Module.Purchasing))
                    modules.Add("Purchasing");
                if (module.HasFlag(Module.Sales))
                    modules.Add("Sales");
                if (module.HasFlag(Module.User))
                    modules.Add("User");

                ModuleNames = String.Join(", ", modules.ToArray());
            }

        }
    }
}
