﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class DomainDataSearchModel : BaseAutomappableModel<DomainDataSearchModel>
    {
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Domain { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.LocationTableName, "Name", "LocationID", ChildObjectTableInfo.IDColumn, "location")]
        public string Location { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.NO)]
        public byte LocationID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EnumDomainAccessTypeTableName, "Name", "AccessType", ChildObjectTableInfo.IDColumn, "accesstype", true)]
        public string ApplicationType { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public bool IsActive { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        [ParentFieldMap(ChildObjectTableInfo.EnumDomainAccessTypeTableName, "Name", "Status", ChildObjectTableInfo.IDColumn, "status", true)]
        public string Status { get; set; }


        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public bool IsDefault { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string DNSStatus { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.SSLCertificateData, "ValidToDT", "SSLCertificateID", ChildObjectTableInfo.IDColumn, "sslcertificate", true)]
        public DateTime? SSLExpirationDT { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        [DBIgnore]
        public bool Use3rdPartySSLCertificate { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        public bool? Use3rdPartyCertificate { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public DateTime? DNSVerifiedDT { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        public DateTime? DNSLastAttemptDT { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{Domain} {Location} {ApplicationType}";
            Detail = Summary;
        }

        public override string Header {
            get => this.Domain;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            this.IsActive = this.Status != "Inactive";
            this.DNSStatus = this.Status;
            this.Use3rdPartySSLCertificate = this.Use3rdPartyCertificate ?? false;
            this.DNSLastAttemptDT = this.DNSLastAttemptDT;
            this.DNSVerifiedDT = this.DNSVerifiedDT;
        }
    }
}
