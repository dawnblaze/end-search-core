﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.Models.DTOs;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class LocationDataSearchModel : BaseAutomappableModel<LocationDataSearchModel>
    {
        #region sorting fields
        //name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        //street
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string StreetSort { get; set; }

        //city
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string CitySort { get; set; }

        //state
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string StateSort { get; set; }

        //postalcode
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string PostalCodeSort { get; set; }
        #endregion sorting fields

        #region dates
        public DateTime CreatedDate { get; set; }
        #endregion

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsDefault { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }
        [JsonIgnore]
        public string LegalName { get; set; }
        [JsonIgnore]
        public string DBA { get; set; }
        [JsonIgnore]
        public string InvoicePrefix { get; set; }
        [JsonIgnore]
        public string EstimatePrefix { get; set; }
        [JsonIgnore]
        public string OrderNumberPrefix { get; set; }
        [JsonIgnore]
        public string POPrefix { get; set; }
        [JsonIgnore]
        public string TaxID { get; set; }
        [JsonIgnore]
        public short? TimeZoneID { get; set; }
        [JsonIgnore]
        public bool? UseDST { get; set; }
        [JsonIgnore]
        public string DefaultAreaCode { get; set; }
        [JsonIgnore]
        public string Slogan { get; set; }

        [Luke(Store = Store.YES, Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string EmailAddress { get; set; }

        [Luke(Store = Store.YES, Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string PhoneNumber { get; set; }

        [JsonIgnore]
        [DBIgnore]
        public AddressMetaData PrimaryAddress { get; set; }

        [Luke(Store = Store.YES, Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string Street { get; set; }

        [Luke(Store = Store.YES, Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string City { get; set; }

        [Luke(Store = Store.YES, Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string State { get; set; }

        [Luke(Store = Store.YES, Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string PostalCode { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.EmployeeLocatorTableName, "ParentID")]
        public ICollection<LocationLocator> LocationLocators { get; set; }

        private string GenerateSearchSummary()
        {
            return String.Join(" ", new string[] {
                $"{this.Name}{(this.IsDefault ? " (Default)" : "")}",
                this.Street ?? "",
                this.City ?? "",
                this.State ?? "",
                this.PostalCode ?? ""
            });
        }

        public override void FillExtendedProperties()
        {
            Summary = this.GenerateSearchSummary();
            Detail = "";
        }

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => "";
        }

        public override string Path => $"/manage/location/{ID}";

        public override void MapAdditional()
        {
            this.PhoneNumber = this.LocationLocators?.OrderBy(x => x.SortIndex).FirstOrDefault(x => x.LocatorType == LocatorType.Phone)?.Locator;
            this.PrimaryAddress = this.LocationLocators?.OrderBy(x => x.SortIndex).FirstOrDefault(x => x.LocatorType == LocatorType.Address)?.MetaData as AddressMetaData;
            this.EmailAddress = this.LocationLocators?.OrderBy(x => x.SortIndex).FirstOrDefault(x => x.LocatorType == LocatorType.Email)?.Locator;

            if (this.PrimaryAddress != null)
            {
                this.Street = this.PrimaryAddress.Street1;
                this.City = this.PrimaryAddress.City;
                this.State = this.PrimaryAddress.State;
                this.PostalCode = this.PrimaryAddress.PostalCode;
            }
            this.Name = $"{this.Name}{(this.IsDefault ? " (Default)" : "")}";
            this.NameSort = (this.Name ?? "").Trim().ToLower();
            this.StreetSort = (this.Street ?? "").Trim().ToLower();
            this.CitySort = (this.City ?? "").Trim().ToLower();
            this.StateSort = (this.State ?? "").Trim().ToLower();
            this.PostalCodeSort = (this.PostalCode ?? "").Trim().ToLower();
        }
    }
}
