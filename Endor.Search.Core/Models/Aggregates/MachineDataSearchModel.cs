﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{ 
    public class MachineDataSearchModel : BaseAutomappableModel<MachineDataSearchModel>
    {
        #region sorting fields
        //name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        // Machine Categories
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string MachineCategoriesSort { get; set; }



        //description
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string DescriptionSort { get; set; }
        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string SKU { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public short ActiveInstanceCount { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public short ActiveProfileCount { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Instances { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Profiles { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string MachineCategoryNames { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.PartMachineCategoryLinkTableName, "PartID", "CategoryID", ChildObjectTableInfo.MachineCategoryTableName, ChildObjectTableInfo.IDColumn)]
        public ICollection<LaborCategoryLink> MachineCategoryLinks { get; set; } //For this purpose LaborCategory looks the same

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.MachineInstanceTableName, "MachineID")]
        public ICollection<MachineInstance> MachineInstances { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.MachineProfileTableName, "MachineID")]
        public ICollection<MachineProfile> MachineProfiles { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{Name} {Description} {MachineCategoryNames}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            this.MachineCategoryNames = GetCommaSegment(this.MachineCategoryLinks?.Select(t => t.Name));

            this.NameSort = (this.Name ?? "").Trim().ToLower();
            this.DescriptionSort = (this.Description ?? "").Trim().ToLower();
            this.MachineCategoriesSort = (this.MachineCategoryNames ?? "").ToLower();
            this.Instances = Map_Instances();
            this.Profiles = Map_Profiles();
        }

        private string Map_Instances()
        {
            string result = String.Empty;

            if (this.MachineInstances != null)
            {
                result = string.Join(",", this.MachineInstances.Select(i => i.Name));
            }

            return result;

        }

        private string Map_Profiles()
        {
            string result = String.Empty;

            if (this.MachineProfiles != null)
            {
                result = string.Join(",", this.MachineProfiles.Select(i => i.Name));
            }

            return result;
        }
    }
}
