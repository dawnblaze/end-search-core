﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class CustomFieldLayoutDefinitionSearchModel : BaseAutomappableModel<CustomFieldLayoutDefinitionSearchModel>
    {
        #region sorting fields
        //appliestoclasstypeidtext
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string AppliesToClassTypeIDTextSort { get; set; }

        //tabname
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string TabNameSort { get; set; }

        //subtabname
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string SubTabNameSort { get; set; }
        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsSystem { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public short AppliesToClassTypeID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EnumClassTypeTableName, "Name", "AppliesToClassTypeID", ChildObjectTableInfo.IDColumn, "classType", true)]
        public string AppliesToClassTypeIDText { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string TabName { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string SubTabName { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public bool IsMainTab { get; set; }
        
        [Luke(Index = Index.NO, Store = Store.NO)]
        public bool IsAllTab { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        public bool IsSubTab { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        public string Name { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{IsActive} {IsSystem} {TabName} {SubTabName} {AppliesToClassTypeIDText}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.TabName;
        }

        public override string Subheader
        {
            get => this.SubTabName;
        }

        public override string Path => $"/manage/global-settings/custom-field-layout/{ID}";

        public override void MapAdditional()
        {
            this.IsMainTab = this.IsActive && this.IsSystem && !this.IsAllTab && !this.IsSubTab;
            
            //https://corebridge.atlassian.net/browse/END-2358
            if (this.IsSubTab)
            {
                this.TabName = $"{this.AppliesToClassTypeIDText} Custom Details";
                this.SubTabName = this.Name;
            }
            else
            {
                this.TabName = this.Name;
                this.SubTabName = "";
            }

            this.AppliesToClassTypeIDTextSort = (this.AppliesToClassTypeIDText ?? "").Trim().ToLower();
            this.TabNameSort = (this.TabName ?? "").Trim().ToLower();
            this.SubTabNameSort = (this.SubTabName ?? "").Trim().ToLower();
        }
    }
}
