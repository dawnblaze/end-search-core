﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.Helper_Classes;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public abstract class TransactionHeaderDataSearchModel<M> : BaseAutomappableModel<M>
        where M : BaseAutomappableModel<M>
    {

        #region sorting fields


        //formattedNumber
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string FormattedNumberSort { get; set; }

        //StatusID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string StatusIDSort { get; set; }

        //Amount
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public double PriceTotalSort { get; set; }

        //Amount
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public double PaymentBalanceDueSort { get; set; }


        //ProductionDue
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public long CreatedDateSort { get; set; }


        //location
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string LocationNameSort { get; set; }

        //PrimaryContactName
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string PrimaryContactNameSort { get; set; }

        //ID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string IDSort { get; set; }

        //CompanyName
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string CompanyNameSort { get; set; }

        #endregion sorting fields

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public byte TransactionType { get; set; }

        //fuzzy match
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }

        //fuzzy match, but don't use to search, use Order instead
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public string FormattedNumber { get; set; }

        //fuzzy match
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public int Number { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.CompanyDataTableName, "Name", "CompanyID", ChildObjectTableInfo.IDColumn, "company")]
        public string CompanyName { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int CompanyID { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [MapDBField("OrderStatusID")]
        public byte StatusID { get; set; }
        
        [Luke(Index = Index.NO, Store = Store.YES)]
        [DBIgnore]
        public string StatusText { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        [DBIgnore]
        public string OrderStatusStart { get; set; }
        
        [Luke(Index = Index.NO, Store = Store.NO)]
        public DateTime OrderStatusStartDT { get; set; }



        [Luke(Index = Index.NO, Store = Store.YES)]
        [DBIgnore]
        public string CreatedDate { get; set; }


        //search only filters
        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [DBIgnore]
        public string ContactRolesNames { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [DBIgnore]
        public string EmployeeRolesIDs { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [DBIgnore]
        public string Order { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public bool IsVoided { get; set; }



        //price and payment (exact match)
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [MapDBField("[Price.Total]")]
        public decimal? PriceTotal { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [MapDBField("[Price.ProductTotal]")]
        public decimal? PriceProductTotal { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [MapDBField("[Price.Discount]")]
        public decimal? PriceDiscount { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [MapDBField("[Price.PreTax]")]
        public decimal? PricePreTax { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [MapDBField("[Price.Tax]")]
        public decimal? PriceTax { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [MapDBField("[Payment.Total]")]
        public decimal? PaymentTotal { get; set; }
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [MapDBField("[Payment.BalanceDue]")]
        public decimal? PaymentBalanceDue { get; set; }

        //location
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public byte LocationID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.LocationTableName, "Name", "LocationID", ChildObjectTableInfo.IDColumn, "location")]
        public string LocationName { get; set; }

        //primary contact
        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public int PrimaryContactID { get; set; }
        
        [Luke(Index = Index.NO, Store = Store.YES)]
        [DBIgnore]
        public string PrimaryContactName { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ParentFieldMap(ChildObjectTableInfo.EnumOrderOrderStatusTableName, "Name", "OrderStatusID", ChildObjectTableInfo.IDColumn, "orderstatus", true)]
        public string OrderStatusName { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.OrderContactRoleTableName, "OrderID")]
        public ICollection<OrderContactRole> ContactRoles { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.OrderKeyDateTableName, "OrderID")]
        public ICollection<OrderKeyDate> Dates { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.OrderOrderLinkTableName, "OrderID")]
        public ICollection<OrderOrderLink> Links { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.OrderEmployeeRoleTableName, "OrderID")]
        public ICollection<OrderEmployeeRole> EmployeeRoles { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = this.GenerateSearchSummary();
            Detail = Summary;
        }

        private string GenerateSearchSummary()
        {
            return String.Join(" ", new string[] {
                (this.FormattedNumber ?? "").StripDashes(),
                this.CompanyName ?? "",
                this.Description ?? "",
                this.OrderStatusStart ?? "",
                this.StatusText ?? "",
                this.LocationName ?? "",
                this.PrimaryContactName ?? "",
                this.PriceTotal.HasValue ? this.PriceTotal.ToString() : ""
            });
        }

        public override string Header
        {
            get => $"#{FormattedNumber}";
        }

        public override string Subheader
        {
            get => $"{CompanyName}";
        }

        public override void MapAdditional()
        {
            if (this.ContactRoles != null)
            {
                var primary = this.ContactRoles.FirstOrDefault(x => x.RoleType == "Primary");
                if (primary != null)
                {
                    PrimaryContactID = (int)(primary.ContactID ?? 0);
                    PrimaryContactName = primary.ContactShortName ?? "";
                }
            }

            StatusText = this.OrderStatusName.ToLower();
            IsVoided = this.OrderStatusName.Contains("Voided");

            string nodashFormattedNumber = (this.FormattedNumber ?? "").Trim().StripDashes();
            Order = $"{this.Number} {nodashFormattedNumber ?? ""} {this.GetLinkedOrderSegment()}".ToLower();

            OrderStatusStart = this.OrderStatusStartDT.ToString("o");

            var createdDate = this.Dates?.FirstOrDefault(x => x.KeyDateType == "Created");


            if (createdDate != null)
                CreatedDate = createdDate.KeyDT.ToString("o");


            ContactRolesNames = this.GetLinkedContactSegment();
            EmployeeRolesIDs = this.GetLinkedEmployeeIDSegment();

            this.PriceTotal = this.PriceTotal.HasValue ? Math.Abs(this.PriceTotal.Value) : this.PriceTotal;
            this.PriceProductTotal = this.PriceProductTotal;
            this.PriceDiscount = this.PriceDiscount;
            this.PricePreTax = this.PricePreTax;
            this.PriceTax = this.PriceTax;
            this.PaymentTotal = this.PaymentTotal;
            this.PaymentBalanceDue = this.PaymentBalanceDue;

            Description = this.Description;
            FormattedNumber = this.FormattedNumber;
            FormattedNumberSort = nodashFormattedNumber.ToLower();
            StatusIDSort = StatusText;
            PriceTotalSort = Convert.ToDouble(this.PriceTotal);
            PaymentBalanceDueSort = Convert.ToDouble(this.PaymentBalanceDue);
            CreatedDateSort = createdDate?.KeyDT.Ticks ?? 0;
            this.LocationNameSort = (this.LocationName ?? "").Trim().ToLower();
            this.PrimaryContactNameSort = (this.PrimaryContactName ?? "").Trim().ToLower();
            this.IDSort = (this.ID.ToString() ?? "").ToLower();
            this.CompanyNameSort = (this.CompanyName ?? "").Trim().ToLower();
            this.TransactionType = this.TransactionType;
            this.Number = this.Number;
            this.BID = this.BID;
        }

        private string GetLinkedOrderSegment()
        {
            return GetSelectedSegment(this.Links?.Select(x => x.LinkedFormattedNumber));
        }

        private string GetLinkedContactSegment()
        {
            return GetSelectedSegment(this.ContactRoles?.Select(x => x.ContactShortName ?? ""));
        }

        private string GetLinkedEmployeeIDSegment()
        {
            return GetSelectedSegment(this.EmployeeRoles?.Select(x => x.EmployeeID.ToString()));
        }
    }
}
