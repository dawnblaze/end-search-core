﻿using Endor.Search.Core.Interfaces;
using System.Collections.Generic;

namespace Endor.Search.Core.Models
{
    /// <summary>
    /// A class for materializing search results when you don't know what type you are serializing
    /// </summary>
    public class WildcardSearchModel : BaseSearchModel, IAutomappableSearchModel<WildcardSearchModel>
    {
        public string[] IncludesForMap => null;

        public override void FillExtendedProperties()
        {
            Detail = null;
            Summary = null;
        }

        // This method is used to set up additional mapping between data records and child objects
        public void MapAdditional()
        {

        }

        public void Map()
        {
            this.MapAdditional();
            this.FillExtendedProperties();
        }
    }
}
