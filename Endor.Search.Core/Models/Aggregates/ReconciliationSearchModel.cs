﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class ReconciliationSearchModel : BaseAutomappableModel<ReconciliationSearchModel>
    {
        #region sorting fields

        // ID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string IDSort { get; set; }

        // ID - formatted
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string FormattedNumberSort { get; set; }

        // Location ID
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string LocationNameSort { get; set; }

        // AccountingDT
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string AccountingDTTextSort { get; set; }

        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.LocationTableName, "Name", "LocationID", ChildObjectTableInfo.IDColumn, "location")]
        public string LocationName { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int LocationID { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public DateTime CreatedDT { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public DateTime AccountingDT { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string AccountingDTText { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public DateTime LastAccountingDT { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public bool? IsAdjustmentEntry { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EmployeeDataTableName, "ShortName", "EnteredByID", ChildObjectTableInfo.IDColumn, "employee")]
        public string EnteredByName { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string FormattedNumber { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public int AdjustedReconciliationID { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public DateTime ExportedDT { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string ExportedDTText { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public DateTime SyncedDT { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string SyncedDTText { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public decimal TotalIncome { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public decimal TotalPayments { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsEmpty { get; set; }

        private string AccountingDTTextSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.AccountingDTText) ? "" : $"AccountingDT: {this.AccountingDTText}";
            }
        }

        public override void FillExtendedProperties()
        {
            Summary = $"{FormattedNumber} {IsAdjustmentEntry} {LocationName} {AccountingDTTextSearchFragment} {TotalIncome}";
            Detail = Summary;
        }

        public override string Header
        {
            get => $"{FormattedNumber}";
        }

        public override string Subheader
        {
            get => $"{LocationName}";
        }

        public override void MapAdditional()
        {
            ExportedDTText = this.ExportedDT.ToString("MM/dd/yyyy - hh:mm tt");
            SyncedDTText = this.SyncedDT.ToString("MM/dd/yyyy - hh:mm tt");
            AccountingDTText = this.AccountingDT.ToString("MM/dd/yyyy - hh:mm tt");
            this.EnteredByName = ""; // NOTE: no idea what this is for

            this.IDSort = (this.ID.ToString() ?? "").ToLower();
            this.FormattedNumberSort = (this.FormattedNumber ?? "").Trim().ToLower();
            this.LocationNameSort = (this.LocationName ?? "").Trim().ToLower();
            this.AccountingDTTextSort = (this.AccountingDTText ?? "").Trim().ToLower();
        }
    }
}
