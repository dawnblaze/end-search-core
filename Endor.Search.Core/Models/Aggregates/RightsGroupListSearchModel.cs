﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class RightsGroupListSearchModel : BaseAutomappableModel<RightsGroupListSearchModel>
    {
        #region sorting fields
        //name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        //typeSort
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string TypeSort { get; set; }

        //assignedEmployeesSort
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string AssignedEmployeesSort { get; set; }
        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Type { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string AssignedEmployees { get; set; }

        [Luke(Index = Index.NO, Store = Store.NO)]
        [JsonIgnore]
        public bool IsUserSpecific { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.UserLinkTableName, "RightsGroupListID")]
        public ICollection<UserLink> UserLinks { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{this.Name} {this.Type} {this.AssignedEmployees}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
           get => string.Empty;
        }

        public override void MapAdditional()
        {
            this.Type = this.IsUserSpecific ? "User Specific" : "Shared";
            this.AssignedEmployees = GetCommaSegment(this.UserLinks?.Where(l => !String.IsNullOrWhiteSpace(l.EmployeeName)).Select(l => l?.EmployeeName));

            this.NameSort = (this.Name ?? "").Trim().ToLower();
            this.TypeSort = (this.Type ?? "").Trim().ToLower();
            this.AssignedEmployeesSort = (this.AssignedEmployees ?? "").Trim().ToLower();
        }
    }
}
