﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class QuickItemSearchModel : BaseAutomappableModel<QuickItemSearchModel>
    {
        #region sorting fields

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string CompanyNameSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string EmployeeNameSort { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public byte LineItemCountSort { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string CategoryListSort { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public byte IsSharedSort { get; set; }

        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public int? CompanyID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.CompanyDataTableName, "Name", "CompanyID", ChildObjectTableInfo.IDColumn, "location")]
        public string CompanyName { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsGlobalItem { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public short? EmployeeID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EmployeeDataTableName, "LongName", "EmployeeID", ChildObjectTableInfo.IDColumn, "employee")]
        public string EmployeeName { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string CategoryList { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        public byte LineItemCount { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsShared { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.PartQuickItemCategoryLinkTableName, "QuickItemID", "CategoryID", ChildObjectTableInfo.FlatListDataTableName, ChildObjectTableInfo.IDColumn)]
        public ICollection<CategoryLink> CategoryLinks { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{Name} {CompanyName} {EmployeeName} {CategoryList}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get =>  "";
        }

        public override void MapAdditional()
        {
            if (this.CategoryLinks != null)
                this.CategoryList = String.Join(", ", this.CategoryLinks.Select(a => a.Name ?? ""));

            this.NameSort = (this?.Name ?? "").Trim().ToLower();
            this.CompanyNameSort = (this.CompanyName ?? "").Trim().ToLower();
            this.EmployeeNameSort = (this.EmployeeName ?? "").Trim().ToLower();
            this.IsSharedSort = (byte)(this.IsShared ? 1 : 0);
            this.CategoryListSort = (this.CategoryList ?? "").ToLower();
            this.LineItemCountSort = (byte)(this.LineItemCount);
        }
    }
}
