﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;


namespace Endor.Search.Core.Models
{
    public class DomainEmailSearchModel : BaseAutomappableModel<DomainEmailSearchModel>
    {
        #region sorting fields
        //DomainName
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string DomainNameSort { get; set; }

        //Provider
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string ProviderSort { get; set; }

        //Locations
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string LocationsSort { get; set; }
        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string DomainName { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Provider { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Locations { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [DBIgnore]
        public string LocationIDs { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.NO, Store = Store.NO)]
        public string SMTPAddress { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsForAllLocations { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }


        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.DomainEmailLocationLinkTableName, "DomainID", "LocationID", ChildObjectTableInfo.LocationTableName, ChildObjectTableInfo.IDColumn)]
        public ICollection<DomainEmailLocationLink> LocationLinks { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{DomainName} {Provider} {Locations} {SMTPAddress}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.DomainName;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            if (this?.LocationLinks?.Count > 0)
            {
                Locations = String.Join(", ",
                    this.LocationLinks.Select(x => x.Name));

                LocationIDs = String.Join(", ",
                    this.LocationLinks.Select(x => x.ID));
            }

            this.DomainNameSort = (this.DomainName ?? "").Trim().ToLower();
            this.ProviderSort = (this.Provider ?? "").Trim().ToLower();
            this.LocationsSort = (Locations ?? "").Trim().ToLower();
        }
    }
}
