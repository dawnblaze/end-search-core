﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.Helper_Classes;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{ 
    public class EstimateDataSearchModel : TransactionHeaderDataSearchModel<EstimateDataSearchModel>
    {
        #region sorting fields

        //ProductionDue
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public long ProductionDueDateSort { get; set; }

        //FollowUpData
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public long FollowUpDataSort { get; set; }

        #endregion


        [Luke(Index = Index.NO, Store = Store.YES)]
        [DBIgnore]
        public string ProductionDueDate { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        [DBIgnore]
        public string FollowUpDate { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public bool IsApproved { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public bool IsLost { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [DBIgnore]
        public static string AdditionalSqlFilter => "EstimateData.TransactionType = 1";

        public override void FillExtendedProperties()
        {
            Summary = GenerateSearchSummary();
            Detail = Summary;
        }
        private string GenerateSearchSummary()
        {
            return String.Join(" ", new string[] {
                    (this.FormattedNumber ?? "").StripDashes(),
                    this.CompanyName ?? "",
                    this.Description ?? "",
                    this.OrderStatusStart ?? "",
                    this.ProductionDueDate ?? "",
                    this.StatusText ?? "",
                    this.LocationName ?? "",
                    this.PrimaryContactName ?? "",
                    this.PriceTotal.HasValue ? this.PriceTotal.ToString() : ""
                });
        }

        public override void MapAdditional()
        {
            base.MapAdditional();

            var keyDate = this.Dates?.FirstOrDefault(x => x.KeyDateType == "Production Due");
            var followUpDate = this.Dates?.FirstOrDefault(x => x.KeyDateType == "Followup");

            if (keyDate != null)
                ProductionDueDate = keyDate.KeyDT.ToString("o");

            if (followUpDate != null)
                this.FollowUpDate = followUpDate.KeyDT.ToString("o");

            IsApproved =
                this.OrderStatusName.Contains("Approved");

            IsLost =
                this.OrderStatusName.Contains("Lost");

            ProductionDueDateSort = keyDate?.KeyDT.Ticks ?? 0;
            FollowUpDataSort = followUpDate?.KeyDT.Ticks ?? 0;
        }
    }
}
