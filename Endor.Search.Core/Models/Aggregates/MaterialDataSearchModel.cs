﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class MaterialDataSearchModel : BaseAutomappableModel<MaterialDataSearchModel>
    {
        #region sorting fields
        //name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        //description
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string DescriptionSort { get; set; }

        //materialcategories
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string MaterialCategoriesSort { get; set; }
        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string SKU { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public int IncomeAccountID { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public int ExpenseAccountID { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string Categories { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.PartMaterialCategoryLinkTableName, "PartID", "CategoryID", ChildObjectTableInfo.MaterialCategoryTableName, ChildObjectTableInfo.IDColumn)]
        public ICollection<LaborCategoryLink> MaterialCategoryLinks { get; set; } //For this purpose LaborCategory looks the same

        public override void FillExtendedProperties()
        {
            Summary = $"{Name} {Description} {Categories}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            this.NameSort = (this.Name ?? "").Trim().ToLower();
            this.DescriptionSort = (this.Description ?? "").Trim().ToLower();

            if (this.MaterialCategoryLinks != null)
            {
                this.Categories = String.Join(", ", this.MaterialCategoryLinks?.Select(t => t.Name));// SelectMany<MaterialCategory>(t=>t.MaterialCategory).Select()
            }
            else
            {
                this.Categories = "";
            }

            this.MaterialCategoriesSort = (this.Categories ?? "").Trim().ToLower();
        }
    }
}
