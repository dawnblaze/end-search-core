﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;
namespace Endor.Search.Core.Models
{
    public partial class AssemblyDataSearchModel : BaseAutomappableModel<AssemblyDataSearchModel>
    {

        #region sorting fields

        //name
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string NameSort { get; set; }

        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string AssemblyCategoriesSort { get; set; }

        //Description
        [JsonIgnore]
        [DBIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        public string DescriptionSort { get; set; }

        #endregion sorting fields


        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public bool IsActive { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string SKU { get; set; }

        [Luke(Index = Index.NO, Store = Store.YES)]
        [DBIgnore]
        public string LinkedAssemblyIDs { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public byte AssemblyType { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string AssemblyCategoryNames { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.SubassemblyVariableTableName, "SubassemblyID")]
        public ICollection<AssemblyVariable> Variables { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.AssemblyCategoryLinkTableName, "PartID", "CategoryID", ChildObjectTableInfo.AssemblyCategoryTableName, ChildObjectTableInfo.IDColumn)]
        public ICollection<AssemblyCategoryLink> AssemblyCategories { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{Name} {AssemblyCategoryNames}";
            Detail = Summary;
        }

        public override string Header
        {
            get => Name;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            this.LinkedAssemblyIDs = null;
            if (this.Variables != null)
            {
                var ids = this.Variables
                            .Where(v => v.LinkedSubassemblyID != null)
                            .Select(v => v.LinkedSubassemblyID.Value.ToString())
                            .ToList();

                this.LinkedAssemblyIDs = String.Join(",", ids);
            }
            if (this.AssemblyCategories != null)
                this.AssemblyCategoryNames = String.Join(", ", this.AssemblyCategories.Select(t => t.Name));

            this.NameSort = (this?.Name ?? "").Trim().ToLower();
            this.DescriptionSort = (this.Description ?? "").Trim().ToLower();
            this.AssemblyCategoriesSort = (this.AssemblyCategoryNames ?? "").ToLower();
        }
    }
}
