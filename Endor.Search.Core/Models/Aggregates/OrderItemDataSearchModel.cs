﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class OrderItemDataSearchModel : BaseAutomappableModel<OrderItemDataSearchModel>
    {
        #region sorting fields

        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string NameSort { get; set; }

        #endregion

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Name { get; set; }

        //exact match
        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string Description { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [MapDBField("ItemStatusID")]
        public short StatusID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public int OrderID { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string SurchargeList { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [LinkCollection(ChildObjectTableInfo.OrderItemSurchargeTableName, "OrderItemID", "SurchargeDefID", ChildObjectTableInfo.SurchargeDefTableName, ChildObjectTableInfo.IDColumn)]
        public ICollection<SurchargeDefLink> Surcharges { get; set; }

        [Luke(Store = Store.NO, Index = Core.SharedKernal.LukeMapper.Attributes.Index.NO)]
        [DBIgnore]
        public override bool HasImage { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [DBIgnore]
        public static string AdditionalSqlFilter => "OrderItemData.TransactionType != 8";

        public override void FillExtendedProperties()
        {
            Summary = $"{Name}";
            Detail = Summary;
        }


        public override string Header
        {
            get => this.Name;
        }

        public override string Subheader
        {
            get => "";
        }

        public override void MapAdditional()
        {
            this.NameSort = (this?.Name ?? "").Trim().ToLower();
            this.Description = this.Description;
            this.OrderID = this.OrderID;
            if (this.Surcharges != null)
                this.SurchargeList = String.Join(", ", this.Surcharges.Select(a => a.Name ?? ""));

        }
    }
}
