﻿using Endor.Search.Core.Enums;
using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using Index = Endor.Search.Core.SharedKernal.LukeMapper.Attributes.Index;

namespace Endor.Search.Core.Models
{
    public class ContactDataSearchModel : BaseAutomappableModel<ContactDataSearchModel>
    {
        #region sorting fields

        //name
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string NameSort { get; set; }

        //companies
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string CompanyNamesSort { get; set; }

        //email
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string EmailSort { get; set; }

        //primaryPhone
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string PrimaryPhoneSort { get; set; }

        //position
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string PositionsSort { get; set; }

        //contactType
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string ContactTypeSort { get; set; }

        //status
        [JsonIgnore]
        [Luke(Index = Index.NOT_ANALYZED)]
        [DBIgnore]
        public string StatusSort { get; set; }

        #endregion sorting fields

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public bool IsActive { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public bool IsCompanyAdHoc { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.YES)]
        public string StatusID { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [ParentFieldMap(ChildObjectTableInfo.EnumCRMCompanyStatusTableName, "Name", "StatusID", ChildObjectTableInfo.IDColumn, "companyStatus", true)]
        public string Status { get; set; }

        #region names

        [Luke(Index = Index.ANALYZED)]
        public string LongName { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [JsonIgnore]
        public string First { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [JsonIgnore]
        public string Middle { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [JsonIgnore]
        public string Last { get; set; }

        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [JsonIgnore]
        public string NickName { get; set; }

        #endregion

        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        [DBIgnore]
        public string EmailAddress { get; set; }

        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        [DBIgnore]
        public string PhoneNumber { get; set; }

        [Luke(Store = Store.YES, Index = Index.NO)]
        [DBIgnore]
        public string UserIDs { get; set; }

        [JsonIgnore]
        [Luke(Index = Index.ANALYZED, Store = Store.NO)]
        [DBIgnore]
        public string CompanyIDs { get; set; }

        [Luke(Index = Index.NOT_ANALYZED, Store = Store.YES)]
        [DBIgnore]
        public string CompanyNames { get; set; }

        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        [DBIgnore]
        public string Positions { get; set; }

        [Luke(Store = Store.YES, Index = Index.ANALYZED)]
        [DBIgnore]
        public string ContactTypes { get; set; }

        private string PhoneNumberSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.PhoneNumber) ? "" : "Ph:" + this.PhoneNumber;
            }
        }
        private string EmailAddressSearchFragment
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.EmailAddress) ? "" : "Email:" + this.EmailAddress;
            }
        }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.ContactLocatorTableName, "ParentID")]
        public ICollection<ContactLocator> ContactLocators { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [ChildCollection(ChildObjectTableInfo.UserLinkTableName, "ContactID")]
        public ICollection<UserLink> UserLinks { get; set; }

        [Luke(Store = Store.NO, Index = Index.NO)]
        [JsonIgnore]
        [CustomSql(" Select cl.ContactID as ParentID, com.Name as Name, com.IsAdHoc as IsAdHoc, com.ID as ID, cl.IsActive as IsActive, cl.IsPrimary as IsPrimary, cl.IsBilling as IsBilling, cl.Position as Position" +
            " from [dbo].[Company.Contact.Link] cl Inner Join  [dbo].[Company.Data] com on com.ID = cl.CompanyID" +
            " where cl.BID = @BID and cl.ContactID IN " +
            "(select ContactID FROM [dbo].[Company.Contact.Link] Where BID = @BID and CompanyID IN @IDs)"
            )]
        public ICollection<ContactCompanyLink> CompanyContactLinks { get; set; }

        public override void FillExtendedProperties()
        {
            Summary = $"{LongName} {CompanyNames} {Positions} {PhoneNumberSearchFragment} {EmailAddressSearchFragment}";
            Detail = Summary;
        }

        public override string Header
        {
            get => this.LongName;
        }

        public override string Subheader
        {
            get => "";
        }

        public override string Path => $"/sales/crm/contact/{ID}";

        public override void MapAdditional()
        {
            this.EmailAddress = this.ContactLocators?.OrderBy(x => x.SortIndex)?.FirstOrDefault(x => x.LocatorType == LocatorType.Email)?.Locator;
            this.PhoneNumber = this.ContactLocators?.OrderBy(x => x.SortIndex)?.FirstOrDefault(x => x.LocatorType == LocatorType.Phone)?.Locator;

            if (this.UserLinks != null)
                UserIDs = String.Join(",", this.UserLinks.Select(x => x.AuthUserID.ToString()).ToArray());

            this.IsActive = this.CompanyContactLinks?.Any(l => l.IsActive == true) ?? false;
            this.IsCompanyAdHoc = this.CompanyContactLinks?.FirstOrDefault()?.IsAdHoc ?? false;

            this.ContactTypes = "";
            if (this.CompanyContactLinks != null && this.CompanyContactLinks.Count > 0)
            {
                this.CompanyIDs = GetSelectedSegment(this.CompanyContactLinks.Select(l => l.ID.ToString()));
                this.Positions = GetCommaSegment(this.CompanyContactLinks.Select(l => l.Position ?? ""));

                var nonAdHocLinks = this.CompanyContactLinks.Where(l => !l.IsAdHoc).OrderByDescending(l => l.IsActive);
                if (nonAdHocLinks != null && nonAdHocLinks.Count() > 0)
                {
                    this.CompanyNames = GetCommaSegment(nonAdHocLinks.Select(l => l.Name ?? ""));

                    if (nonAdHocLinks.Any(c => c.IsPrimary == true) == true)
                        this.ContactTypes += "Primary";

                    if (nonAdHocLinks.Any(c => c.IsBilling == true) == true)
                        this.ContactTypes += (this.ContactTypes.Length > 0 ? ", Billing" : "Billing");
                }
            }

            this.CompanyNamesSort = (this.CompanyNames ?? "").Trim().ToLower();
            this.NameSort = (this.LongName ?? "").Trim().ToLower();
            this.EmailSort = (this.EmailAddress ?? "").Trim().ToLower();

            var phoneStr = this.PhoneNumber ?? "";
            this.PrimaryPhoneSort = CleanPhone(phoneStr);

            this.PositionsSort = (this.Positions ?? "").Trim().ToLower();
            this.ContactTypeSort = (this.ContactTypes ?? "").Trim().ToLower();
            this.StatusSort = (this.Status ?? "").Trim().ToLower();
        }

        private static Regex digitsOnly = new Regex(@"[^\d]");
        public static string CleanPhone(string phone)
        {
            return digitsOnly.Replace(phone, "");
        }
    }
}
