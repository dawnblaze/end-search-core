﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models.DTOs
{
    public class AddressMetaData
    {
        public string FormattedAddress { get; set; }
        public int TaxClassID { get; set; }
        public string PlaceID { get; set; }
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? VerifiedDate { get; set; }
        public string Website { get; set; }
        public string BusinessPhone { get; set; }
        public string PlaceName { get; set; }
        public string Long { get; set; }
        public string Lat { get; set; }
        public string Suburb { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Street2 { get; set; }
        public string Street1 { get; set; }
        public string IconURL { get; set; }
        public string UtcOffset { get; set; }
    }

    public class LocatorMetaData
    {
        public string CustomLabel { get; set; }
    }
}
