﻿using Endor.Search.Core.Interfaces;

namespace Endor.Search.Core.Models
{
    /// <summary>
    /// base model for listing of things in the list sidebar on the client
    /// This model should be created from a BaseSearchModel if needed.
    /// </summary>
    public class SimpleSearchModelDTO : ISimpleSearchModel
    {
        public SimpleSearchModelDTO() { }

        public SimpleSearchModelDTO(ISimpleSearchModel model)
        {
            ID = model.ID;
            Path = model.Path;
            Header = model.Header;
            Subheader = model.Subheader;
        }

        public int ID { get; private set; }

        public string Path { get; private set; }

        public string Header { get; private set; }

        public string Subheader { get; private set; }
    }
}