﻿using Endor.Search.Core.SharedKernal;
using System;

namespace Endor.Search.Core.Models
{
    public class UserLink
    {
        public short? EmployeeID { get; set; }
        [ParentFieldMap(ChildObjectTableInfo.EmployeeDataTableName, "ShortName", "EmployeeID", ChildObjectTableInfo.IDColumn, "employee")]
        public string EmployeeName { get; set; }
        public short? ContactID { get; set; }
        public int? AuthUserID { get; set; }
        public int? RightsGroupListID { get; set; }
        public string UserName { get; set; }
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get => 1012; set { } }
        public DateTime? LastConnectionDT { get; set; }
    }
}
