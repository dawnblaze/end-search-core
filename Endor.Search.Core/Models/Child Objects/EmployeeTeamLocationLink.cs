﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Search.Core.SharedKernal;

namespace Endor.Search.Core.Models
{
    public class EmployeeTeamLocationLink : Location
    {
        [DBIgnore]
        public int ParentID { get; set; }
    }
}
