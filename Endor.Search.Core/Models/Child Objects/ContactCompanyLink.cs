﻿using Endor.Search.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class ContactCompanyLink : Company
    {
        [DBIgnore]
        public int ParentID { get; set; }

        [DBIgnore]
        public bool IsActive { get; set; }
        [DBIgnore]
        public bool IsPrimary { get; set; }
        [DBIgnore]
        public bool IsBilling { get; set; }
        [DBIgnore]
        public string Position { get; set; }
    }
}
