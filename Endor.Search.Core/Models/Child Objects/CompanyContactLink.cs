﻿using Endor.Search.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class CompanyContactLink : Contact
    {
        [DBIgnore]
        public int ParentID { get; set; }
    }
}
