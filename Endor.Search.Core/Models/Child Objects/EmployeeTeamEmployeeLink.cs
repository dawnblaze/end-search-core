﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Search.Core.SharedKernal;

namespace Endor.Search.Core.Models
{
    public class EmployeeTeamEmployeeLink : Employee
    {
        [DBIgnore]
        public int ParentID { get; set; }
    }
}
