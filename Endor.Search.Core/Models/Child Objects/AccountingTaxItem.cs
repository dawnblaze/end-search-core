﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class AccountingTaxItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
