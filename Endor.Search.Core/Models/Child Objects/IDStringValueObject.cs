﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class IDStringValueObject
    {
        public int ID { get; set; }
        public string Value { get; set; }
    }
}
