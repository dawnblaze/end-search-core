﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class MessageParticipantInfo
    {
        public int ID { get; set; }
        public int BodyID { get; set; }
        public string UserName { get; set; }
    }
}
