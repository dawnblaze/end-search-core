﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class DestinationProfile
    {
        public int DestinationID { get; set; }
        public string Name { get; set; }
    }
}
