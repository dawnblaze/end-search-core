﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class AssemblyCategory
    {
        public string Name { get; set; }
    }
}
