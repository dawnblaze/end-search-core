﻿using Endor.Search.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class AlertDefinitionAction
    {
        public int AlertDefinitionID { get; set; }

        [ParentFieldMap(ChildObjectTableInfo.SystemAutomationActionDefinitionTableName, "Name", "ActionDefinitionID", ChildObjectTableInfo.IDColumn, "actiondefinitionid", true)]
        public string DefinitionName { get; set; }
    }
}
