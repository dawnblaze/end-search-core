﻿namespace Endor.Search.Core.Models
{
    public class ChildObjectTableInfo
    {
        public const string IDColumn = "ID";

        public const string AccountingGLAccountTableName = "[dbo].[Accounting.GL.Account]";
        public const string AccountingTaxCodeItemExemptionLinkTableName = "[dbo].[Accounting.Tax.Code.ItemExemptionLink]";
        public const string AccountingTaxCodeTableName = "[dbo].[Accounting.Tax.Code]";
        public const string AccountingTaxItemTableName = "[dbo].[Accounting.Tax.Item]";
        public const string AlertDefinitionTableName = "[dbo].[Alert.Definition]";
        public const string AlertDefinitionActionTableName = "[dbo].[Alert.Definition.Action]";
        public const string AssemblyDataTableName = "[dbo].[Part.Subassembly.Data]";
        public const string AssemblyCategoryLinkTableName = "[dbo].[Part.Subassembly.CategoryLink]";
        public const string AssemblyCategoryTableName = "[dbo].[Part.Subassembly.Category]";
        public const string BoardModuleLinkTableName = "[dbo].[Board.Module.Link]";
        public const string BoardDefinitionDataTableName = "[dbo].[Board.Definition.Data]";
        public const string CompanyContactLinkTableName = "[dbo].[Company.Contact.Link]";
        public const string CompanyDataTableName = "[dbo].[Company.Data]";
        public const string CompanyLocatorTableName = "[dbo].[Company.Locator]";
        public const string ContactDataTableName = "[dbo].[Contact.Data]";
        public const string ContactLocatorTableName = "[dbo].[Contact.Locator]";
        public const string CustomFieldDefinitionTableName = "[dbo].[CustomField.Definition]";
        public const string CustomFieldLayoutDefinitionTableName = "[dbo].[CustomField.Layout.Definition]";
        public const string CrmIndustryTableName = "[dbo].[CRM.Industry]";
        public const string CrmOriginTableName = "[dbo].[CRM.Origin]";
        public const string DashboardDataTableName = "[dbo].[Dashboard.Data]";
        public const string DashboardWidgetDefinitionTableName = "[dbo].[System.Dashboard.Widget.Definition]";
        public const string DestinationDataTableName = "[dbo].[Destination.Data]";
        public const string DestinationProfileTableName = "[dbo].[Destination.Profile]";
        public const string DomainEmailAccountTableName = "[dbo].[Domain.Email.Data]";
        public const string DomainEmailLocationLinkTableName = "[dbo].[Domain.Email.LocationLink]";
        public const string EmailAccountTableName = "[dbo].[Email.Account.Data]";
        public const string EmailAccountTeamLinkTableName = "[dbo].[Email.Account.TeamLink]";
        public const string EmailAccountTeamLinkEmailAccountIDColumn = "EmailAccountID";
        public const string EmployeeDataTableName = "[dbo].[Employee.Data]";
        public const string EmployeeLocatorTableName = "[dbo].[Employee.Locator]";
        public const string EmployeeTeamEmployeeLinkTableName = "[dbo].[Employee.TeamLink]";
        public const string EmployeeTeamLocationLinkTableName = "[dbo].[Employee.Team.LocationLink]";
        public const string EmployeeTeamTableName = "[dbo].[Employee.Team]";
        public const string EnumAutomationTriggerCategoryTypeTableName = "[dbo].[enum.Automation.Trigger.CategoryType]";
        public const string EnumClassTypeTableName = "[dbo].[enum.ClassType]";
        public const string EnumCRMCompanyStatusTableName = "[dbo].[enum.CRM.Company.Status]";
        public const string EnumDataTypeTableName = "[dbo].[enum.DataType]";
        public const string EnumDomainAccessTypeTableName = "[dbo].[enum.DomainAccessType]";
        public const string EnumDomainStatusTableName = "[dbo].[enum.DomainStatus]";
        public const string EnumEmailAccountStatusTableName = "[dbo].[enum.EmailAccountStatus]";
        public const string EnumMessageChannelTypeTableName = "[dbo].[enum.Message.ChannelType]";
        public const string EnumModuleTableName = "[dbo].[enum.Module]";
        public const string EnumOrderContactRoleTypeTableName = "[dbo].[enum.Order.ContactRoleType]";
        public const string EnumOrderKeyDateTypeTableName = "[dbo].[enum.Order.KeyDateType]";
        public const string EnumOrderOrderStatusTableName = "[dbo].[enum.Order.OrderStatus]";
        public const string EnumPaymentTransactionTypeTableName = "[dbo].[enum.Accounting.PaymentTransactionType]";
        public const string FlatListDataTableName = "[dbo].[List.FlatList.Data]";
        public const string GLActivityTableName = "[dbo].[Activity.GLActivity]";
        public const string LaborCategoryTableName = "[dbo].[Part.Labor.Category]";
        public const string LocationTableName = "[dbo].[Location.Data]";
        public const string MachineCategoryTableName = "[dbo].[Part.Machine.Category]";
        public const string MachineInstanceTableName = "[dbo].[Part.Machine.Instance]";
        public const string MachineProfileTableName = "[dbo].[Part.Machine.Profile]";
        public const string MachineProfileTableTableName = "[dbo].[Part.Machine.Profile.Table]";
        public const string MaterialCategoryTableName = "[dbo].[Part.Material.Category]";
        public const string MessageBodyTableName = "[dbo].[Message.Body]";
        public const string MessageParticipantInfoTableName = "[dbo].[Message.Participant.Info]";
        public const string OrderContactRoleTableName = "[dbo].[Order.Contact.Role]";
        public const string OrderDataTableName = "[dbo].[Order.Data]";
        public const string OrderEmployeeRoleTableName = "[dbo].[Order.Employee.Role]";
        public const string OrderItemComponentTableName = "[dbo].[Order.Item.Component]";
        public const string OrderItemSurchargeTableName = "[dbo].[Order.Item.Surcharge]";
        public const string OrderKeyDateTableName = "[dbo].[Order.KeyDate]";
        public const string OrderOrderLinkTableName = "[dbo].[Order.OrderLink]";
        public const string PartLaborCategoryLinkTableName = "[dbo].[Part.Labor.CategoryLink]";
        public const string PartLaborDataTableName = "[dbo].[Part.Labor.Data]";
        public const string PartMachineCategoryLinkTableName = "[dbo].[Part.Machine.CategoryLink]";
        public const string PartMachineDataTableName = "[dbo].[Part.Machine.Data]";
        public const string PartMaterialCategoryLinkTableName = "[dbo].[Part.Material.CategoryLink]";
        public const string PartMaterialDataTableName = "[dbo].[Part.Material.Data]";
        public const string PartQuickItemCategoryLinkTableName = "[dbo].[Part.QuickItem.CategoryLink]";
        public const string PaymentApplicationTableName = "[dbo].[Accounting.Payment.Application]";
        public const string PaymentMethodTypeTableName = "[dbo].[Accounting.Payment.Method]";
        public const string SSLCertificateData = "[dbo].[SSLCertificate.Data]";
        public const string SubassemblyVariableTableName = "[dbo].[Part.Subassembly.Variable]";
        public const string SurchargeDefTableName = "[dbo].[Part.Surcharge.Data]";
        public const string SystemActionDefinitionTableName = "[dbo].[System.Automation.Action.Definition]";
        public const string SystemAutomationActionDefinitionTableName = "[dbo].[System.Automation.Action.Definition]";
        public const string SystemAutomationTriggerDefinitionTableName = "[dbo].[System.Automation.Trigger.Definition]";
        public const string SystemMessageTemplateTypeTableName = "[dbo].[System.Message.TemplateType]";
        public const string UserDraftTableName = "[dbo].[User.Draft]";
        public const string UserLinkTableName = "[dbo].[User.Link]";



        


    }

}
