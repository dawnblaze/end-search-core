﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class AssemblyVariable
    {
        public int SubassemblyID { get; set; }
        public int? LinkedSubassemblyID { get; set; }
    }
}
