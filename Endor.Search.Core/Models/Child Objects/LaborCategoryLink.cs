﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class LaborCategoryLink : LaborCategory
    {
        public int ParentID { get; set; }
    }
}
