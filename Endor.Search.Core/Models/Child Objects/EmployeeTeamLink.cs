﻿using Endor.Search.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class EmployeeTeamLink : EmployeeTeam
    {
        [DBIgnore]
        public int ParentID { get; set; }
    }
}
