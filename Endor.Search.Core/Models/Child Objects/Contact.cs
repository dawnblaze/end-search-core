﻿using Endor.Search.Core.SharedKernal;
using Endor.Search.Core.SharedKernal.LukeMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class Contact
    {
        public int ID { get; set; }
        public string LongName { get; set; }
    }
}
