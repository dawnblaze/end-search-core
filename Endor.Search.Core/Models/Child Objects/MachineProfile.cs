﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class MachineProfile
    {
        public int MachineID { get; set; }
        public string Name { get; set; }
    }
}
