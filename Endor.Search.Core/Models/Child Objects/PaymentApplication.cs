﻿using Endor.Search.Core.Models;
using Endor.Search.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class PaymentApplication
    {
        public int ID { get; set; }
        public int MasterID { get; set; }
        public int PaymentMethodID { get; set; }
        [ParentFieldMap(ChildObjectTableInfo.PaymentMethodTypeTableName, "Name", "PaymentMethodID", ChildObjectTableInfo.IDColumn, "paymentmethod")]
        public string PaymentMethodText { get; set; }
        [ParentFieldMap(ChildObjectTableInfo.OrderDataTableName, "FormattedNumber", "OrderID", ChildObjectTableInfo.IDColumn, "orderdata")]
        public string FormattedNumber { get; set; }
        public int OrderID { get; set; }
        public decimal Amount { get; set; }
    }
}
