﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class OrderEmployeeRole
    {
        public int OrderID { get; set; }
        public int EmployeeID { get; set; }
    }
}
