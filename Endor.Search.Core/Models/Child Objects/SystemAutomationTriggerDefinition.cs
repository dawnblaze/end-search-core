﻿using Endor.Search.Core.Models;
using Endor.Search.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class SystemAutomationTriggerDefinition
    {
        public string Name { get; set; }

        public int ID { get; set; }

        [ParentFieldMap(ChildObjectTableInfo.EnumAutomationTriggerCategoryTypeTableName, "Name", "TriggerCategoryType", ChildObjectTableInfo.IDColumn, "triggercategorytype", true)]
        public string TriggerCategoryType { get; set; }
    }
}
