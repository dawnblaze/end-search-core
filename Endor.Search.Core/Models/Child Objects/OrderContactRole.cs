﻿using Endor.Search.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class OrderContactRole
    {
        public int OrderID { get; set; }

        [ParentFieldMap(ChildObjectTableInfo.EnumOrderContactRoleTypeTableName, "Name", "RoleType", ChildObjectTableInfo.IDColumn, "orderroletype", true)]
        public string RoleType { get; set; }

        public int? ContactID { get; set; }
        [ParentFieldMap(ChildObjectTableInfo.ContactDataTableName, "ShortName", "ContactID", ChildObjectTableInfo.IDColumn, "contact")]
        public string ContactShortName { get; set; }
    }
}
