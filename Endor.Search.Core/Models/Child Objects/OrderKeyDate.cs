﻿using Endor.Search.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class OrderKeyDate
    {
        public int OrderID { get; set; }

        [ParentFieldMap(ChildObjectTableInfo.EnumOrderContactRoleTypeTableName, "Name", "KeyDateType", ChildObjectTableInfo.IDColumn, "orderroletype", true)]
        public string KeyDateType { get; set; }

        public DateTime KeyDT { get; set; }
    }
}
