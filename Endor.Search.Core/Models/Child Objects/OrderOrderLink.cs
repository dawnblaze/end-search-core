﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Models
{
    public class OrderOrderLink
    {
        public int OrderID { get; set; }
        public string LinkedFormattedNumber { get; set; }
    }
}
