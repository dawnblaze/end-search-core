﻿using System;
using System.Linq.Expressions;


namespace Endor.Search.Core.SharedKernal
{
    internal sealed class IdentityCriteria<T> : Criteria<T>
    {
        public override Expression<Func<T, bool>> ToExpression()
        {
            return x => true;
        }
    }

    public abstract class Criteria<T>
    {

        public static readonly Criteria<T> All = new IdentityCriteria<T>();
        public abstract Expression<Func<T, bool>> ToExpression();


        /// <summary>  
        /// Determines if the given entitiy matches the specification
        /// </summary>  
        public bool IsSatisfiedBy(T entity)
        {
            Func<T, bool> predicate = ToExpression().Compile();
            return predicate(entity);
        }


        public Criteria<T> And(Criteria<T> Criteria)
        {
            return new AndCriteria<T>(this, Criteria);
        }


        public Criteria<T> Or(Criteria<T> Criteria)
        {
            return new OrCriteria<T>(this, Criteria);
        }
    }


    public class AndCriteria<T> : Criteria<T>
    {
        private readonly Criteria<T> _left;
        private readonly Criteria<T> _right;


        public AndCriteria(Criteria<T> left, Criteria<T> right)
        {
            _right = right;
            _left = left;
        }


        public override Expression<Func<T, bool>> ToExpression()
        {
            Expression<Func<T, bool>> leftExpression = _left.ToExpression();
            Expression<Func<T, bool>> rightExpression = _right.ToExpression();

            var paramExpr = Expression.Parameter(typeof(T));
            var exprBody = Expression.AndAlso(leftExpression.Body, rightExpression.Body);
            exprBody = (BinaryExpression)new ParameterReplacer(paramExpr).Visit(exprBody);
            var finalExpr = Expression.Lambda<Func<T, bool>>(exprBody, paramExpr);

            return finalExpr;
        }
    }

    public class OrCriteria<T> : Criteria<T>
    {
        private readonly Criteria<T> _left;
        private readonly Criteria<T> _right;


        public OrCriteria(Criteria<T> left, Criteria<T> right)
        {
            _right = right;
            _left = left;
        }


        public override Expression<Func<T, bool>> ToExpression()
        {
            var leftExpression = _left.ToExpression();
            var rightExpression = _right.ToExpression();
            var paramExpr = Expression.Parameter(typeof(T));
            var exprBody = Expression.OrElse(leftExpression.Body, rightExpression.Body);
            exprBody = (BinaryExpression)new ParameterReplacer(paramExpr).Visit(exprBody);
            var finalExpr = Expression.Lambda<Func<T, bool>>(exprBody, paramExpr);

            return finalExpr;
        }
    }

    internal class ParameterReplacer : ExpressionVisitor
    {

        private readonly ParameterExpression _parameter;

        protected override Expression VisitParameter(ParameterExpression node)
            => base.VisitParameter(_parameter);

        internal ParameterReplacer(ParameterExpression parameter)
        {
            _parameter = parameter;
        }
    }
}
