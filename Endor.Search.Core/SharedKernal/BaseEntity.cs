﻿using Endor.Search.Core.Interfaces;
using System.Text.Json.Serialization;

namespace Endor.Search.Core.SharedKernal
{
    public class BaseEntity : IEntity
    {
        [JsonIgnore]
        public short BID { get; set; }
    }
}
