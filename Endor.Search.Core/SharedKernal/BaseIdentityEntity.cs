﻿using Endor.Search.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Endor.Search.Core.SharedKernal
{
    public abstract class BaseIdentityEntity : BaseEntity, IAtom
    {
        public int ID { get; set; }
        public abstract int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        [JsonIgnore]
        [NotMapped]
        public short IDAsShort
        {
            get => (short)ID;
            set => ID = value;
        }
        [JsonIgnore]
        [NotMapped]
        public byte IDAsByte
        {
            get => (byte)ID;
            set => ID = value;
        }

        public virtual IEnumerable<string> IncludeDefaults<TEntity>()
            where TEntity : class
        {
            return new string[0];
        }
    }
}
