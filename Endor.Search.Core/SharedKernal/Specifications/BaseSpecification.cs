﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Endor.Search.Core.Interfaces;

namespace Endor.Search.Core.SharedKernal
{
    public abstract class BaseSpecification<T> : ISpecification<T>
    {
        //protected BaseSpecification(Expression<Func<T, bool>> criteria)
        protected BaseSpecification()
        {
            Criteria = Criteria<T>.All;
        }
        //public Expression<Func<T, bool>> Criteria { get; }
        public Criteria<T> Criteria { get; private set; }
        public IEnumerable<Expression<Func<T, object>>> Includes { get; } = new List<Expression<Func<T, object>>>();
        public IEnumerable<string> IncludeStrings { get; } = new List<string>();
        public Expression<Func<T, object>> OrderBy { get; private set; }
        public Expression<Func<T, object>> OrderByDescending { get; private set; }
        public Expression<Func<T, object>> GroupBy { get; private set; }

        public int Take { get; private set; }
        public int Skip { get; private set; }
        public bool IsPagingEnabled { get; private set; } = false;

        public string CacheKey { get; protected set; }
        public bool CacheEnabled { get; private set; }

        /// <summary>  
        /// Add a criteria to a specification - similiar to a where clause
        /// </summary>  
        public virtual void AddCriteria(Criteria<T> criteria)
        {
            Criteria = Criteria.And(criteria);
        }

        /// <summary>  
        /// Add an include to retrieve with the object
        /// </summary>  
        protected virtual void AddInclude(Expression<Func<T, object>> includeExpression)
        {
            ((List<Expression<Func<T, object>>>)Includes).Add(includeExpression);
        }

        /// <summary>  
        /// Add an include to retrieve with the object
        /// </summary>  
        protected virtual void AddInclude(string includeString)
        {
            ((List<string>)IncludeStrings).Add(includeString);
        }

        /// <summary>  
        /// Specify paging to apply
        /// </summary>  
        protected virtual void ApplyPaging(int skip, int take)
        {
            Skip = skip;
            Take = take;
            IsPagingEnabled = true;
        }

        /// <summary>  
        /// Specify an order by for the returned objects
        /// </summary>  
        protected virtual void ApplyOrderBy(Expression<Func<T, object>> orderByExpression)
        {
            OrderBy = orderByExpression;
        }

        /// <summary>  
        /// Specify a descending order
        /// </summary>  
        protected virtual void ApplyOrderByDescending(Expression<Func<T, object>> orderByDescendingExpression)
        {
            OrderByDescending = orderByDescendingExpression;
        }

        /// <summary>  
        /// Group by for specification
        /// </summary>  
        protected virtual void ApplyGroupBy(Expression<Func<T, object>> groupByExpression)
        {
            GroupBy = groupByExpression;
        }
    }
}
