﻿namespace Endor.Search.Core.SharedKernal
{
    /// <summary>
    /// Retrieve all messages by channgel type and employee id (optional)
    /// </summary>
    public class FilterByBIDSpecification<T> : BaseSpecification<T> where T : BaseEntity
    {
        public FilterByBIDSpecification(short bid)
            : base()
        {
            AddCriteria(new FilterByBIDCriteria<T>(bid));
        }
    }
}
