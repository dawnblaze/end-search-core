﻿using System;
using System.Linq.Expressions;

namespace Endor.Search.Core.SharedKernal
{
    /// <summary>
    /// Basic Filter to filter on BID value
    /// </summary>
    /// <typeparam name="T">BaseEntity</typeparam>
    public class FilterByBIDCriteria<T> : Criteria<T> where T : BaseEntity
    {
        private readonly short _bid;
        public FilterByBIDCriteria(short bid)
        {
            _bid = bid;
        }
        public override Expression<Func<T, bool>> ToExpression()
        {
            return ul => ul.BID == _bid;
        }
    }
}
