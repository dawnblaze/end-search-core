﻿using Endor.Search.Core.Models;
using System;
using System.Linq;

namespace Endor.Search.Core
{

    public class ClassTypeIDMap
    {
        public ClassTypeIDMap(int classtypeid, System.Type classtype, System.Type dbIDType = null, string tablename = "")
        {
            ClassTypeID = classtypeid;
            ClassType = classtype;
            DbIDType = dbIDType ?? typeof(int);
            TableName = tablename;
            ClassName = classtype.Name;
        }

        public int ClassTypeID;
        public System.Type ClassType;
        public System.Type DbIDType;
        public string TableName;
        public string ClassName;

        public static readonly ClassTypeIDMap[] Map =
        {
            new ClassTypeIDMap(1005, typeof(LocationDataSearchModel), typeof(short), "dbo.[Location.Data]"),
            new ClassTypeIDMap(1012, typeof(UserLink), typeof(short), "dbo.[User.Link]"),
            new ClassTypeIDMap(1021, typeof(DomainDataSearchModel), typeof(int), "dbo.[Domain.Data]"),
            new ClassTypeIDMap(1022, typeof(DomainEmailSearchModel), typeof(int), "dbo.[Domain.Email.Data]"),
            new ClassTypeIDMap(1023, typeof(EmailAccountDataSearchModel), typeof(int), "dbo.[Email.Account.Data]"),
            new ClassTypeIDMap(1050, typeof(UserDraftSearchModel), typeof(int), "dbo.[User.Draft]"),
            new ClassTypeIDMap(1100, typeof(RightsGroupListSearchModel), typeof(int), "dbo.[Rights.Group.List]"),
            new ClassTypeIDMap(2000, typeof(CompanyDataSearchModel), typeof(int), "dbo.[Company.Data]"),
            new ClassTypeIDMap(2011, typeof(CrmIndustrySearchModel), typeof(int), "dbo.[CRM.Industry]"),
            new ClassTypeIDMap(2011, typeof(CrmOriginSearchModel), typeof(int), "dbo.[CRM.Origin]"),
            new ClassTypeIDMap(3000, typeof(ContactDataSearchModel), typeof(int), "dbo.[Contact.Data]"),
            new ClassTypeIDMap(5000, typeof(EmployeeDataSearchModel), typeof(short), "dbo.[Employee.Data]"),
            new ClassTypeIDMap(5003, typeof(EmployeeLocator), typeof(int), "dbo.[Employee.Locator]"),
            new ClassTypeIDMap(5020, typeof(EmployeeTeamSearchModel), typeof(short), "dbo.[Employee.Team]"),
            new ClassTypeIDMap(8005, typeof(TaxabilityCodeSearchModel), typeof(int), "dbo.[Accounting.Tax.Code]"),
            new ClassTypeIDMap(8010, typeof(PaymentMasterDataSearchModel), typeof(int), "dbo.[Accounting.Payment.Master]"),
            new ClassTypeIDMap(8011, typeof(PaymentApplicationDataSearchModel), typeof(int), "dbo.[Accounting.Payment.Application]"),
            new ClassTypeIDMap(8015, typeof(ReconciliationSearchModel), typeof(int), "dbo.[Accounting.Reconciliation.Data]"),
            new ClassTypeIDMap(9000, typeof(Opportunity), typeof(int), "dbo.[Opportunity.Data]"),
            new ClassTypeIDMap(10000, typeof(OrderDataSearchModel), typeof(int), "dbo.[Order.Data]"),
            new ClassTypeIDMap(10020, typeof(OrderItemDataSearchModel), typeof(int), "dbo.[Order.Item.Data]"),
            new ClassTypeIDMap(10040, typeof(DestinationDataSearchModel), typeof(int), "dbo.[Order.Destination.Data]"),
            new ClassTypeIDMap(10200, typeof(EstimateDataSearchModel), typeof(int), "dbo.[Order.Data]"),
            new ClassTypeIDMap(10300, typeof(CreditMemoDataSearchModel), typeof(int), "dbo.[Order.Data]"),
            new ClassTypeIDMap(12000, typeof(MaterialDataSearchModel), typeof(int), "dbo.[Part.Material.Data]"),
            new ClassTypeIDMap(12002, typeof(MaterialCategorySearchModel), typeof(int), "dbo.[Part.Material.Category]"),
            new ClassTypeIDMap(12020, typeof(LaborDataSearchModel), typeof(int), "dbo.[Part.Labor.Data]"),
            new ClassTypeIDMap(12022, typeof(LaborCategorySearchModel), typeof(int), "dbo.[Part.Labor.Category]"),
            new ClassTypeIDMap(12030, typeof(MachineDataSearchModel), typeof(int), "dbo.[Part.Machine.Data]"),
            new ClassTypeIDMap(12032, typeof(MachineCategorySearchModel), typeof(int), "dbo.[Part.Machine.Category]"),
            new ClassTypeIDMap(12040, typeof(AssemblyDataSearchModel), typeof(int), "dbo.[Part.Subassembly.Data]"),
            new ClassTypeIDMap(12042, typeof(AssemblyCategorySearchModel), typeof(int), "dbo.[Part.Subassembly.Category]"),
            new ClassTypeIDMap(12060, typeof(QuickItemSearchModel), typeof(int), "dbo.[Part.QuickItem.Data]"),
            new ClassTypeIDMap(12070, typeof(SurchargeDefSearchModel), typeof(int), "dbo.[Part.Surcharge.Data]"),
            new ClassTypeIDMap(14000, typeof(BoardDefinitionDataSearchModel), typeof(int), "dbo.[Board.Definition.Data]"),
            new ClassTypeIDMap(14100, typeof(AlertDefinitionSearchModel), typeof(int), "dbo.[Alert.Definition]"),
            new ClassTypeIDMap(14110, typeof(MessageBody), typeof(int), "dbo.[Message.Body]"),
            new ClassTypeIDMap(14111, typeof(MessageHeaderSearchModel), typeof(int), "dbo.[Message.Header]"),
            new ClassTypeIDMap(14220, typeof(MessageBodyTemplateSearchModel), typeof(int), "dbo.[Message.Body.Template]"),
            new ClassTypeIDMap(14500, typeof(DashboardDataSearchModel), typeof(int), "dbo.[Dashboard.Data]"),
            new ClassTypeIDMap(14503, typeof(DashboardWidgetDefinitionSearchModel), typeof(int), "dbo.[System.Dashboard.Widget.Definition]"),
            new ClassTypeIDMap(15025, typeof(CustomFieldDefinitionSearchModel), typeof(int), "dbo.[CustomField.Definition]"),
            new ClassTypeIDMap(15027, typeof(CustomFieldLayoutDefinitionSearchModel), typeof(int), "dbo.[CustomField.Layout.Definition]"),

        };

        public static ClassTypeIDMap MapForCTID(int ctid)
        {
            ClassTypeIDMap m = Map.Where(x => x.ClassTypeID == ctid).FirstOrDefault();
            if (m==null)
                throw (new Exception("Can't match class type of" + ctid.ToString()));
            return m;
        }

        public static ClassTypeIDMap MapForClassType(System.Type classtype)
        {
            ClassTypeIDMap m = Map.Where(x => x.ClassType == classtype).FirstOrDefault();
            if (m == null)
                throw (new Exception("Can't match class for" + classtype.Name));
            return m;
        }

        public static ClassTypeIDMap MapForClassName(string classname)
        {
            ClassTypeIDMap m = Map.Where(x => x.ClassName == classname).FirstOrDefault();
            if (m == null)
                throw (new Exception("Can't match class for" + classname));
            return m;
        }


        public static Type ClassTypeForCTID(int ctid) => MapForCTID(ctid).ClassType;

        public static int CTIDForClassType(System.Type classtype) => MapForClassType(classtype).ClassTypeID;

    }
}
