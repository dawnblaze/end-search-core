﻿using Endor.Search.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.Helper_Classes
{
    public static class Extensions
    {
        private static readonly Dictionary<int, BoardDataTypeMap> TypeMap = new Dictionary<int, BoardDataTypeMap>()
        {
            {
                ClassTypeIDMap.MapForClassType(typeof(CompanyDataSearchModel)).ClassTypeID, new BoardDataTypeMap { Type = "Company", DetailLevel = "Company Level" }  //Company
            },
            {
                ClassTypeIDMap.MapForClassType(typeof(ContactDataSearchModel)).ClassTypeID, new BoardDataTypeMap { Type = "Company", DetailLevel = "Contact Level" }  //Contact
            },
            {
                ClassTypeIDMap.MapForClassType(typeof(EstimateDataSearchModel)).ClassTypeID, new BoardDataTypeMap { Type = "Estimate", DetailLevel = "Estimate Level" } //Estimate
            },
            {
                ClassTypeIDMap.MapForClassType(typeof(OrderDataSearchModel)).ClassTypeID, new BoardDataTypeMap { Type = "Order", DetailLevel = "Order Level" } //Order
            },
            {
                ClassTypeIDMap.MapForClassType(typeof(DestinationDataSearchModel)).ClassTypeID, new BoardDataTypeMap { Type = "Order/Estimate", DetailLevel = "Order Destination Level" } //Destination
            },
            {
                ClassTypeIDMap.MapForClassType(typeof(OrderItemDataSearchModel)).ClassTypeID, new BoardDataTypeMap { Type = "Order/Estimate", DetailLevel = "Order/Estimate Line Item Level" } //LineItem
            },
            {
                ClassTypeIDMap.MapForClassType(typeof(Opportunity)).ClassTypeID, new BoardDataTypeMap { Type = "Opportunity", DetailLevel ="Opportunity Level" } //Opportunity
            }
        };

        public static BoardDataTypeMap GetBoardDataTypeMappings(int ClassTypeID)
        {
            return TypeMap.ContainsKey(ClassTypeID) ? TypeMap[ClassTypeID] : null;
        }

        public class BoardDataTypeMap
        {
            public string DetailLevel;
            public string Type;
        }

        public static string StripDashes(this string mystring)
        {
            return mystring.Replace("-", String.Empty);
        }
    }
}
