﻿namespace Endor.Search.Core.Enums
{
    public enum LocatorType
    {
        Unknown = 0,
        Address = 1,
        Phone = 2,
        Email = 3,
        WebAddress = 4,
        Twitter = 5,
        Facebook = 6,
        LinkedIn = 7,
        FTP = 8,
        IP = 9
    }
}
