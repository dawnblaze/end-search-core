﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Endor.Search.Core.Interfaces
{
    public interface IReadOnlyRepository
    {
        /// <summary>
        /// Get List of search models for the specified IDs
        /// </summary>
        /// <typeparam name="T">IAutomappableSearchModel</typeparam>
        /// <param name="IDs">ID Values</param>
        /// <returns></returns>
        Task<List<T>> GetSearchModelByIdAsync<T>(int[] IDs) where T : IAutomappableSearchModel<T>;

    }
}
