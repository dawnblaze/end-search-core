﻿namespace Endor.Search.Core.Interfaces
{
    /// <summary>
    /// interface for listing of things in the list sidebar on the client
    /// </summary>
    public interface ISimpleSearchModel
    {
        int ID { get; }
        string Path { get; }
        string Header { get; }
        string Subheader { get; }

    }
}
