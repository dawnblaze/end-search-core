﻿using System;

namespace Endor.Search.Core.Interfaces
{
    public interface IModifyDateTimeTrackable
    {
        DateTime ModifiedDT { get; set; }
    }

}
