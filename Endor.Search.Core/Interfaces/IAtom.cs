﻿namespace Endor.Search.Core.Interfaces
{
    public interface IAtom : IModifyDateTimeTrackable, IEntity
    {
        int ClassTypeID { get; }
        int ID { get; set; }
    }

}
