﻿using System;
using System.Threading.Tasks;

namespace Endor.Search.Core.Interfaces
{
    public interface IAtomService
    {
        /// <summary>
        /// Called Before Model is updated
        /// </summary>
        Task DoBeforeUpdateAsync(IRepository repository, IAtom oldModel);

        /// <summary>
        /// Called before Model is Cloned
        /// </summary>
        Task DoBeforeCloneAsync(IRepository repository);

        /// <summary>
        /// Check if the model can be deleted
        /// </summary>
        /// <param name="repository"></param>
        /// <returns></returns>
        Task<Tuple<bool, string>> CanDeleteAsync(IRepository repository);
    }

}
