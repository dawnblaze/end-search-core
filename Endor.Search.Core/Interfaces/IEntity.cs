﻿namespace Endor.Search.Core.Interfaces
{
    /// <summary>
    /// Interface for CRUD Service
    /// </summary>
    public interface IEntity
    {
        short BID { get; set; }
    }
}
