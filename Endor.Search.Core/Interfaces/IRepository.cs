﻿using System.Threading.Tasks;

namespace Endor.Search.Core.Interfaces
{
    public interface IRepository : IReadOnlyRepository
    {
        /// <summary>
        /// Execute Raw SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<int> ExecuteSqlRawAsync(string sql, params object[] parameters);
    }
}
