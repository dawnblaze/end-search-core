﻿using Lucene.Net.Index;

namespace Endor.Search.Core.Interfaces
{

    /// <summary>
    /// A search model with required properties like SearchDetail and Path
    /// </summary>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="I"></typeparam>
    public interface ISearchModel : IAtom
    {
        //string Key { get; } // appears to not be used
        string Summary { get; set; }
        string Detail { get; set; }
        Term GetSearchKeyTerm();

        bool HasImage { get; set; }
        void FillExtendedProperties(); // this is used to fill in the Summary, Detail, etc. based on other information in the data
    }
}
