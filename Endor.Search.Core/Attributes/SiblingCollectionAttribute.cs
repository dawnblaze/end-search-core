﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.SharedKernal
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SiblingCollectionAttribute : Attribute
    {
        private string _parentLinkIDProperty;
        private string _siblingTableName;
        private string _siblingLinkToParentID;
        public SiblingCollectionAttribute(string parentLinkIDProperty, string siblingTableName, string siblingLinkToParentID)
        {
            _parentLinkIDProperty = parentLinkIDProperty;
            _siblingTableName = siblingTableName;
            _siblingLinkToParentID = siblingLinkToParentID;
        }

        public string GetFromStmt(string originalSiblingTableName)
        {
            var IDIncludeStmt = "(select " + _parentLinkIDProperty + " FROM " + originalSiblingTableName + " Where BID = @BID and ID IN @IDs)";
            return " FROM " + _siblingTableName + " WHERE BID = @BID AND " + _siblingLinkToParentID + " IN " + IDIncludeStmt;
        }
    }
}
