﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Search.Core.SharedKernal.LukeMapper.Attributes
{
    public enum Store
    {
        YES,
        NO
    }

    public enum Index
    {
        ANALYZED,
        ANALYZED_NO_NORMS,
        NO,
        NOT_ANALYZED,
        NOT_ANALYZED_NO_NORMS
    }
}
