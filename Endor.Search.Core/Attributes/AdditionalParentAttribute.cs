﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.SharedKernal
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class AdditionalParentAttribute : Attribute
    {
        private string _parentTableName;
        private string _childIDProperty;
        private string _parentIDProperty;
        private bool _ignoreBID;
        public AdditionalParentAttribute(string parentTableName, string childIDProperty, string parentIDProperty, bool ignoreBID = false)
        {
            _parentTableName = parentTableName;
            _childIDProperty = childIDProperty;
            _parentIDProperty = parentIDProperty;
            _ignoreBID = ignoreBID;
        }

        public string GetFromStmt(string thisTableIdentifier, string joinStmt = "", string childTableReference = "")
        {
            var childTableReferenceWithPeriod = String.IsNullOrWhiteSpace(childTableReference) ? "" : childTableReference + '.';
            var IDIncludeStmt = "(select " + _childIDProperty + " FROM " + thisTableIdentifier + " Where BID = @BID and ID IN @IDs)";
            var stmt = " FROM " + _parentTableName + $" {childTableReference} " + joinStmt;
            if (_ignoreBID) stmt += $" WHERE {childTableReferenceWithPeriod}" + _parentIDProperty + " IN " + IDIncludeStmt;
            else stmt += $" WHERE {childTableReferenceWithPeriod}BID = @BID AND {childTableReferenceWithPeriod}" + _parentIDProperty + " IN " + IDIncludeStmt;
            return stmt;
        }
    }
}
