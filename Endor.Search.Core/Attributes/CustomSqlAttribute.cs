﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.SharedKernal
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CustomSqlAttribute : Attribute
    {
        private string _customStmt;
        
        public CustomSqlAttribute(string customStmt)
        {
            _customStmt = customStmt;
            
        }

        public string GetCustomStmt => _customStmt;

    }
}
