﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.SharedKernal
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class DBIgnoreAttribute : Attribute
    {
    }
}
