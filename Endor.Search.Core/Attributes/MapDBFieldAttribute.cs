﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.SharedKernal
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MapDBFieldAttribute : Attribute
    {
        private string _DBFieldName;
        public MapDBFieldAttribute(string DBFieldName)
        {
            _DBFieldName = DBFieldName;
        }
        public string DBFieldName => _DBFieldName;

    }
}
