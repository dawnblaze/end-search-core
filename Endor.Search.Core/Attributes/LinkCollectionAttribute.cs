﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.SharedKernal
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class LinkCollectionAttribute : Attribute
    {
        private string _linkTableName;
        private string _linkIDProperty;
        private string _linkTableSiblingIDProperty;
        private string _siblingLinkTableName;
        private string _siblingLinkIDColumn;
        private string _additionalWhereStmt;
        public LinkCollectionAttribute(string linkTableName, string linkIDProperty, string linkTableSiblingIDProperty, string siblingLinkTableName, string siblingLinkIDColumn, string additionalWhereStmt = "")
        {
            _linkTableName = linkTableName;
            _linkIDProperty = linkIDProperty;
            _linkTableSiblingIDProperty = linkTableSiblingIDProperty;
            _siblingLinkTableName = siblingLinkTableName;
            _siblingLinkIDColumn = siblingLinkIDColumn;
            _additionalWhereStmt = additionalWhereStmt;
        }

        public string GetExtraSelectParamters()
        {
            return "lt." + _linkIDProperty +" AS ParentID ";
        }

        public string GetFromStmt(string parentTableName)
        {
            var IDIncludeStmt = "(select ID FROM " + parentTableName + " Where BID = @BID and ID IN @IDs)";
            var str = " FROM " + _linkTableName + " lt "
                + " INNER JOIN " + _siblingLinkTableName + " st on st." + _siblingLinkIDColumn + " = lt." + _linkTableSiblingIDProperty
                + " WHERE lt.BID = @BID AND lt." + _linkIDProperty + " IN " + IDIncludeStmt;
            if (!String.IsNullOrWhiteSpace(_additionalWhereStmt))
            {
                str += " AND lt." + _additionalWhereStmt;
            }
            return str;
        }
    }
}
