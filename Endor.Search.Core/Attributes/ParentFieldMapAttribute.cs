﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.SharedKernal
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class ParentFieldMapAttribute : Attribute
    {
        private string _parentTableName;
        private string _parentProperty;
        private string _childIDProperty;
        private string _parentIDProperty;
        private string _parentTableIdentifierProperty;
        private bool _ignoreBIDProperty;
        public ParentFieldMapAttribute(string parentTableName, string parentProperty, string childIDProperty, string parentIDProperty, string parentTableIdentifierProperty, bool ignoreBID = false)

        {
            _parentTableName = parentTableName;
            _parentProperty = parentProperty;
            _childIDProperty = childIDProperty;
            _parentIDProperty = parentIDProperty;
            _parentTableIdentifierProperty = parentTableIdentifierProperty;
            _ignoreBIDProperty = ignoreBID;
        }

        public string GetParentPropertyName => _parentProperty;
        public string GetParentTableIdentifier => _parentTableIdentifierProperty;

        public string GetJoinStmt(string thisTableIdentifier)
        {
            string childIdentifier = thisTableIdentifier + "." + _childIDProperty;
            string parentIdentifier = _parentTableIdentifierProperty + "." + _parentIDProperty;
            var returnString = " LEFT JOIN " + _parentTableName + " " + _parentTableIdentifierProperty + " ON " + childIdentifier + " = " + parentIdentifier + " ";
            if (!_ignoreBIDProperty) returnString += "AND " + _parentTableIdentifierProperty + ".BID = @BID"; 
            return returnString;
        }
    }
}
