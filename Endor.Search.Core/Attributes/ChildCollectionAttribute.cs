﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Search.Core.SharedKernal
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class ChildCollectionAttribute : Attribute
    {
        private string _childTableName;
        private string _childLinkIDProperty;
        public ChildCollectionAttribute(string childTableName, string childLinkIDProperty)
        {
            _childTableName = childTableName;
            _childLinkIDProperty = childLinkIDProperty;
        }

        public string GetFromStmt(string parentTableName, string joinStmt = "", string childTableReference = "")
        {
            var childTableReferenceWithPeriod = String.IsNullOrWhiteSpace(childTableReference) ? "" : childTableReference + '.';
            var IDIncludeStmt = "(select ID FROM " + parentTableName + " Where BID = @BID and ID IN @IDs)";
            var stmt = " FROM " + _childTableName + $" {childTableReference} " + joinStmt;
            stmt += $" WHERE {childTableReferenceWithPeriod}BID = @BID AND {childTableReferenceWithPeriod}" +_childLinkIDProperty + $" IN " + IDIncludeStmt;
            return stmt;
        }
    }
}
